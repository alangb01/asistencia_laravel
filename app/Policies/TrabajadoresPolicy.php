<?php

namespace App\Policies;

use App\Admin\Asistencia\Trabajador;
use App\Admin\Personal\Usuario;
use Illuminate\Auth\Access\HandlesAuthorization;

class TrabajadoresPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */

    public function before($user, $ability){
        $niveles=[
            Usuario::NIVEL_ADMINISTRADOR_SUCURSAL,
            Usuario::NIVEL_GERENTE_GENERAL
        ];

        if(in_array($user->nivel_acceso ,$niveles)){
            return true;
        }
    }


    public function index(Usuario $user)
    {
        return false;
    }


}
