<?php

namespace App\Admin\Asistencia;

use Illuminate\Database\Eloquent\Model;

use App\Admin\Util\Cronograma;
use App\Admin\Util\Util;

class Horario extends Model
{
    //
    const TIPO_GENERAL_TIEMPO_COMPLETO="TIEMPO_COMPLETO";
    const TIPO_GENERAL_MEDIO_TIEMPO="MEDIO_TIEMPO";
	const TIPO_GENERAL_PRACTICAS="PRACTICAS";
	
	const TIPO_ESPECIFICO_DIA_SEMANA="POR_DIA_SEMANA";
	const TIPO_ESPECIFICO_FECHA="POR_FECHA";

    protected $table='asistencia_horarios';
    protected $dates=['fecha_inicio','fecha_termino'];

    public function trabajador(){
        return $this->belongsTo(Trabajador::class,'id_trabajador','id');
    }

    public function turnos(){
        return $this->hasMany(Turno::class,'id_horario','id')->orderBy('dia_semana','asc')->orderBy('fecha_hora_entrada','asc');
    }

    public function getCronograma(){
        return new Cronograma($this);
    }

    public function turnosDelDia($dia_semana){
        $listado=Turno::where('id_horario','=',$this->id)
            ->where('dia_semana','=',$dia_semana)->get();

        return $listado;
    }

    public function totalHorasSemanales(){
        $total_horas=0;
        foreach ($this->turnos as $id => $turno) {
            $turno->timeEnCarbon();


            $total_horas+=$turno->fecha_hora_salida->timestamp-$turno->fecha_hora_entrada->timestamp;
        }

        return Util::calcularEnHorasDesdeDiffTimestamp($total_horas);
    }

    public function totalHorasPorDia($dia_semana){
        $total_horas=0;

        foreach ($this->turnos as $id => $turno) {
            if ($turno->dia_semana==$dia_semana) {
                $total_horas+=$turno->fecha_hora_salida->timestamp-$turno->fecha_hora_entrada->timestamp;
            }
        }

        if($total_horas==0){
            return "";
        }
        return Util::calcularEnHorasDesdeDiffTimestamp($total_horas);
    }

    public function existeCruceDeTurnos(Turno $turno){
        $turno->timeEnCarbon();
        $fecha_hora_entrada=$turno->fecha_hora_entrada->format('H:i:s');
        $fecha_hora_salida=$turno->fecha_hora_salida->format('H:i:s');


        $filtro_cruce="(";
        $filtro_cruce.="(TIME_TO_SEC('".$fecha_hora_entrada."')>=TIME_TO_SEC(fecha_hora_entrada) ";
        $filtro_cruce.="AND TIME_TO_SEC('".$fecha_hora_entrada."')<=TIME_TO_SEC(fecha_hora_salida)) ";
        $filtro_cruce.="OR (TIME_TO_SEC('".$fecha_hora_salida."')>=TIME_TO_SEC(fecha_hora_entrada) ";
        $filtro_cruce.="AND TIME_TO_SEC('".$fecha_hora_salida."')<=TIME_TO_SEC(fecha_hora_salida)) ";
        $filtro_cruce.=")"; 

        $turnos_cruzados=Turno::where('id_horario','=',$this->id)
            ->where('id','!=',$turno->id)
            ->where('dia_semana','=',$turno->dia_semana)
            ->whereRaw(\DB::raw($filtro_cruce))->get();

        // dd($listado);
        return $turnos_cruzados->count()>0;
    }

    public function existeCruceDeHorarios(Horario $horario){
        $fecha_inicio=$horario->fecha_termino->format('Y-m-d');
        $fecha_termino=$horario->fecha_termino->format('Y-m-d');

        $filtro_cruce="(";
        $filtro_cruce.="'".$fecha_inicio."' between fecha_inicio and fecha_termino ";
        $filtro_cruce.="or '".$fecha_termino."' between fecha_inicio and fecha_termino";
        $filtro_cruce.=")";
        /*  $filtro_cruce="(";
        $filtro_cruce.="(TIME_TO_SEC('".$fecha_hora_entrada."')>=TIME_TO_SEC(fecha_hora_entrada) ";
        $filtro_cruce.="AND TIME_TO_SEC('".$fecha_hora_entrada."')<=TIME_TO_SEC(fecha_hora_salida)) ";
        $filtro_cruce.="OR (TIME_TO_SEC('".$fecha_hora_salida."')>=TIME_TO_SEC(fecha_hora_entrada) ";
        $filtro_cruce.="AND TIME_TO_SEC('".$fecha_hora_salida."')<=TIME_TO_SEC(fecha_hora_salida)) ";
        $filtro_cruce.=")"; 
        */


        $horarios_cruzados=Horario::where('id_trabajador','=',$horario->trabajador->id)->where('id','!=',$this->id)
            ->whereRaw(\DB::raw($filtro_cruce))->get();

        // dd($listado);
        return $horarios_cruzados->count()>0;
    }
}
