<?php

namespace App\Admin\Asistencia;

use Illuminate\Database\Eloquent\Model;

use App\Admin\Util\Util;

class Sancion extends Model
{
    //
    protected $table = 'asistencia_sanciones';
    protected $dates = ['fecha_hora_inicio','fecha_hora_termino'];

     public function trabajador(){
    	return $this->belongsTo(Trabajador::class,'id_trabajador','id');
    }

    public function tiempo($mostrar_cero=true){
        return Util::calcularTiempoEntreFechasHoras($this->fecha_hora_inicio,$this->fecha_hora_termino);
        // $diferencia=$this->salida->fecha_hora_marcacion->timestamp
        // 				-$this->entrada->fecha_hora_marcacion->timestamp;
        // return $diferencia;

       /* $intervalo=$this->fecha_hora_termino->diff($this->fecha_hora_inicio);

        // $intervalo->format("%Y %M %d %H %M ");
        $año=$intervalo->format("%Y");
        $mes=$intervalo->format("%M");
        $dia=$intervalo->format("%d");
        $hora=$intervalo->format("%H");
        $minuto=$intervalo->format("%m");
        $segundo=$intervalo->format("%s");

        $cadena="";
        if ($año>0) {
            $cadena.=" ".$año." año".($año>1?'s':'');
        }
        if ($mes>0) {
            $cadena.=" ".$mes." mes".($mes>1?'es':'');
        }
        if ($dia>0) {
            $cadena.=" ".$dia." dia".($dia>1?'s':'');
        }
        if ($hora>0) {
            $cadena.=" ".$hora."h";
        }
        if ($minuto>0) {
            $cadena.=" ".$minuto."m";
        }
        if ($segundo>0) {
            $cadena.=" ".$segundo."s";
        }

        $cadena=trim($cadena);
        if ($mostrar_cero && $cadena=="") {
            $cadena="0s";
        }elseif ($cadena=="") {
            $cadena="No se pudo calcular";;
        }

        return $cadena;*/
    }

     public function descripcionToString(){
        if(trim($this->descripcion)!=""){
            return $this->descripcion;
        }else{
            return "Sin descripcion";
        }
    }
}
