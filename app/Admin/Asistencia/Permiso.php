<?php

namespace App\Admin\Asistencia;

use Illuminate\Database\Eloquent\Model;

use App\Admin\Util\Util;

class Permiso extends Model
{
    //
	const TIPO_DIA_LIBRE="DL";
	const TIPO_LICENCIA="LIC";
	const TIPO_PERMISO="PER";
	const TIPO_VACACIONES="VAC";

     const ESTADO_ACTIVO="AC";
    const ESTADO_INACTIVO="IN";
    const ESTADO_VERIFICAR="VR";

    protected $table = 'asistencia_permisos';

    protected $dates = ['fecha_hora_inicio','fecha_hora_termino'];

    public function trabajador(){
    	return $this->belongsTo(Trabajador::class,'id_trabajador','id');
    }

    public function tiempoTranscurrido($mostrar_cero=true){
        return Util::calcularTiempoDesdeCarbon($this->fecha_hora_inicio,$this->fecha_hora_termino);
    }

    public function tiempoJustificado(){
        return $this->tiempo_justificado;
    }

    

    public function inicioToString(){
        $cadena="[".Util::diaToString($this->fecha_hora_inicio)."]";
        $cadena.=$this->fecha_hora_inicio->format("Y-");
        $cadena.=Util::mesToString($this->fecha_hora_inicio);
        $cadena.=$this->fecha_hora_inicio->format("-d");
        $cadena.=$this->fecha_hora_inicio->format(" h:i a");
        return $cadena;
    }

    public function terminoToString(){
        $cadena="[".Util::diaToString($this->fecha_hora_termino)."]";
        $cadena.=$this->fecha_hora_termino->format("Y-");
        $cadena.=Util::mesToString($this->fecha_hora_termino);
        $cadena.=$this->fecha_hora_termino->format("-d");
        $cadena.=$this->fecha_hora_termino->format(" h:i a");

        return $cadena;
    }

    public function diaToString(){
        return Util::diaToString($this->fecha_hora_inicio);
    }

    public function tipoToString(){
        switch ($this->tipo_justificacion) {
            case self::TIPO_PERMISO:
                $justificacion="permiso";
                break;
            case self::TIPO_VACACIONES:
                $justificacion="vacaciones";
                break;
            case self::TIPO_LICENCIA:
                $justificacion="licencia";
                break;
            case self::TIPO_DIA_LIBRE:
                $justificacion="dia libre";
                break;
            default:
                $justificacion="no definido";
                break;
        }
        return $justificacion;
    }

    public function descripcionToString(){
        if(trim($this->descripcion)!=""){
            return $this->descripcion;
        }else{
            return "Sin descripcion";
        }
    }
}
