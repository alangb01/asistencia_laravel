<?php

namespace App\Admin\Asistencia;

use Illuminate\Database\Eloquent\Model;

use App\Admin\Util\Util;

class Suspencion extends Model
{
    //SUSPENCION DE LABORES APLICADAS POR LA EMPRESA
    protected $table = 'asistencia_suspenciones';

    protected $dates = ['fecha_hora_inicio','fecha_hora_termino'];

    public function scopeCargarPorPeriodo(Request $request, $fecha_inicio, $fecha_final){
    	return $this->whereBetween('fecha_hora_inicio',array($fecha_inicio,$fecha_termino));
    }


    public function tiempo($mostrar_cero=true){
        return Util::calcularTiempoEntreFechas($this->fecha_hora_inicio,$this->fecha_hora_termino);
    }

     public function descripcionToString(){
        if(trim($this->descripcion)!=""){
            return $this->descripcion;
        }else{
            return "Sin descripcion";
        }
    }

}
