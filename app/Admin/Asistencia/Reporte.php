<?php

namespace App\Admin\Asistencia;

use Illuminate\Database\Eloquent\Model;

use Carbon\Carbon;
use App\Admin\Util\Util;

class Reporte extends Model
{
    //FECHA INICIO Y TERMINO CARBON INSTANCE

    public function trabajador(){
    	return $this->hasOne(Trabajador::class,'id','id_trabajador');
    }

    public function marcaciones(){
        return $this->hasMany(Marcacion::class,'id_reporte','id');
    }

    public function horarios(){
        return collect();
    }

    /*
    public function scopeFiltrar($query,$request){
    	return $this->trabajador->where('estado','AC')->get();
    }*/

    private $listado_turnos;
    private $listado_marcaciones;
    private $listado_asistencias;

    private $listado_suspenciones;
    private $listado_permisos;
    private $listado_sanciones;

    //estadisticas generales
    // public $dias_programados;
    // public $tiempo_programado;



    // public $dias_asistidos;
    // public $tiempo_asistido;
public $dias_suspenciones;
public $tiempo_suspenciones;
    //estadisticas especificas
   /* public $dias_turnos;
    
    public $dias_asistencias;
    public $dias_permisos;
    public $dias_sancionados;

    public $tiempo_turnos;
    
    public $tiempo_asistencias;
    public $tiempo_permisos;
    public $tiempo_sanciones;*/



    public function preparar(){

        // DIAS - CONTROL
        $this->dias_programados=0;
        $this->dias_asistidos=0;
        $this->ratio_dias=0;

        //TIEMPO - CONTROL
        $this->tiempo_programado=0;
        $this->tiempo_asisstido=0;
        $this->ratio_tiempo=0;

        /*
        //TIEMPO CONTABILIZADO DE TRABAJO
        $this->tiempo_laborado=0;               //TIEMPO LABORADO NORMAL (POR ASISTENCIAS DE MARCACION)
        $this->tiempo_permiso_justificado=0;    //TIEMPO NO LABORADO PERO CONTABILIZADO COMO LABORADO 
        
        //TIEMPO NO CONTABILIZADO
        $this->tiempo_permiso_injustificado=0;  //TIEMPO NO LABORADO
        $this->tiempo_no_laborado=0;  // TIEMPO NO LABORADO POR INASISTENCIA
        $this->tiempo_sancionado=0; // TIEMPO NO LABORADO POR SANCION
    */
        //LISTADOS OBTENIDOS
        $this->listado_reportes_diarios=collect();

        $this->listado_horarios=collect();
        $this->listado_turnos=collect();
        $this->listado_marcaciones=collect();
        $this->listado_asistencias=collect();
        $this->listado_sanciones=collect();
        $this->listado_permisos=collect();
        $this->listado_suspenciones=collect();
        


        $this->generarReportesDiarios();
    }

    private function generarReportesDiarios(){
        $fecha_recorrido=$this->fecha_inicio->copy();  

        //cargar reportes diarios
        while ($fecha_recorrido->timestamp<=$this->fecha_final->timestamp){
            $listado_horarios=$this->cargarHorarios($fecha_recorrido);

            $listado_turnos=$this->cargarTurnos($fecha_recorrido,$listado_horarios);
            $listado_marcaciones=$this->cargarMarcaciones($fecha_recorrido);

            $listado_suspenciones=$this->cargarSuspenciones($fecha_recorrido);
            $listado_permisos=$this->cargarPermisos($fecha_recorrido);
            $listado_sanciones=$this->cargarSanciones($fecha_recorrido);
            $listado_asistencias=$this->cargarAsistencias($listado_marcaciones);

            $reporte_diario=new ReporteDiario($this,$fecha_recorrido->copy(),
                $listado_horarios,$listado_turnos,$listado_marcaciones,
                $listado_suspenciones,$listado_permisos,
                $listado_sanciones,$listado_asistencias);

            $this->listado_reportes_diarios->push($reporte_diario);
            $fecha_recorrido->addDay();
        } 

        //generando estadisticas generales
        foreach ($this->listado_reportes_diarios as $id_r => $reporte_diario) {
            $reporte_diario->preparar();
            
            $this->dias_turnos+=$reporte_diario->dias_turnos;
            $this->tiempo_turnos+=$reporte_diario->tiempo_turnos;

            $this->dias_asistencias+=$reporte_diario->dias_asistencias;
            $this->tiempo_asistencias+=$reporte_diario->tiempo_asistencias;

            $this->dias_suspenciones+=$reporte_diario->dias_suspenciones;
            $this->tiempo_suspenciones+=$reporte_diario->tiempo_suspenciones;
            /*
            $this->dias_suspenciones_completas+=$reporte_diario->dias_suspenciones_completas;
            $this->dias_suspenciones_parciales+=$reporte_diario->dias_suspenciones_parciales;
            
            $this->dias_permisos_completos+=$reporte_diario->dias_permisos_completos;
            $this->dias_permisos_parciales+=$reporte_diario->dias_permisos_parciales;
            $this->dias_sanciones_completas+=$reporte_diario->dias_sanciones_completas;
            $this->dias_sanciones_parciales+=$reporte_diario->dias_sanciones_parciales;

            
            $this->tiempo_suspenciones_completas+=$reporte_diario->tiempo_suspenciones_completas;
            $this->tiempo_suspenciones_parciales+=$reporte_diario->tiempo_suspenciones_parciales;
            
            $this->tiempo_permisos_completos+=$reporte_diario->tiempo_permisos_completos;
            $this->tiempo_permisos_parciales+=$reporte_diario->tiempo_permisos_parciales;

            // echo $reporte_diario->tiempo_permisos_parciales."<br>";
            $this->tiempo_sanciones_completas+=$reporte_diario->tiempo_sanciones_completas;
            $this->tiempo_sanciones_parciales+=$reporte_diario->tiempo_sanciones_parciales;*/
        }

        //estadisticas reales
        $this->dias_programados=$this->dias_turnos;
        $this->dias_laborables=$this->dias_turnos-$this->dias_suspenciones;
        $this->dias_asistidos=$this->dias_asistencias;
        $this->dias_suspenciones=$this->dias_suspenciones;
        // $this->dias_suspendidos=$this->dias_suspenciones_completas+$this->dias_suspenciones_parciales;
        // $this->dias_laborables=$this->dias_turnos-$this->dias_suspenciones_completas;

        // $this->dias_asistidos=$this->dias_asistencias;
        // $this->dias_asistencias=$this->dias_asistidos;
        // $this->dias_faltados_justificados=$this->dias_permisos_completos+$this->dias_permisos_parciales;

        // $this->dias_sancionados=$this->dias_sanciones_completas+$this->dias_sanciones_parciales;
        // // $this->dias_suspenciones=$this->dias_suspenciones_completas+$this->dias_suspenciones_parciales;


        $this->tiempo_programado=$this->tiempo_turnos;
        $this->tiempo_laborable=$this->tiempo_turnos-$this->tiempo_suspenciones;
        $this->tiempo_suspenciones=$this->tiempo_suspenciones;
        // $this->tiempo_suspendido=$this->tiempo_suspenciones_completas+$this->tiempo_suspenciones_parciales;
        // $this->tiempo_laborable=$this->tiempo_turnos-$this->tiempo_suspendido;
        $this->tiempo_asistido=$this->tiempo_asistencias;

        // $this->tiempo_faltado_justificado=$this->tiempo_permisos_completos+$this->tiempo_permisos_parciales;
        // // $this->tiempo_suspendido=$this->tiempo_suspenciones_completas+$this->tiempo_suspenciones_parciales;

        // $this->tiempo_sancionado=$this->tiempo_sanciones_completas+$this->tiempo_sanciones_parciales;

        if ($this->dias_laborables>0) {
            $this->ratio_dias=$this->dias_asistidos/$this->dias_laborables;
        }
        
        if ($this->tiempo_laborable>0) {
            $this->ratio_tiempo=$this->tiempo_asistido/$this->tiempo_laborable;
        }
    }

   



    //CARGAR INFORMACION
    private function cargarHorarios(Carbon $fecha){
        
        $filtro_horario="'".$fecha->format('Y-m-d')."' between `fecha_inicio` and `fecha_termino` ";
        $filtro_horario.="|| `fecha_inicio`<='".$fecha->format('Y-m-d')."' and `fecha_termino`='0000-00-00'";

        $listado_horarios=Horario::where('id_trabajador','=',$this->trabajador->id)
            ->whereRaw(\DB::raw($filtro_horario))->orderBy('fecha_inicio','desc')->get();


        foreach ($listado_horarios as $id_h => $horario) {
            if (!$this->listado_horarios->contains($horario)) {
                $this->listado_horarios->push($horario);
            }   
        }
        

        return $listado_horarios;
    }

    private function cargarTurnos(Carbon $fecha,$listado_horarios){
        $turnos=collect();
        foreach ($listado_horarios as $id_h => $horario) {
            $filtro_rango_horario="";

            $turnos=Turno::where('id_horario','=',$horario->id)
                ->where('dia_semana','=',$fecha->dayOfWeek)
                ->get();

            foreach ($turnos as $id_t => $turno) {
                if (!$this->listado_turnos->contains($turno)) {
                    $this->listado_turnos->push($turno);
                }else{
                    $posicion=$this->listado_turnos->search($turno);
                    $turnos[$id_t]=$this->listado_turnos->get($posicion);
                }
            }
        }
        
        return $turnos;
    }

    private function cargarMarcaciones(Carbon $fecha){
        $marcaciones=Marcacion::where('id_trabajador','=',$this->trabajador->id)
            ->whereDate('fecha_hora_marcacion','=',$fecha->format('Y-m-d'))->get();
        
        foreach ($marcaciones as $id_m => $marcacion) {
            if (!$this->listado_marcaciones->contains($marcacion)) {
                $this->listado_marcaciones->push($marcacion);
            }else{
                $posicion=$this->listado_marcaciones->search($marcacion);
                $marcaciones[$id_m]=$this->listado_marcaciones->get($posicion);
            }
        }
        
        return $marcaciones;
    }

    private function cargarSuspenciones(Carbon $fecha){
        $filtro_suspencion="'".$fecha->format('Y-m-d')."' between date(`fecha_hora_inicio`) and date(`fecha_hora_termino`)";
        $suspenciones=Suspencion::whereRaw(\DB::raw($filtro_suspencion))->get();
        
        foreach ($suspenciones as $id_su => $suspencion) {
            if (!$this->listado_suspenciones->contains($suspencion)) {
                //si no existe en la lista general, agregar y enviar lista del dia
                $this->listado_suspenciones=$this->listado_suspenciones->push($suspencion);
            }else{
                //existe en la lista general, referenciarlo para no duplicar o perder el object
                //reemplazar item en lista del dia
                $posicion=$this->listado_suspenciones->search($suspencion);
                $suspenciones[$id_su]=$this->listado_suspenciones->get($posicion);
            }
        }

        return $suspenciones;
    }

    private function cargarPermisos(Carbon $fecha){
        $filtro_permisos="'".$fecha->format('Y-m-d')."' between date(`fecha_hora_inicio`) and date(`fecha_hora_termino`)";
        // echo $filtro_permisos."<br>";
        $permisos=Permiso::where('id_trabajador','=',$this->trabajador->id)
            ->whereRaw(\DB::raw($filtro_permisos))->get();
        
       foreach ($permisos as $id_p => $permiso) {
            if (!$this->listado_permisos->contains($permiso)) {
                $this->listado_permisos=$this->listado_permisos->push($permiso);
            }else{
                //existe en la lista general, referenciarlo para no duplicar o perder el object
                //reemplazar item en lista del dia
                $posicion=$this->listado_permisos->search($permiso);
                $permisos[$id_p]=$this->listado_permisos->get($posicion);
            }
        }

        return $permisos;
    }

    private function cargarSanciones(Carbon $fecha){
        $filtro_sanciones="'".$fecha->format('Y-m-d')."' between date(`fecha_hora_inicio`) and date(`fecha_hora_termino`)";

        $sanciones=Sancion::where('id_trabajador','=',$this->trabajador->id)
            ->whereRaw(\DB::raw($filtro_sanciones))->get();
        
        foreach ($sanciones as $id_sa => $sancion) {
            if (!$this->listado_sanciones->contains($sancion)) {
                $this->listado_sanciones=$this->listado_sanciones->push($sancion);
            }
            else{
                //existe en la lista general, referenciarlo para no duplicar o perder el object
                //reemplazar item en lista del dia
                $posicion=$this->listado_sanciones->search($sancion);
                $sancion[$id_sa]=$this->listado_sanciones->get($posicion);
            }
        }

        return $sanciones;
    }

   
    private function cargarAsistencias($listado_marcaciones){
        // $fecha_actual=$fecha->format("Y-m-d");

        // $listado_marcaciones=$this->trabajador->marcaciones($fecha_actual,$fecha_actual);
        
        $listado_asistencias=collect();

        $tmp_entrada=null;
        $tmp_salida=null;

        $ultima_posicion=count($listado_marcaciones);
        foreach ($listado_marcaciones as $posicion_actual => $marcacion) {
            //VERIFICA LA MARCACION EN RECORRIDO ACTUAL
            //PRIMERO SETEA UNA ENTRADA SI CUMPLE CONDICIONES
            //SEGUNDO SETEA UNA SALIDA SI CUMPLE CONDICIONES
            //SINO MARCA CUALQUIER TEMPORAL QUE TENGA Y LA MARCACION ACTUAL y ESTABLECE ESTADO POR VERIFICAR
            if ($tmp_entrada==null && $tmp_salida==null
                && $marcacion->tipo_marcacion==$marcacion::TIPO_ENTRADA) {
                $tmp_entrada=$marcacion;
            }elseif ($tmp_salida==null && $tmp_entrada!=null 
                && $marcacion->tipo_marcacion==$marcacion::TIPO_SALIDA) {
                $tmp_salida=$marcacion;
            }else{
                if ($tmp_entrada!=null) {
                    $tmp_entrada->estado='VR';
                    $tmp_entrada=null;
                }

                if ($tmp_salida!=null) {
                    $tmp_salida->estado='VR';
                    $tmp_salida=null;
                }

                //marca el actual como null
                $marcacion->estado='VR';
            }

            //echo ($posicion_actual+1)." y ".$ultima_posicion."<br>";
            //SI SE ASIGNARON
            if ($tmp_entrada!=null && $tmp_salida!=null 
               ) {
                // echo $tmp_entrada->fecha_hora_marcacion->format('Y-M-d H:i:s');
                // echo " a ";
                // echo $tmp_salida->fecha_hora_marcacion->format("Y-M-d H:i:s");
                // echo "<br>";
                //SI HAY ENTRADA Y SALIDA Y NO ES ULTIMO asignar
               

                if ($tmp_salida->fecha_hora_marcacion->timestamp-$tmp_entrada->fecha_hora_marcacion->timestamp>0) {
                    $asistencia=new Asistencia();
                    $asistencia->entrada=$tmp_entrada;
                    $asistencia->salida=$tmp_salida;

                    $listado_asistencias->push($asistencia);
                }else{
                    $tmp_entrada->estado="VR";
                    $tmp_salida->estado="VR";
                }

                $tmp_entrada=null;
                $tmp_salida=null;
            }

            if($posicion_actual+1==$ultima_posicion){
                if ($tmp_entrada!=null) {
                    $tmp_entrada->estado='VR';
                    $tmp_entrada=null;
                }

                if ($tmp_salida!=null) {
                    $tmp_salida->estado='VR';
                    $tmp_salida=null;
                }
            }
        }

        $this->listado_asistencias=$this->listado_asistencias->merge($listado_asistencias);
        return $listado_asistencias;
    }  

    public function marcacionesProcesadas(){
        return $this->listado_marcaciones->all();
    }

    public function asistenciasProcesadas(){
        return $this->listado_asistencias->all();
    }

    public function suspencionesProcesadas(){
        return $this->listado_suspenciones->all();
    }

    public function sancionesProcesadas(){
        return $this->listado_sanciones->all();
    }

    public function permisosProcesados(){
        return $this->listado_permisos->all();
    }

    public function reportesDiariosProcesados(){
        return $this->listado_reportes_diarios;
    }

    //ESTADISTICAS
    public function diasProgramadosToString(){
        return $this->dias_programados;
    }

    public function diasAsistidosToString(){
        return $this->dias_asistidos;
    }

    public function ratioDias($formato=null){
        if ($formato=="%") {
            return round($this->ratio_dias*100,2)."%";    
        }else{
            return $this->ratio_dias;
        }
    }

     public function tiempoSuspencionesToString(){
         return Util::calcularEnHorasDesdeDiffTimestamp($this->tiempo_suspenciones);
    }
    /*  
    public function tiempoTurnosToString(){
        return Util::calcularEnHorasDesdeDiffTimestamp($this->tiempo_turnos);
    }

   

    public function tiempoAsistenciasToString(){
        return Util::calcularEnHorasDesdeDiffTimestamp($this->tiempo_asistencias);
    }

    public function tiempoPermisosToString(){
        return Util::calcularEnHorasDesdeDiffTimestamp($this->tiempo_permisos);
    }

    public function tiempoSancionesToString(){
        return Util::calcularEnHorasDesdeDiffTimestamp($this->tiempo_sanciones);
    }*/

    //ESTADISTICAS REALES

    public function tiempoProgramadoToString(){
        return Util::calcularEnHorasDesdeDiffTimestamp($this->tiempo_programado);
    }

    public function tiempoAsistidoToString(){
        return Util::calcularEnHorasDesdeDiffTimestamp($this->tiempo_asistido);
    }

    public function tiempoFueraDeHorarioToString(){
        return Util::calcularEnHorasDesdeDiffTimestamp($this->tiempo_fuera_horario);
    }

    public function tiempoSuspendidoToString(){
        return Util::calcularEnHorasDesdeDiffTimestamp($this->tiempo_suspendido);
    }

    public function tiempoSuspencionesCompletasToString(){
        return Util::calcularEnHorasDesdeDiffTimestamp($this->tiempo_suspenciones_completas);
    }

    public function tiempoSuspencionesParcialesToString(){
        return Util::calcularEnHorasDesdeDiffTimestamp($this->tiempo_suspenciones_parciales);
    }

    public function tiempoLaborableToString(){
        return Util::calcularEnHorasDesdeDiffTimestamp($this->tiempo_laborable);
    }

     public function tiempoFaltaJustificadaToString(){
        return Util::calcularEnHorasDesdeDiffTimestamp($this->tiempo_faltado_justificado);
    }


    public function tiempoFaltaInjustificadaToString(){
        return Util::calcularEnHorasDesdeDiffTimestamp($this->tiempo_faltado_injustificado);
    }

    public function tiempoSancionadoToString(){
        return Util::calcularEnHorasDesdeDiffTimestamp($this->tiempo_sancionado);
    }


    public function ratioTiempo($formato=null){
        if ($formato=="%") {
            return round($this->ratio_tiempo*100,2)."%";
        }else{
            return $this->ratio_tiempo;
        }
    }
}
