<?php

namespace App\Admin\Asistencia;

use Illuminate\Database\Eloquent\Model;

use App\Admin\Util\Util;

class Asistencia extends Model
{
    //
    protected $table = 'asistencia_asistencias';

    public function entrada(){
    	return $this->hasOne(Marcacion::class,'id','id_marcacion_entrada');
    }

    public function salida(){
    	return $this->hasOne(Marcacion::class,'id','id_marcacion_salida');
    }

    public function fecha(){
    	return $this->entrada->fecha_hora_marcacion;
    }

    /**
        @return DateInterval
    */

    public function tiempoAcumulado($mostrar_cero=true){
        return Util::calcularTiempoDesdeCarbon($this->salida->fecha_hora_marcacion,$this->entrada->fecha_hora_marcacion);
        // $diferencia=$this->salida->fecha_hora_marcacion->timestamp
        // 				-$this->entrada->fecha_hora_marcacion->timestamp;
        // return $diferencia;

      /*  $intervalo=$this->salida->fecha_hora_marcacion->diff($this->entrada->fecha_hora_marcacion);

        // $intervalo->format("%Y %M %d %H %M ");
        $año=$intervalo->format("%Y");
        $mes=$intervalo->format("%M");
        $dia=$intervalo->format("%d");
        $hora=$intervalo->format("%H");
        $minuto=$intervalo->format("%m");
        $segundo=$intervalo->format("%s");

        $cadena="";
        if ($año>0) {
            $cadena.=" ".$año." año".($año>1?'s':'');
        }
        if ($mes>0) {
            $cadena.=" ".$mes." mes".($mes>1?'es':'');
        }
        if ($dia>0) {
            $cadena.=" ".$dia." dia".($dia>1?'s':'');
        }
        if ($hora>0) {
            $cadena.=" ".$hora."h";
        }
        if ($minuto>0) {
            $cadena.=" ".$minuto."m";
        }
        if ($segundo>0) {
            $cadena.=" ".$segundo."s";
        }

        $cadena=trim($cadena);
        if ($mostrar_cero && $cadena=="") {
            $cadena="0s";
        }elseif ($cadena=="") {
            $cadena="No se pudo calcular";;
        }

        return $cadena;*/
    }

    public function diaToString(){
        return Util::diaToString($this->fecha());
    }
}
