<?php

namespace App\Admin\Asistencia;

use Illuminate\Database\Eloquent\Model;

use App\Admin\Util\Util;
class Marcacion extends Model
{
    const TIPO_ENTRADA=1;
    const TIPO_SALIDA=2;

    const JUSTIFICACION_NORMAL=1;
    const JUSTIFICACION_ESTUDIO=2;
    const JUSTIFICACION_TRABAJO=3;
    const JUSTIFICACION_SALUD=4;
    const JUSTIFICACION_OTRO=5;

    const ESTADO_ACTIVO="AC";
    const ESTADO_INACTIVO="IN";
    const ESTADO_VERIFICAR="VR";
    //
    protected $table = "asistencia_marcaciones";

    protected $dates=['fecha_hora_marcacion'];

    /*public function scopeFiltrar($query,$params){
    	if (isset($params['id_trabajador'])) {
    		$this->where('id_trabajador',$params['id_trabajador']);
    	}

    	if (isset($params['fecha_inicio']) && $params['fecha_final']) {
    		$this->whereBetween('fecha_hora_marcacion',
    			array(
    				$params['fecha_inicio'],
    				$params['fecha_final']
    			)
    		);
    	}

    	return $this->get();
    }*/

    public function trabajador(){
        return $this->belongsTo(Trabajador::class,'id_trabajador','id');
    }

    public function __toString(){
        $dia=Util::diaToString($this->fecha_hora_marcacion);
        $fecha=$this->fecha_hora_marcacion->format('Y-');
        $fecha.=Util::mesToString($this->fecha_hora_marcacion);
        $fecha.=$this->fecha_hora_marcacion->format('-d');
        $hora=$this->fecha_hora_marcacion->format('h:i:s a');

        return "[".$dia."] ".$fecha." a las ".$hora;
    }

    public function tipoToString(){
        switch ($this->tipo_marcacion) {
            case self::TIPO_ENTRADA:
                $tipo="entrada";
                break;
            case self::TIPO_SALIDA:
                $tipo="salida";
                break;
            default:
                $tipo="no definido";
                break;
        }

        return $tipo;
    }

    public function justificacionToString(){
        switch ($this->tipo_justificacion) {
            case self::JUSTIFICACION_NORMAL:
                $justificacion="normal";
                break;
            case self::JUSTIFICACION_SALUD:
                $justificacion="salud";
                break;
            case self::JUSTIFICACION_TRABAJO:
                $justificacion="trabajo";
                break;
            case self::JUSTIFICACION_ESTUDIO:
                $justificacion="estudio";
                break;
            case self::JUSTIFICACION_OTRO:
                $justificacion="Otro";
                break;
            default:
                $justificacion="no definido";
                break;
        }
        return $justificacion;
    }

    public function observacionToString(){
        if(trim($this->observacion)!=""){
            return $this->observacion;
        }else{
            return "Sin observacion";
        }
    }

    public function horario(){
        return $this->belongsTo(Horario::class,'id_horario','id');
    }

    public function diaToString(){
        return Util::diaToString($this->fecha_hora_marcacion);
    }
}
