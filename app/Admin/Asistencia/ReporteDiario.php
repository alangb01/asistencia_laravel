<?php 
namespace App\Admin\Asistencia;

use App\Admin\Asistencia\Reporte;

use Carbon\Carbon;
use App\Admin\Util\Util;
/**
* 
*/
class ReporteDiario
{
	public $fecha;
	public $reporte;

	public $listado_periodo_tiempo;


	public $listado_marcaciones;
	public $listado_asistencias;
	public $listado_horarios;
	public $listado_turnos;
	public $listado_suspenciones;
	public $listado_sanciones;
	public $listado_permisos;

	public $listado_observaciones;


	//ESTADISTICAS POR DIA
	public $dia_programado;
	public $dia_asistido;
	public $ratio_dias;

	public $tiempo_programado;
	public $tiempo_asistido;
	public $ratio_tiempo;


	public $dias_turnos;
	public $dias_suspenciones;
	// public $dias_suspenciones_completas;
	// public $dias_suspenciones_parciales;
	public $dias_asistencias;
	// public $dias_permisos_completos;
	// public $dias_permisos_parciales;
	// public $dias_sanciones_completas;
	// public $dias_sanciones_parciales;

	public $tiempo_turnos;
	public $tiempo_suspenciones;
	// public $tiempo_suspenciones_completas;
	// public $tiempo_suspenciones_parciales;
	public $tiempo_asistencias;
	// public $tiempo_permisos_completos;
	// public $tiempo_permisos_parciales;
	// public $tiempo_sanciones_completas;
	// public $tiempo_sanciones_parciales;

	/*public $tiempo_asistido_marcacion;
	public $tiempo_asistido_permiso;
	public $tiempo_asistido_sancion;
	public $tiempo_asistido_suspencion;*/


	public function __construct(Reporte $reporte, Carbon $fecha,
		$listado_horarios,
		$listado_turnos,
		$listado_marcaciones,
		$listado_suspenciones,
		$listado_permisos,
		$listado_sanciones,
		$listado_asistencias){
		
		$this->reporte=$reporte;
		$this->fecha=$fecha;

		
		$this->listado_horarios=$listado_horarios;
		$this->listado_turnos=$listado_turnos;
		$this->listado_marcaciones=$listado_marcaciones;
		$this->listado_sanciones=$listado_sanciones;
		$this->listado_permisos=$listado_permisos;
		$this->listado_suspenciones=$listado_suspenciones;
		$this->listado_asistencias=$listado_asistencias;

		// echo $fecha."<br>";
		$this->inicializar();
		// $this->analizar();

	}
	public function diaToString(){
        return Util::diaToString($this->fecha);
    }

	

	private function inicializar(){
		$this->tiempo_programado=0; 

		if ($this->listado_turnos==null) {
			$this->listado_turnos=collect();
		}

		if ($this->listado_marcaciones==null) {
			$this->listado_marcaciones=collect();
		}

		if ($this->listado_suspenciones==null) {
			$this->listado_suspenciones=collect();
		}

		if ($this->listado_permisos==null) {
			$this->listado_permisos=collect();
		}

		if ($this->listado_sanciones==null) {
			$this->listado_sanciones=collect();
		}

		if ($this->listado_asistencias==null) {
			$this->listado_asistencias=collect();
		}

		if ($this->listado_periodo_tiempo==null) {
			$this->listado_periodo_tiempo=collect();
		}

		if($this->listado_observaciones==null){
			$this->listado_observaciones=collect();
		}
	}


	public function preparar(){
		
		//VERIFICA LOS CRUCES ENTRE LAS ASISTENCIAS POR MARCACION Y SUSPENCIONES, SANCIONES = DIAS Y HORAS NO LABORABLES
		foreach ($this->listado_asistencias as $id_a=>$asistencia) {
			foreach ($this->listado_suspenciones as $id_su => $suspencion) {
//				$this->compararAsistenciaSuspencion($asistencia,$suspencion);
			}

			foreach ($this->listado_sanciones as $id_sa => $sancion) {
				// $this->compararAsistenciaSancion($asistencia,$sancion);
			}

			foreach ($this->listado_permisos as $id_sa => $permiso) {
				// $this->compararAsistenciaPermiso($asistencia,$permiso);
			}
		}

		//VERIFICA LOS TURNOS ASIGNADOS Y LAS ASISTENCIAS Y PERMISOS MARCADOS.
		foreach($this->listado_turnos as $id_t=>$turno){
			$turno->timeEnCarbon();
			// $tiempo_programado+=$turno->fecha_hora_salida->timestamp-$turno->fecha_hora_entrada->timestamp;
			foreach ($this->listado_asistencias as $id_a => $asistencia) {
				# code...
				//$this->compararTurnoAsistencia($turno,$asistencia);
			}

			foreach ($this->listado_permisos as $id_p => $permiso) {
				$this->compararTurnoPermiso($turno,$permiso);
			}
		}

		//GENERAR ESTADISTICAS
		$this->generarEstadisticas();
		
	}

	private function generarEstadisticas(){
		//TURNOS
		foreach($this->listado_turnos as $id_t=>$turno){
			$turno->timeEnCarbon();
			$this->tiempo_turnos+=$turno->fecha_hora_salida->timestamp-$turno->fecha_hora_entrada->timestamp;

			if ($this->listado_suspenciones->count()>0) {
				$this->tiempo_suspenciones+=$turno->fecha_hora_salida->timestamp-$turno->fecha_hora_entrada->timestamp;
			}
		}

		if($this->tiempo_turnos>0){
			$this->dias_turnos++;
		}

		if($this->tiempo_suspenciones>0){
			$this->dias_suspenciones++;
		}

		
		// $this->analizarSuspencionesTurnos();

		
		//ASISTENCIAS
		foreach ($this->listado_asistencias as $id_a => $asistencia) {
			$this->tiempo_asistencias+=$asistencia->salida->fecha_hora_marcacion->timestamp
				-$asistencia->entrada->fecha_hora_marcacion->timestamp;
		}

		if ($this->tiempo_asistencias>0) {
			$this->dias_asistencias++;
		}

		// $this->analizarPermisosTurnos();

		

		// $this->analizarSancionesTurnos();
		
	}

	
	
	
	
}