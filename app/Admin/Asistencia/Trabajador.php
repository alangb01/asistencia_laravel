<?php 

namespace App\Admin\Asistencia;


// use App\Admin\Asistencia\Marcacion;
use App\Admin\Personal\Trabajador as TrabajadorBase;
use Carbon\Carbon;
/**
* 
*/
class Trabajador extends TrabajadorBase
{
	
	public function ultimaMarcacion(){
    	return $this->hasOne(Marcacion::class,'id_trabajador','id')->orderBy('fecha_hora_marcacion','desc')->first();
    }

    public function ultimaMarcacionHoy(){
    	$query=$this->hasOne(Marcacion::class,'id_trabajador','id')
    		->whereDate('fecha_hora_marcacion','=',Carbon::today()->format('Y-m-d'))
    		->orderBy('fecha_hora_marcacion','desc');

    	return $query->first();
    }

   
	public function marcaciones($fecha_inicio, $fecha_final){
		return $this->hasMany(Marcacion::class,'id_trabajador','id')
			->whereDate('fecha_hora_marcacion','=',$fecha_inicio)->orderBy('fecha_hora_marcacion','asc')->get();
	}	

	public function sanciones($fecha_inicio, $fecha_final){
		return $this->hasMany(Sancion::class,'id_trabajador','id')
			->whereDate('fecha_hora_inicio','>=',$fecha_inicio)
			->whereDate('fecha_hora_termino','<=',$fecha_final)->orderBy('fecha_hora_inicio','asc')->get();
	}	

	public function permisos($fecha_inicio, $fecha_final){
		return $this->hasMany(Permiso::class,'id_trabajador','id')
			->whereDate('fecha_hora_inicio','>=',$fecha_inicio)
			->whereDate('fecha_hora_termino','<=',$fecha_final)->orderBy('fecha_hora_inicio','asc')->get();
	}	  

	public function horarios(){
		return $this->hasMany(Horario::class,'id_trabajador','id');
	}  
}