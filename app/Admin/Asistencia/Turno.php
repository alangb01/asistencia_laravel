<?php

namespace App\Admin\Asistencia;

use Illuminate\Database\Eloquent\Model;

use App\Admin\Util\Util;

use Carbon\Carbon;
class Turno extends Model
{
    //
    protected $table = 'asistencia_turnos';

    //protected $dates = ['fecha_hora_entrada','fecha_hora_salida'];
    
    public function timeEnCarbon(){
        // protected $dates = ['fecha_hora_entrada','fecha_hora_salida'];
        // protected $dateFormat='H:i:s';

        if ($this->fecha_hora_entrada instanceof Carbon) {
            
            # code...
        }else{
            $this->fecha_hora_entrada=Carbon::createFromFormat("H:i:s",$this->fecha_hora_entrada);
        }


        if ($this->fecha_hora_salida instanceof Carbon) {
            
            # code...
        }else{
            $this->fecha_hora_salida=Carbon::createFromFormat("H:i:s",$this->fecha_hora_salida);
        }
        
        
    }

    public function horario(){
    	return $this->belongsTo(Horario::class,'id_horario','id');
    }

    public function diaToString(){
    	$dias=Util::$DIAS;
    	if ($this->dia_semana!=null && is_numeric($this->dia_semana) && isset($dias[$this->dia_semana])) {
    		return $dias[$this->dia_semana];
    	}else{
    		return "No asignado";
    	}
    }

    public function tiempo($mostrar_cero=true){
        return Util::calcularTiempoDesdeCarbon($this->fecha_hora_entrada,$this->fecha_hora_salida);
    }
}
