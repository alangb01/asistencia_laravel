<?php 

namespace App\Admin\Util;

use Carbon\Carbon;
use App\Admin\Asistencia\Horario;

use App\Admin\Util\Util;
/**
* 
*/
class Cronograma 
{
	private $horario;
	public $ancho;
	public $alto;

	private $ancho_celda;
	private $alto_celda;
	private $ancho_celda_leyenda;
	private $alto_celda_leyenda;

	public function __construct(Horario $horario,$ancho=1000,$alto=500)
	{
		$this->horario=$horario;
		$this->ancho=$ancho;
		$this->alto=$alto;

		$this->calcularCelda();
	}

	private function calcularCelda(){
		$columnas=7; // 7 dias + columna leyenda arriba
		$filas=24;	// 24 horas + fila leyenda izquierda

		$ancho_proporcional=$this->ancho/($columnas+1);
		$alto_proporcional=$this->alto/($filas+1);

		$this->ancho_celda_leyenda=$ancho_proporcional*1.2; //porcentaje de aumento en ahco para leyenda
		$this->alto_celda_leyenda=$alto_proporcional*2; //porcentaje de aumento en ahco para leyenda

		$this->ancho_celda=($this->ancho-$this->ancho_celda_leyenda)/$columnas;
		$this->alto_celda=($this->alto-$this->alto_celda_leyenda)/$filas;

		
	}

	

	
	public function prepararBloquesDia(){
		//determinar los bloques de estructura con coordenadas segun ancho y alto;
		$dia_semana_recorrido=Carbon::SUNDAY;
		$bloques_dia=array();

		//BLOQUE INICIAL

		array_push($bloques_dia,
			(object)array('texto'=>'','tipo'=>'dia',
				'x'=>0,'y'=>0,
				'ancho'=>$this->ancho_celda_leyenda,'alto'=>$this->alto_celda_leyenda,
				'total'=>''));
		//BLOQUE DIAS
		while($dia_semana_recorrido<=Carbon::SATURDAY){
			// $texto=Util::$DIAS[$dia_semana_recorrido]."<br>".$this->horario->totalHorasPorDia($dia_semana_recorrido);

			$x_ajustado=$this->ancho_celda*$dia_semana_recorrido+$this->ancho_celda_leyenda;
			array_push($bloques_dia,
				(object)array('texto'=>Util::$DIAS[$dia_semana_recorrido],'tipo'=>'dia',
					'x'=>$x_ajustado,'y'=>0,
					'ancho'=>$this->ancho_celda,'alto'=>$this->alto_celda_leyenda,
					'total'=>$this->horario->totalHorasPorDia($dia_semana_recorrido)));
			$dia_semana_recorrido++;
		}

		return  $bloques_dia;
	}

	public function prepararBloquesHoras(){
		//determinar los bloques de estructura con coordenadas segun ancho y alto;
		$hora_recorrido=Carbon::today()->addHours(0);
		$final_dia=$hora_recorrido->copy();
		$final_dia->addDay();

	
		$bloques_hora=array();

		while($hora_recorrido->timestamp<$final_dia->timestamp){
			$hora_inicio=$hora_recorrido->copy();
			$hora_final=$hora_recorrido->copy();
			$hora_final->addHour();

			$texto=$hora_inicio->format('h:i a')." - ".$hora_final->format('h:i a');
			$y_ajustado=$this->alto_celda*($hora_recorrido->format('H')+1)+$this->alto_celda_leyenda/2;

			array_push($bloques_hora,
				(object)array('texto'=>$texto, 'tipo'=>'hora',
					'x'=>0,'y'=>$y_ajustado,
					'ancho'=>$this->ancho_celda_leyenda,'alto'=>$this->alto_celda));
			$hora_recorrido->addHour();
		}
		
		return  $bloques_hora;
	}

	public function prepararBloquesCelda(){

		//determinar los bloques de estructura con coordenadas segun ancho y alto;
		$dia_semana_recorrido=Carbon::SUNDAY;
		$bloques_celda=array();

		
		//BLOQUE -> COLUMNA
		while($dia_semana_recorrido<=Carbon::SATURDAY){
			$x_ajustado=$this->ancho_celda*$dia_semana_recorrido+$this->ancho_celda_leyenda;
			
			//HORA
			$hora_recorrido=Carbon::today()->addHours(0);
			$final_dia=$hora_recorrido->copy();
			$final_dia->addDay();


			while($hora_recorrido->timestamp<$final_dia->timestamp){
				$hora_inicio=$hora_recorrido->copy();
				$hora_final=$hora_recorrido->copy();
				$hora_final->addHour();

				$y_ajustado=$this->alto_celda*($hora_recorrido->format('H')+1)+$this->alto_celda_leyenda/2;

				array_push($bloques_celda,
					(object)array('texto'=>"",'tipo'=>'celda',
						'x'=>$x_ajustado,'y'=>$y_ajustado,
						'ancho'=>$this->ancho_celda,'alto'=>$this->alto_celda));
				$hora_recorrido->addHour();
			}

			$dia_semana_recorrido++;
		}


		//determinar los bloques de estructura con coordenadas segun ancho y alto;
		
		
		return  $bloques_celda;
	}

	public function prepararBloquesTurno(){
		$hora_inicio_dia=Carbon::today()->addHours(0);
		$hora_final_dia=Carbon::today()->addDay()->addHours(0);

		$time_al_dia=$hora_final_dia->timestamp-$hora_inicio_dia->timestamp;

		$y_alto_turnos=$this->alto-$this->alto_celda_leyenda;

		$y_escala=($time_al_dia/$y_alto_turnos);

		$turnos=$this->horario->turnos;

		$bloques_turno=array();
		foreach ($turnos as $i => $turno) {
			// dd("X");
			$hora_entrada=$turno->fecha_hora_entrada;
			$hora_final=$turno->fecha_hora_salida;

			$texto=$hora_entrada->format('h:i a')." a ".$hora_final->format('h:i a')."<br>(".$turno->tiempo().")";

			

			$y_posicion=($hora_entrada->timestamp-$hora_inicio_dia->timestamp)/$y_escala;//;
			$alto_turno=($hora_final->timestamp-$hora_entrada->timestamp)/$y_escala;// diferencia de tiempo en escala;

			$x_ajustado=$this->ancho_celda*$turno->dia_semana+$this->ancho_celda_leyenda;
			$y_ajustado=$y_posicion+$this->alto_celda_leyenda;

			// echo "<br>X:".$x_ajustado."-".$x_ajustado."<br>";
			// echo "Y:".$y_posicion."-".$y_ajustado."<br>";
			array_push($bloques_turno, (object)array('texto'=>$texto, 'tipo'=>'turno',
					'x'=>$x_ajustado,'y'=>$y_ajustado,
					'ancho'=>$this->ancho_celda,'alto'=>$alto_turno,'turno'=>$turno));

		}

		return $bloques_turno;
		return array();
	}


	public function preparar(){
		$this->calcularCelda();

		$bloques_dia=$this->prepararBloquesDia();

		$bloques_hora=$this->prepararBloquesHoras();

		$bloques_celda=$this->prepararBloquesCelda();

		$bloques_turno=$this->prepararBloquesTurno();

		return compact('bloques_dia','bloques_hora','bloques_celda','bloques_turno');
	}


}	