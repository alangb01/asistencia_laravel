<?php 

namespace App\Admin\Util;

use Carbon\Carbon;
/**
* 
*/
class Util
{
	
	public static function calcularTiempoDesdeCarbon(Carbon $fecha_hora_inicio,Carbon $fecha_hora_termino, $mostrar_cero=true){
		$intervalo=$fecha_hora_inicio->diff($fecha_hora_termino);

        // $intervalo->format("%Y %M %d %H %M ");
        $año=$intervalo->format("%Y");
        $mes=$intervalo->format("%m");
        $dia=$intervalo->format("%d");
        $hora=$intervalo->format("%H");
        $minuto=$intervalo->format("%i");
        $segundo=$intervalo->format("%s");

        $cadena="";
        if ($año>0) {
            $cadena.=" ".$año." año".($año>1?'s':'');
        }
        if ($mes>0) {
            $cadena.=" ".$mes." mes".($mes>1?'es':'');
        }
        if ($dia>0) {
            $cadena.=" ".$dia." dia".($dia>1?'s':'');
        }
        if ($hora>0) {
            $cadena.=" ".$hora."h";
        }
        if ($minuto>0) {
            $cadena.=" ".$minuto."m";
        }
        if ($segundo>0) {
            $cadena.=" ".$segundo."s";
        }

        $cadena=trim($cadena);
        if ($mostrar_cero && $cadena=="") {
            $cadena="0s";
        }elseif ($cadena=="") {
            $cadena="No se pudo calcular";;
        }

        return $cadena;
	}

    public static function calcularEnHorasDesdeCarbon(Carbon $fecha_hora_inicio,Carbon $fecha_hora_termino, $mostrar_cero=true){
        $intervalo=$fecha_hora_inicio->diff($fecha_hora_termino);

        // $intervalo->format("%Y %M %d %H %M ");
        // $año=$intervalo->format("%Y");
        // $mes=$intervalo->format("%m");
        $dia=$intervalo->format("%d");
        $hora=$intervalo->format("%H")+$dia*24;
        $minuto=$intervalo->format("%i");
        $segundo=$intervalo->format("%s");

     

        $cadena="";
        if ($hora>0) {
            $cadena=" ".$hora."h";
        }
        if ($minuto>0) {
            $cadena.=" ".$minuto."m";
        }
        if ($segundo>0) {
            $cadena.=" ".$segundo."s";
        }

        $cadena=trim($cadena);
        if ($mostrar_cero && $cadena=="") {
            $cadena="0s";
        }elseif ($cadena=="") {
            $cadena="No se pudo calcular";;
        }

        return $cadena;
    }

    public static function calcularEnHorasDesdeDiffTimestamp($segundos){
        $inicio=Carbon::createFromTimestamp(0);
        $final=Carbon::createFromTimestamp($segundos);
        return self::calcularEnHorasDesdeCarbon($inicio,$final);
    }

    public static function calcularTiempoEntreFechasHoras(Carbon $fecha_inicio,Carbon $fecha_termino, $mostrar_cero=true){
        return self::calcularTiempoDesdeCarbon($fecha_inicio,$fecha_termino,$mostrar_cero);
        
    }

    public static function calcularTiempoEntreFechas(Carbon $fecha_inicio,Carbon $fecha_termino, $mostrar_cero=true){
        $fecha_termino->addDay();
        return self::calcularTiempoDesdeCarbon($fecha_inicio,$fecha_termino,$mostrar_cero);
    }


   /* public  function calcularTiempoDesdeCarbon(Carbon $fecha_hora_inicio,Carbon $fecha_hora_termino, $mostrar_cero=true){
        return self::calcularTiempoDesdeCarbon($fecha_hora_inicio,$fecha_hora_termino,$mostrar_cero);
    }*/

    public static function crearCarbonDesdeParametros($fecha,$hora){
        $fecha_hora=Carbon::today();
        if ($fecha!="" && $hora!="") {
            $fecha_hora=Carbon::createFromFormat('Y-m-d H:i:s',$fecha." ".$hora);
        }elseif($fecha!="" && $hora==null){
            $fecha_hora=Carbon::createFromFormat('Y-m-d',$fecha);
        }elseif($fecha==null && $hora!=""){
            $fecha_hora=Carbon::createFromFormat('H:i:s',$hora);
        }
        return $fecha_hora;
    }

    public static function diaToString(Carbon $fecha){
        $dias=self::$DIAS;

        return $dias[$fecha->dayOfWeek];

    }

    public static function mesToString(Carbon $fecha){
        $meses=self::$MESES;

        return $meses[$fecha->month-1];

    }

     public static function fechaToString(Carbon $fecha){
        $año=$fecha->format("Y");
        $mes=self::mesToString($fecha);
        $dia=$fecha->format("d");

        return $dia."-".$mes."-".$año;

    }

    public static $DIAS=array('domingo','lunes','martes','miercoles','jueves','viernes','sabado');

    public static $MESES=array('enero','febrero','marzo','abril','mayo','junio','julio',
                'agosto','setiembre','octubre','noviembre','diciembre');

    public static function compararPeriodosDeTiempo(Periodo $periodo_1,Periodo $periodo_2){
        
    }
}