<?php

namespace App\Admin\Personal;

use Illuminate\Database\Eloquent\Model;

class Sucursal extends Model
{
    //
     const ESTADO_ACTIVO="AC";
    const ESTADO_INACTIVO="IN";

    protected $table="personal_sucursales";

    public function trabajadores($estado=null){
    	$consulta=$this->hasMany(Trabajador::class,'id','id_sucursal');

    	if ($estado!=null) {
			$consulta->where('estado','=',$estado);
    	}
    	return $consulta;
    }
}
