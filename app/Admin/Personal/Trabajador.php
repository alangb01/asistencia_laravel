<?php

namespace App\Admin\Personal;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Trabajador extends Model
{
    const SEXO_FEMENINO="2";
    const SEXO_MASCULINO="1";

    const ESTADO_ACTIVO="AC";
    const ESTADO_INACTIVO="IN";


    protected $table = 'personal_trabajadores';

    protected $id=['id'];

    protected $dates=['fecha_nacimiento'];

    public function cargo(){
    	return $this->hasOne(Cargo::class,"id","id_cargo");
    }

    public function sucursal(){
        return $this->belongsTo(Sucursal::class,"id","id_cargo");
    }

    public function scopeFiltrar($query,$request=null){
    	if (isset($request['estado']) && $request['estado']!="") {
            // echo "cargando estado<br>";
    		$query->where('estado','=',$request['estado']);
    	}


        //FILTRADO POR SUCURSAL Y NIVEL DE ACCESO

        $usuario=Usuario::logueado();
        $id_sucursal=$usuario->trabajador!=null ? $usuario->trabajador->id_sucursal:0;
        
        if ($usuario->nivel_acceso>=Usuario::NIVEL_GERENTE_GENERAL && $id_sucursal>0) {
            //echo "sucursal: ".$id_sucursal."<br>";
            $query->where('id_sucursal','=',$id_sucursal);
        }

        //campos validos 
        
        if (isset($request['orden']) && is_array($request['orden'])) {
        //     // echo "cargando orden<br>";
        //     $params=explode("-",$request['orden']);
        //     if (in_array($params[0],$campos_ordenacion_validos)) {
        //         $query->orderBy($params[0],$params[1]); 
        //     }
            $campos_ordenacion_validos=array('nombre','apellido','fecha_marcacion');
            
            foreach ($request['orden'] as $campo => $ordenacion) {
                $query->orderBy($campo,$ordenacion);
            }
        }

    	return $query;
    }

    public function __toString(){
    	return $this->apellido.", ".$this->nombre;;
    }
}
