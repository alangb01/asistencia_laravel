<?php

namespace App\Admin\Personal;

use Illuminate\Database\Eloquent\Model;

class Cargo extends Model
{
    //
     const ESTADO_ACTIVO="AC";
    const ESTADO_INACTIVO="IN";

    protected $table="personal_cargos";

    public function trabajadores(){
    	return $this->hasMany(Trabajador::class,'id_cargo','id');
    }

    public function __toString(){
    	return $this->nombre;
    }
}
