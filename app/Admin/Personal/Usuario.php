<?php

namespace App\Admin\Personal;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Support\Facades\Auth;

class Usuario extends Model
{
    //
    const NIVEL_ADMINISTRADOR_SUCURSAL=1;
    const NIVEL_GERENTE_GENERAL=2;

    protected $table="users";

    public function trabajador(){
    	return $this->belongsTo(Trabajador::class,'id_trabajador','id');
    }

   	public function __toString(){
   		if ($this->name!="") {
        return $this->name;
      }else if ($this->id_trabajador>0 && $this->trabajador!=null) {
   			return $this->trabajador->nombre;
   		}else{
   			return $this->name;
   		}
   		
   	}

   	public static function logueado(){
   		$usuario_logueado=Auth::user();

   		return Usuario::findOrFail($usuario_logueado->id);
   	}
}
