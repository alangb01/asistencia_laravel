<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/



Route::auth();

Route::get('/logout', ['as'=>'logout', function(){
    Auth::logout();
	Session::flush();
    return Redirect::to('/admin');
}]);

// Route::get('/admin', 'HomeController@index');
Route::get('/util/calcular-tiempo-fecha-hora', ['as'=>'util.calcular.tiempo--fecha-hora','uses'=>'HomeController@calcularTiempoDesdeFechaHora']);
Route::get('/util/calcular-tiempo-fecha', ['as'=>'util.calcular.tiempo--fecha','uses'=>'HomeController@calcularTiempoDesdeFecha']);
Route::get('/util/calcular-tiempo', ['as'=>'util.calcular.tiempo','uses'=>'HomeController@calcularTiempo']);

Route::get('/',['middleware'=>'auth','uses'=>'HomeController@index']);
Route::get('/admin',['middleware'=>'auth','uses'=>'HomeController@index']);

Route::group(['namespace'=>'Marcador','prefix'=>'marcador'], function () {
	Route::get('trabajador/buscar/{dni}',['as'=>'trabajador.buscar','uses'=>'TrabajadorController@buscar']);
	Route::get('trabajador/identificar',['as'=>'trabajador.identificar','uses'=>'TrabajadorController@cargarHuellas']);
	Route::get('trabajador/cargar/{id_trabajador}',['as'=>'trabajador.cargar','uses'=>'TrabajadorController@cargar']);
	Route::post('trabajador/huella/registrar',['as'=>'trabajador.huella.registrar','uses'=>'TrabajadorController@registrar']);
	Route::get('trabajador/marcar/{id_trabajador}',['as'=>'trabajador.marcacion.registrar','uses'=>'MarcacionController@store']);
});


Route::group(['namespace'=>'Admin','middleware'=>['auth'],'prefix'=>'admin'], function () {
	
	Route::group(['namespace'=>'Cuenta','prefix'=>'cuenta'],function(){
		Route::get('cambiar-clave',['as'=>'usuario.cambiar-clave','uses'=>'UsuarioController@cambiarClave']);
		Route::put('cambiar-clave',['as'=>'usuario.procesar-cambio-clave','uses'=>'UsuarioController@procesarCambioClave']);
		Route::get('editar-perfil',['as'=>'usuario.editar-perfil','uses'=>'UsuarioController@edit']);
		Route::put('editar-perfil',['as'=>'usuario.update-perfil','uses'=>'UsuarioController@update']);
	});

	// Route::get('/','HomeController@index');
    Route::group(['namespace'=>'Personal','middleware'=>['validar.sesion'],'prefix'=>'personal'], function () {

    	Route::resource('sucursal','SucursalController', ['names' => [
		    'index' => 'sucursal.listado',
		    'create' => 'sucursal.crear',
		    'show' => 'sucursal.mostrar',
		    'store'=>'sucursal.almacenar',
		    'edit'=>'sucursal.editar',
		    'update' => 'sucursal.actualizar',
		    'destroy' => 'sucursal.eliminar'
		]]);

    	Route::resource('cargo','CargoController', ['names' => [
		    'index' => 'cargo.listado',
		    'create' => 'cargo.crear',
		    'show' => 'cargo.mostrar',
		    'store'=>'cargo.almacenar',
		    'edit'=>'cargo.editar',
		    'update' => 'cargo.actualizar',
		    'destroy' => 'cargo.eliminar'
		]]);

		Route::resource('trabajador','TrabajadorController', ['names' => [
		    'index' => 'trabajador.listado',
		    'create' => 'trabajador.crear',
		    'show' => 'trabajador.mostrar',
		    'store'=>'trabajador.almacenar',
		    'edit'=>'trabajador.editar',
		    'update' => 'trabajador.actualizar',
		    'destroy' => 'trabajador.eliminar'
		]]);


		Route::get('usuario/{id_usuario}/forzar-cambio-clave',['as'=>'usuario.forzar.cambio.clave','uses'=>'UsuarioController@forzarCambioClave']);
		Route::get('usuario/{id_usuario}/resetear-clave',['as'=>'usuario.resetear.clave','uses'=>'UsuarioController@resetearClave']);

		Route::resource('usuario','UsuarioController', ['names' => [
		    'index' => 'usuario.listado',
		    'create' => 'usuario.crear',
		    'show' => 'usuario.mostrar',
		    'store'=>'usuario.almacenar',
		    'edit'=>'usuario.editar',
		    'update' => 'usuario.actualizar',
		    'destroy' => 'usuario.eliminar'
		]]);
	});
	
	Route::group(['namespace'=>'Asistencia','middleware'=>['validar.sesion'],'prefix'=>'asistencia'], function () {
		Route::resource('suspencion','SuspencionController', ['names' => [
		    'index' => 'suspencion.listado',
		    'create' => 'suspencion.crear',
		    'show' => 'suspencion.detalle',
		    'store'=>'suspencion.almacenar',
		    'edit'=>'suspencion.editar',
		    'update' => 'suspencion.actualizar',
		    'destroy' => 'suspencion.eliminar'
		]]);

		Route::get('reportes',['as'=>'reporte.trabajadores','uses'=>'ReporteController@index']);
		//Route::get('reporte/{id_trabajador}/anteriores',['as'=>'reporte.anteriores','uses'=>'ReporteController@historial']);
		Route::get('reporte/{id_trabajador}/vista-previa',['as'=>'reporte.preview','uses'=>'ReporteController@preview']);

		Route::post('reporte/{id_trabajador}/exportar/pdf',['as'=>'reporte.exportar.pdf','uses'=>'ReporteController@guardar']);
		Route::post('reporte/{id_trabajador}/exportar/excel',['as'=>'reporte.exportar.excel','uses'=>'ReporteController@exportarExcel']);
		
		// Route::get('reporte/{id_trabajador}/vista-previa/mes-anterior',['as'=>'reporte.preview.anterior','uses'=>'ReporteController@preview']);
		Route::put('reporte/{id_trabajador}/marcacion/{id_marcacion}',['as'=>'reporte.marcacion.cambiar','uses'=>'ReporteMarcadorController@cambiarMarcacion']);
		Route::delete('reporte/{id_trabajador}/marcacion/{id_marcacion}',['as'=>'reporte.marcacion.eliminar','uses'=>'ReporteMarcadorController@eliminarMarcacion']);

		Route::get('reporte/{id_trabajador}/marcador',['as'=>'reporte.marcacion.justificar','uses'=>'ReporteMarcadorController@marcador']);
		Route::post('reporte/{id_trabajador}/marcador',['as'=>'reporte.marcacion.almacenar','uses'=>'ReporteMarcadorController@procesarMarcacion']);

		// Route::get('reporte/{id_trabajador}/permiso-horas',['as'=>'reporte.permiso-horas.crear','uses'=>'ReportePermisoController@crearPermisoHoras']);
		// Route::post('reporte/{id_trabajador}/permiso-horas',['as'=>'reporte.permiso-horas.almacenar','uses'=>'ReportePermisoController@procesarPermisoHoras']);

		Route::get('reporte/{id_trabajador}/permiso',['as'=>'reporte.permiso-horas.crear','uses'=>'ReportePermisoController@crearPermisoHoras']);
		Route::post('reporte/{id_trabajador}/permiso',['as'=>'reporte.permiso-horas.almacenar','uses'=>'ReportePermisoController@procesarPermisoHoras']);

		Route::delete('reporte/{id_trabajador}/permiso/{id_permiso}/eliminar',['as'=>'reporte.permiso.eliminar','uses'=>'ReportePermisoController@eliminarPermiso']);

		Route::get('reporte/{id_trabajador}/sancion',['as'=>'reporte.sancion.crear','uses'=>'ReporteSancionController@crearSancion']);
		Route::post('reporte/{id_trabajador}/sancion',['as'=>'reporte.sancion.almacenar','uses'=>'ReporteSancionController@procesarSancion']);
		

		/*Route::resource('horario-personal/{id}/turnos/{id_horario}','TurnoController', ['names' => [
		    'index' => 'turno.listado',
		    // 'create' => 'horario.crear',
		    // 'show' => 'horario.mostrar',
		    'store'=>'turno.almacenar',
		    // 'edit'=>'horario.editar',
		    // 'update' => 'horario.actualizar',
		    // 'destroy' => 'horario.eliminar'
		]]);*/
		Route::get('horarios/{id_trabajador}',['as'=>'horario.listado','uses'=>'HorarioController@index']);
		Route::get('horarios/{id_trabajador}/clonar/{id_horario}',['as'=>'horario.clonar','uses'=>'HorarioController@clonar']);

		Route::get('horarios/{id_trabajador}/crear',['as'=>'horario.crear','uses'=>'HorarioController@create']);
		Route::post('horarios/{id_trabajador}/crear',['as'=>'horario.almacenar','uses'=>'HorarioController@store']);
		Route::get('horarios/{id_trabajador}/{id_horario}/editar',['as'=>'horario.editar','uses'=>'HorarioController@edit']);
		Route::put('horarios/{id_trabajador}/{id_horario}/editar',['as'=>'horario.actualizar','uses'=>'HorarioController@update']);
		Route::delete('horarios/{id_trabajador}/{id_horario}',['as'=>'horario.eliminar','uses'=>'HorarioController@destroy']);

		
		Route::get('horarios/{id_horario}/cronograma',['as'=>'turno.listado','uses'=>'HorarioTurnoController@index']);
		Route::put('horarios/{id_horario}/cambiar',['as'=>'turno.cambiar.dia','uses'=>'HorarioTurnoController@cambiar']);
		Route::get('horarios/{id_horario}/turno-dia-semana',['as'=>'turno.dia-semana.crear','uses'=>'HorarioTurnoController@crearTurnoDiaSemana']);
		Route::post('horario/{id_horario}/turno-dia-semana',['as'=>'turno.dia-semana.almacenar','uses'=>'HorarioTurnoController@storeTurnoDiaSemana']);
		Route::get('horarios/{id_horario}/turno-dia-semana/{id_turno}',['as'=>'turno.dia-semana.editar','uses'=>'HorarioTurnoController@editarTurnoDiaSemana']);
		Route::put('horario/{id_horario}/turno-dia-semana/{id_turno}',['as'=>'turno.dia-semana.actualizar','uses'=>'HorarioTurnoController@updateTurnoDiaSemana']);
		Route::delete('horario/{id_horario}/turno-dia-semana/{id_turno}',['as'=>'turno.dia-semana.eliminar','uses'=>'HorarioTurnoController@destroyTurnoDiaSemana']);

		Route::get('horarios/{id_horario}/turno-programado',['as'=>'turno.programado.crear','uses'=>'HorarioTurnoController@programado']);
		Route::post('horario/{id_horario}/turno-programado',['as'=>'turno.programado.almacenar','uses'=>'HorarioTurnoController@storeProgramado']);

		/*Route::resource('horario-personal/{id_trabajador}','HorarioController', ['names' => [
		    'index' => 'horario.listado',
		    // 'create' => 'horario.crear',
		    // 'show' => 'horario.mostrar',
		    'store'=>'horario.almacenar',
		    // 'edit'=>'horario.editar',
		    // 'update' => 'horario.actualizar',
		    // 'destroy' => 'horario.eliminar'
		]]);*/
		

		/*Route::resource('reporte/{id_trabajador}','ReporteController', ['names' => [
		    // 'index' => 'reporte.listado',
		    // 'create' => 'reporte.crear',
		    // 'show' => 'reporte.mostrar',
		    // 'store'=>'reporte.almacenar',
		    // 'edit'=>'reporte.editar',
		    'update' => 'reporte.actualizar',
		    'destroy' => 'reporte.eliminar'
		]]);*/

		// Route::put('reporte/{id}/vista-previa',['as'=>'reporte.preview','uses'=>'ReporteController@preview']);
		// Route::put('reporte/{id}/vista-previa',['as'=>'reporte.preview','uses'=>'ReporteController@preview']);

    	/*Route::resource('horario-general','ReporteController', ['names' => [
		    'index' => 'reporte.listado',
		    'create' => 'reporte.crear',
		    'show' => 'reporte.mostrar',
		    'store'=>'reporte.almacenar',
		    'edit'=>'reporte.editar',
		    'update' => 'reporte.actualizar',
		    'destroy' => 'reporte.eliminar'
		]]);

    	

		*/

		
	});
});



/**
personal/cargos
personal/trabajador
personal/usuarios
 

caja/cuentas
caja/cuenta/{}/ingreso
caja/cuenta/{}/salida
caja/cuenta/{}/cobranzas
caja/cuenta/{}/pagos

inventario/almacen
inventario/productos
inventario/productos
inventario/movimiento/entrada
inventario/movimiento/salida
*/

