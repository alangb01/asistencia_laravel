<?php

namespace App\Http\Controllers\Admin\Cuenta;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;


use View;
use Validator;
use Illuminate\Support\Facades\Auth;
use App\Admin\Personal\Usuario;

class UsuarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        //
        $usuario=Usuario::logueado();
        $data=compact('usuario');
        return View::make('admin.cuenta.usuario.perfil',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
        $usuario=Usuario::logueado();

        $validator = Validator::make($request->all(), [
            // 'id_trabajador' => 'required',
            'name' => 'required|max:50|unique:users,id,:id',
            'email' => 'required|email|unique:users,id,:id',
            'password' => 'required'
        ]);
        //si no cumple las validaciones, regresa
        if ($validator->fails()) {
            return redirect()->route('usuario.editar-perfil')
                        ->withErrors($validator)
                        ->withInput();
        }


        if (!\Hash::check($request->input('password'),$usuario->password)) {
            $mensaje=array('tipo'=>'warning','mensaje'=>'Debe ingresar la contraseña actual');
            return redirect()->route('usuario.editar-perfil')
                        ->with('notificacion',$mensaje)
                        ->withInput();
        }

        // $usuario=Auth::user();
        // $usuario->id_trabajador=$request->input('id_trabajador');
        $usuario->name=$request->input('name');
        $usuario->email=$request->input('email');
        // $usuario->password=bcrypt($request->input('password'));
        // $usuario->forzar_cambio_clave=0;
       
        if ($usuario->save()) {
            $mensaje=array('tipo'=>'success','mensaje'=>'Actualizado con exito');
            return redirect()->route('usuario.listado')->with('notificacion',$mensaje);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function cambiarClave(){
        // dd("x");
        $usuario=Usuario::logueado();

        $usuario->action_form=route('usuario.procesar-cambio-clave');
        // dd($usuario);
        $data=compact('usuario');
        return View::make('admin.cuenta.usuario.cambiar-clave',$data);
    }

    public function procesarCambioClave(Request $request){


        $validator = Validator::make($request->all(), [
            // 'id_trabajador' => 'required',
            // 'name' => 'required|max:50|unique:users,id,:id',
            // 'email' => 'required|email|unique:users,id,:id',
            'password' => 'required|min:6|confirmed',
            'password_confirmation' => 'required|min:6',
        ]);
        //si no cumple las validaciones, regresa
        if ($validator->fails()) {
            return redirect()->route('usuario.cambiar-clave')
                        ->withErrors($validator)
                        ->withInput();
        }

       

        $usuario=Usuario::logueado();
        // $usuario->id_trabajador=$request->input('id_trabajador');
        $usuario->name=$request->input('name');
        // $usuario->email=$request->input('email');
        $usuario->password=bcrypt($request->input('password'));
        $usuario->forzar_cambio_clave=0;
       
        if ($usuario->save()) {
            $mensaje=array('tipo'=>'success','mensaje'=>'Actualizado con exito');
            return redirect()->route('usuario.listado')->with('notificacion',$mensaje);
        }

    }
}
