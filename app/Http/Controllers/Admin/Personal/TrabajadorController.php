<?php

namespace App\Http\Controllers\Admin\Personal;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;

use App\Admin\Personal\Trabajador;
use App\Admin\Personal\Cargo;
use App\Admin\Personal\Sucursal;

use Carbon\Carbon;

use Validator;


class TrabajadorController extends Controller
{
    const DIR_IMAGENES="personal/trabajador/";
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $url_nuevo=route('trabajador.crear');

        $filtro=array('buscar'=>$request->get('buscar'));
        $listado_trabajadores=Trabajador::filtrar($filtro)->orderBy('estado','asc')->orderBy('apellido','asc')->paginate(10);

        $fecha_actual=Carbon::now();
        foreach ($listado_trabajadores as $id => $trabajador) {
            $trabajador->edad_actual=$trabajador->fecha_nacimiento->diffInYears($fecha_actual);

            $trabajador->id_listado=$id+1+($listado_trabajadores->currentPage()-1)*$listado_trabajadores->perPage();
            $trabajador->url_editar=route('trabajador.editar',$trabajador->id);
            $trabajador->url_eliminar=route('trabajador.eliminar',$trabajador->id);
        }
        

        $data=compact('listado_trabajadores','url_nuevo');
        return View::make('admin.personal.trabajador.listado',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        

        $trabajador=new Trabajador();
        $trabajador->action_form=route('trabajador.almacenar');
        $trabajador->fecha_nacimiento=Carbon::today();
        $url_volver=route('trabajador.listado');

        $cargos_activos=Cargo::where('estado','=','AC')->orderBy('nombre','asc')->get();
        $sucursales_activas=Sucursal::where('estado','=','AC')->orderBy('nombre','asc')->get();

        $estados_disponibles=collect();
        $estados_disponibles->push((object)array('id'=>Trabajador::ESTADO_ACTIVO,'nombre'=>"Activo"));
        $estados_disponibles->push((object)array('id'=>Trabajador::ESTADO_INACTIVO,'nombre'=>"Inactivo"));

        $data=compact('trabajador','cargos_activos','sucursales_activas','estados_disponibles','url_volver');   
        return View::make('admin.personal.trabajador.formulario',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $ruta_foto=$this->almacenarImagen($request);
        $validator = Validator::make($request->all(), [
            'dni' => 'required|unique:personal_trabajadores,id,:id',
            'nombre' => 'required|max:50',
            'apellido' => 'required|max:50',
            // 'sexo' => 'required',
            // 'fecha_nacimiento' => 'required|date',
            // 'correo_personal' => 'required|email'
            // 'telefono_fijo' => 'required|min:6',
            // 'telefono_celular' => 'required|min:6',
            // 'direccion' => 'required|min:2',
            'id_cargo' => 'required',
            'id_sucursal' => 'required',
            'estado' => 'required',
        ]);

        //si no cumple las validaciones, regresa
        if ($validator->fails()) {
            return redirect()->route('trabajador.crear')
                        ->withErrors($validator)
                        ->withInput();
        }


        $trabajador=new Trabajador();
        $trabajador->id_sucursal=$request->input('id_sucursal');
        $trabajador->id_cargo=$request->input('id_cargo');
        $trabajador->dni=$request->input('dni');
        $trabajador->nombre=$request->input('nombre');
        $trabajador->apellido=$request->input('apellido');
        $trabajador->sexo=$request->input('sexo');
        $trabajador->fecha_nacimiento=$request->input('fecha_nacimiento');
        $trabajador->direccion=$request->input('direccion');
        $trabajador->telefono_fijo=$request->input('telefono_fijo');
        $trabajador->telefono_celular=$request->input('telefono_celular');
        $trabajador->correo_personal=$request->input('correo_personal');
        $trabajador->ruta_foto=$ruta_foto!=""?$ruta_foto:$request->input('ruta_foto_actual');
        $trabajador->estado='AC';
        if ($trabajador->save()) {
            $mensaje=array('tipo'=>'success','mensaje'=>'Guardado con exito');
            return redirect()->route('trabajador.listado')->with('notificacion',$mensaje);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
       
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        

        $trabajador=Trabajador::findOrFail($id);
        $trabajador->action_form=route('trabajador.actualizar',$id);
        $url_volver=route('trabajador.listado');

        $this->cargarImagen($trabajador);

        $cargos_activos=Cargo::where('estado','=','AC')->orderBy('nombre','asc')->get();
        $sucursales_activas=Sucursal::where('estado','=','AC')->orderBy('nombre','asc')->get();

         $estados_disponibles=collect();
        $estados_disponibles->push((object)array('id'=>Trabajador::ESTADO_ACTIVO,'nombre'=>"Activo"));
        $estados_disponibles->push((object)array('id'=>Trabajador::ESTADO_INACTIVO,'nombre'=>"Inactivo"));

        foreach ($cargos_activos as $id => $cargo) {
            $cargo->selected=$trabajador->id_cargo==$cargo->id?'selected':'';    
        }

        foreach ($sucursales_activas as $id => $sucursal) {
            $sucursal->selected=$trabajador->id_sucursal==$sucursal->id?'selected':'';    
        }

        $data=compact('trabajador','cargos_activos','sucursales_activas','estados_disponibles','url_volver');   
        return View::make('admin.personal.trabajador.formulario',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {   
         // dd("xx");
        $ruta_foto=$this->almacenarImagen($request);
        $validator = Validator::make($request->all(), [
            'dni' => 'required|unique:personal_trabajadores,id,:id',
            'nombre' => 'required|max:50',
            'apellido' => 'required|max:50',
            // 'sexo' => 'required',
            // 'fecha_nacimiento' => 'required|date',
            // 'correo_personal' => 'required|email'
            // 'telefono_fijo' => 'required|min:6',
            // 'telefono_celular' => 'required|min:6',
            // 'direccion' => 'required|min:2',
            'id_cargo' => 'required',
            'id_sucursal' => 'required',
            'estado' => 'required',
        ]);

        //si no cumple las validaciones, regresa
        if ($validator->fails()) {
            // dd($validator);
            return redirect()->route('trabajador.editar',[$id])
                        ->withErrors($validator)
                        ->withInput();
        }


        
        $trabajador=Trabajador::findOrFail($id);
        $trabajador->id_sucursal=$request->input('id_sucursal');
        $trabajador->id_cargo=$request->input('id_cargo');
        $trabajador->dni=$request->input('dni');
        $trabajador->nombre=$request->input('nombre');
        $trabajador->apellido=$request->input('apellido');
        $trabajador->sexo=$request->input('sexo');
        $trabajador->fecha_nacimiento=$request->input('fecha_nacimiento');
        $trabajador->direccion=$request->input('direccion');
        $trabajador->telefono_fijo=$request->input('telefono_fijo');
        $trabajador->telefono_celular=$request->input('telefono_celular');
        $trabajador->correo_personal=$request->input('correo_personal');
        //$trabajador->ruta_foto=$request->input('ruta_foto_actual');
        //$this->almacenarImagen($trabajador,$request);
        $trabajador->ruta_foto=$ruta_foto!=""?$ruta_foto:$request->input('ruta_foto_actual');
        $trabajador->estado=$request->input('estado');
        if ($trabajador->save()) {
            $mensaje=array('tipo'=>'success','mensaje'=>'Actualizado con exito');
            return redirect()->route('trabajador.listado')->with('notificacion',$mensaje);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        \DB::beginTransaction();

        $trabajador=Trabajador::findOrFail($id);
        if ($trabajador->delete()) {
            $this->eliminarImagen($trabajador);

            \DB::commit();
            $mensaje=array('tipo'=>'success','mensaje'=>'Eliminado con exito');
            return redirect()->route('trabajador.listado')->with('notificacion',$mensaje);
        }

        \DB::rollback();
    }

    private function almacenarImagen($request){
        if ($request->file('ruta_foto')!=null && $request->file('ruta_foto')->isValid()) {
            $file = $request->file('ruta_foto');
            $nombre = $file->getClientOriginalName();
            \Storage::disk('local')->put(self::DIR_IMAGENES.$nombre,\File::get($file));
            return $nombre;
        }
        return "";
    }

    private function cargarImagen($trabajador){
        $archivo_imagen=self::DIR_IMAGENES.$trabajador->ruta_foto;
        if (trim($trabajador->ruta_foto)!="" && \Storage::disk('local')->exists($archivo_imagen)) {
            $contenido=\Storage::disk('local')->get($archivo_imagen);
            \Storage::disk('url')->put($archivo_imagen,$contenido);
            $trabajador->url_foto=asset($archivo_imagen);
        }
    }

    private function eliminarImagen($trabajador){
        $archivo_imagen=self::DIR_IMAGENES.$trabajador->ruta_foto;
        if (trim($trabajador->ruta_foto)!="" && \Storage::disk('local')->exists($archivo_imagen)) {
            // $contenido=\Storage::disk('local')->get($archivo_imagen);
            \Storage::disk('local')->delete($archivo_imagen);
            return true;
            // \Storage::disk('url')->put($archivo_imagen,$contenido);
            // $trabajador->url_foto=asset($archivo_imagen);
        }
    }
}
