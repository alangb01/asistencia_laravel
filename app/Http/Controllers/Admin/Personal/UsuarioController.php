<?php

namespace App\Http\Controllers\Admin\Personal;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\View;
use Validator;
use Illuminate\Support\Facades\Input;

use App\Admin\Personal\Usuario;
use App\Admin\Personal\Trabajador;

class UsuarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $url_nuevo=route('usuario.crear');
        $listado_usuarios=Usuario::paginate(10);

        // $fecha_actual=Carbon::now();
        foreach ($listado_usuarios as $id => $usuario) {
            // $usuario->url_permisos=route('permiso.listado',$usuario->id);

            $usuario->url_forzar_cambio_clave=route('usuario.forzar.cambio.clave',$usuario->id);
            $usuario->url_resetear_clave=route('usuario.resetear.clave',$usuario->id);

            $usuario->url_editar=route('usuario.editar',$usuario->id);
            $usuario->url_eliminar=route('usuario.eliminar',$usuario->id);
        }
        

        $data=compact('listado_usuarios','url_nuevo');
        return View::make('admin.personal.usuario.listado',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $usuario=new Usuario();
        $usuario->action_form=route('usuario.almacenar');
        $url_volver=route('usuario.listado');

        $trabajadores_activos=Trabajador::orderBy('apellido','asc')->get();
        foreach ($trabajadores_activos as $id => $trabajador) {
            $trabajador->selected=$trabajador->id==$usuario->id_trabajador?'selected':'';    
        }

        $data=compact('usuario','trabajadores_activos','url_volver');   
        return View::make('admin.personal.usuario.formulario',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        if(Usuario::logueado()->nivel_acceso==Usuario::NIVEL_GERENTE_GENERAL){
            $reglas_validacion=[
                // 'id_trabajador' => 'required',
                'name' => 'required|max:50|unique:users',
                'email' => 'required|email|unique:users',
                // 'password' => 'required|min:6|confirmed',
                // 'password_confirmation' => 'required|min:6',
            ];
        }else{
            $reglas_validacion=[
                'id_trabajador' => 'required',
                'name' => 'required|max:50|unique:users',
                'email' => 'required|email|unique:users',
                // 'password' => 'required|min:6|confirmed',
                // 'password_confirmation' => 'required|min:6',
            ];
        }

        $validator = Validator::make($request->all(),$reglas_validacion);

        //si no cumple las validaciones, regresa
        if ($validator->fails()) {
            return redirect()->route('usuario.crear')
                        ->withErrors($validator)
                        ->withInput();
        }

        $usuario=new Usuario();
        $usuario->id_trabajador=$request->input('id_trabajador');
        $usuario->name=$request->input('name');
        $usuario->email=$request->input('email');
        $usuario->password=bcrypt($request->input('email'));
       
        if ($usuario->save()) {
            $mensaje=array('tipo'=>'success','mensaje'=>'Guardado con exito');
            return redirect()->route('usuario.listado')->with('notificacion',$mensaje);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $usuario=Usuario::findOrFail($id);
        $usuario->action_form=route('usuario.actualizar',$id);
        $url_volver=route('usuario.listado');

       

        $trabajadores_activos=Trabajador::where('estado','=','')->orderBy('apellido','asc')->get();

        foreach ($trabajadores_activos as $id => $trabajador) {
            $trabajador->selected=$trabajador->id==$usuario->id_trabajador?'selected':'';    
        }


        $data=compact('usuario','trabajadores_activos','url_volver');   
        return View::make('admin.personal.usuario.formulario',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
       if(Usuario::logueado()->nivel_acceso==Usuario::NIVEL_GERENTE_GENERAL){
            $reglas_validacion=[
                // 'id_trabajador' => 'required',
                'name' => 'required|max:50|unique:users',
                'email' => 'required|email|unique:users',
                // 'password' => 'required|min:6|confirmed',
                // 'password_confirmation' => 'required|min:6',
            ];
        }else{
            $reglas_validacion=[
                'id_trabajador' => 'required',
                'name' => 'required|max:50|unique:users',
                'email' => 'required|email|unique:users',
                // 'password' => 'required|min:6|confirmed',
                // 'password_confirmation' => 'required|min:6',
            ];
        }

        $validator = Validator::make($request->all(),$reglas_validacion);
        //si no cumple las validaciones, regresa
        if ($validator->fails()) {
            return redirect()->route('usuario.editar',[$id])
                        ->withErrors($validator)
                        ->withInput();
        }

        $usuario=Usuario::findOrFail($id);
        $usuario->id_trabajador=$request->input('id_trabajador');
        $usuario->name=$request->input('name');
        $usuario->email=$request->input('email');
        // $usuario->password=bcrypt($request->input('password'));
       
        if ($usuario->save()) {
            $mensaje=array('tipo'=>'success','mensaje'=>'Actualizado con exito');
            return redirect()->route('usuario.listado')->with('notificacion',$mensaje);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $usuario=Usuario::findOrFail($id);
        if ($usuario->delete()) {
            $mensaje=array('tipo'=>'success','mensaje'=>'Eliminado con exito');
            return redirect()->route('usuario.listado')->with('notificacion',$mensaje);
        }
    }

    public function forzarCambioClave(Request $request,$id_usuario){
        $usuario=Usuario::findOrFail($id_usuario);
        $usuario->forzar_cambio_clave=1;

        if ($usuario->save()) {
            $mensaje=array('tipo'=>'success','mensaje'=>'Guardado con exito');
            return redirect()->route('usuario.listado',$id_usuario)->with('notificacion',$mensaje);   
        }
    }

    public function resetearClave(Request $request,$id_usuario){
        $usuario=Usuario::findOrFail($id_usuario);
        $usuario->forzar_cambio_clave=1;
        $usuario->password=bcrypt($usuario->email);
        $usuario->remember_token=null;
        if ($usuario->save()) {
            $mensaje=array('tipo'=>'success','mensaje'=>'Guardado con exito');
            return redirect()->route('usuario.listado',$id_usuario)->with('notificacion',$mensaje);   
        }
    }
}
