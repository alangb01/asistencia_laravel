<?php

namespace App\Http\Controllers\Admin\Personal;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Admin\Personal\Sucursal;

use View;
use Validator;


class SucursalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $url_nuevo=route('sucursal.crear');
        $listado_sucursales=Sucursal::paginate(10);

        foreach ($listado_sucursales as $id => $sucursal) {
            $sucursal->id_listado=$id+1+($listado_sucursales->currentPage()-1)*$listado_sucursales->perPage();
            $sucursal->url_editar=route('sucursal.editar',$sucursal->id);
            $sucursal->url_eliminar=route('sucursal.eliminar',$sucursal->id);
        }
        

        $data=compact('listado_sucursales','url_nuevo');
        return View::make('admin.personal.sucursal.listado',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $sucursal=new Sucursal();
        $sucursal->action_form=route('sucursal.almacenar');
        $url_volver=route('sucursal.listado');

        //si hay inputs enviados
        // dd(Input::get('nombre'));
        // dd($validator);
        $estados_disponibles=collect();
        $estados_disponibles->push((object)array('id'=>Sucursal::ESTADO_ACTIVO,'nombre'=>"Activo"));
        $estados_disponibles->push((object)array('id'=>Sucursal::ESTADO_INACTIVO,'nombre'=>"Inactivo"));


        $data=compact('sucursal','url_volver','estados_disponibles');   
        return View::make('admin.personal.sucursal.formulario',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validator = Validator::make($request->all(), [
            'nombre' => 'required|max:50',
            'direccion' => 'required|max:120'
        ]);

        //si no cumple las validaciones, regresa
        if ($validator->fails()) {
            return redirect()->route('sucursal.crear')
                        ->withErrors($validator)
                        ->withInput();
        }

        $sucursal=new Sucursal();
        $sucursal->nombre=$request->input('nombre');
        $sucursal->direccion=$request->input('direccion');
        $sucursal->telefono=$request->input('telefono');
        $sucursal->estado=$request->input('estado');
        if ($sucursal->save()) {
            $mensaje=array('tipo'=>'success','mensaje'=>'Guardado con exito');
            return redirect()->route('sucursal.listado')->with('notificacion',$mensaje);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $sucursal=Sucursal::findOrFail($id);    
        
        $sucursal->action_form=route('sucursal.actualizar',$id);

        $estados_disponibles=collect();
        $estados_disponibles->push((object)array('id'=>Sucursal::ESTADO_ACTIVO,'nombre'=>"Activo"));
        $estados_disponibles->push((object)array('id'=>Sucursal::ESTADO_INACTIVO,'nombre'=>"Inactivo"));


        $url_volver=route('sucursal.listado');

        $data=compact('sucursal','url_volver','estados_disponibles');   
        return View::make('admin.personal.sucursal.formulario',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $validator = Validator::make($request->all(), [
            'nombre' => 'required|max:50',
            'direccion' => 'required|max:120'
        ]);

        //si no cumple las validaciones, regresa
        if ($validator->fails()) {
            return redirect()->route('sucursal.editar',[$id])
                        ->withErrors($validator)
                        ->withInput();
        }

        //
        $sucursal=Sucursal::find($id);
        $sucursal->nombre=$request->input('nombre');
        $sucursal->direccion=$request->input('direccion');
        $sucursal->telefono=$request->input('telefono');
        $sucursal->estado=$request->input('estado');
        if ($sucursal->save()) {
            $mensaje=array('tipo'=>'success','mensaje'=>'Actualizado con exito');
            return redirect()->route('sucursal.listado')->with('notificacion',$mensaje);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $sucursal=Sucursal::findOrFail($id);

        if ($sucursal->delete()) {
            $mensaje=array('tipo'=>'success','mensaje'=>'Eliminado con exito');
        }else{
            $mensaje=array('tipo'=>'warning','mensaje'=>'No se pudo eliminar');
        }
        return redirect()->route('sucursal.listado')->with('notificacion',$mensaje);
    }
}
