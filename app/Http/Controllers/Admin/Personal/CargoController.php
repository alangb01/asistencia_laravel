<?php

namespace App\Http\Controllers\Admin\Personal;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Input;
use Validator;

use App\Admin\Personal\Cargo;

class CargoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $url_nuevo=route('cargo.crear');
        $listado_cargos=Cargo::paginate(10);

        foreach ($listado_cargos as $id => $cargo) {
            $cargo->id_listado=$id+1+($listado_cargos->currentPage()-1)*$listado_cargos->perPage();
            $cargo->url_editar=route('cargo.editar',$cargo->id);
            $cargo->url_eliminar=route('cargo.eliminar',$cargo->id);
        }
        

        $data=compact('listado_cargos','url_nuevo');
        return View::make('admin.personal.cargo.listado',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //
        $cargo=new Cargo();
        $cargo->action_form=route('cargo.almacenar');
        $url_volver=route('cargo.listado');

        //si hay inputs enviados
        // dd(Input::get('nombre'));
        // dd($validator);
         $estados_disponibles=collect();
        $estados_disponibles->push((object)array('id'=>Cargo::ESTADO_ACTIVO,'nombre'=>"Activo"));
        $estados_disponibles->push((object)array('id'=>Cargo::ESTADO_INACTIVO,'nombre'=>"Inactivo"));


        $data=compact('cargo','url_volver','estados_disponibles');   
        return View::make('admin.personal.cargo.formulario',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $validator = Validator::make($request->all(), [
            'nombre' => 'required|max:50',
        ]);

        //si no cumple las validaciones, regresa
        if ($validator->fails()) {
            return redirect()->route('cargo.crear')
                        ->withErrors($validator)
                        ->withInput();
        }

        $cargo=new Cargo();
        $cargo->nombre=$request->input('nombre');
        $cargo->estado=$request->input('estado');
       
        if ($cargo->save()) {
            $mensaje=array('tipo'=>'success','mensaje'=>'Guardado con exito');
            return redirect()->route('cargo.listado')->with('notificacion',$mensaje);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
       
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //  
        try {
            $cargo=Cargo::findOrFail($id);    
        } catch (ModelNotFoundException $e) {
            dd($e);
        }

        
        $cargo->action_form=route('cargo.actualizar',$id);
        $url_volver=route('cargo.listado');

        $estados_disponibles=collect();
        $estados_disponibles->push((object)array('id'=>Cargo::ESTADO_ACTIVO,'nombre'=>"Activo"));
        $estados_disponibles->push((object)array('id'=>Cargo::ESTADO_INACTIVO,'nombre'=>"Inactivo"));


        $data=compact('cargo','url_volver','estados_disponibles');   
        return View::make('admin.personal.cargo.formulario',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'nombre' => 'required|max:50'
        ]);

        //si no cumple las validaciones, regresa
        if ($validator->fails()) {
            return redirect()->route('cargo.editar',[$id])
                        ->withErrors($validator)
                        ->withInput();
        }

        //
        $cargo=Cargo::find($id);
        $cargo->nombre=$request->input('nombre');
        $cargo->estado=$request->input('estado');
        if ($cargo->save()) {
            $mensaje=array('tipo'=>'success','mensaje'=>'Actualizado con exito');
            return redirect()->route('cargo.listado')->with('notificacion',$mensaje);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $cargo=Cargo::findOrFail($id);

        if ($cargo->delete()) {
            $mensaje=array('tipo'=>'success','mensaje'=>'Eliminado con exito');
        }else{
            $mensaje=array('tipo'=>'warning','mensaje'=>'No se pudo eliminar');
        }
        return redirect()->route('cargo.listado')->with('notificacion',$mensaje);
    }
}
