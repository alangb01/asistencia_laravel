<?php

namespace App\Http\Controllers\Admin\Asistencia;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\View;
// use Illuminate\Support\Facades\Input;
use Validator;
use Carbon\Carbon;

use Fpdf;
// use PDF;
// use Barryvdh\DomPDF\Facade as PDF;

use App\Admin\Asistencia\Reporte;
use App\Admin\Asistencia\Trabajador;
use App\Admin\Asistencia\Marcacion;
use App\Admin\Asistencia\Permiso;
use App\Admin\Asistencia\Sancion;

class ReporteSancionController extends Controller
{
    public function crearSancion(Request $request,$id_trabajador){
        $trabajador=Trabajador::findOrFail($id_trabajador);
        // $
        // $trabajador->action_form=route('reporte.marcacion.almacenar',$id_trabajador);
        $sancion=new Sancion();
        $sancion->id_trabajador=$id_trabajador;
        $sancion->fecha_hora_inicio=Carbon::today()->addHours(0);
        $sancion->fecha_hora_termino=Carbon::today()->addHours(0);//->addHours(23)->addMinutes(59)->addSeconds(59);

        $sancion->action_form=route('reporte.sancion.almacenar',$id_trabajador);


        $url_volver=route('reporte.preview',$id_trabajador);

        $data=compact('sancion','url_volver');
        return View::make('admin.asistencia.reporte.sancion',$data);
    }

    public function procesarSancion(Request $request,$id_trabajador){
        $validator = Validator::make($request->all(), [
            // 'id_trabajador' => 'required',
            'fecha_inicio'=>'required|date',
            'hora_inicio'=>'required',
            'fecha_termino'=>'required|date',
            'hora_termino'=>'required',
            'descripcion'=>'required',
        ]);

        //si no cumple las validaciones, regresa
        if ($validator->fails()) {
            return redirect()->route('reporte.sancion.crear',[$id_trabajador])
                        ->withErrors($validator)
                        ->withInput();
        }


        $fecha_hora_inicio=Carbon::createFromFormat('Y-m-d H:i:s',$request->input('fecha_inicio')." 00:00:00");
        $fecha_hora_termino=Carbon::createFromFormat('Y-m-d H:i:s',$request->input('fecha_termino')." 00:00:00");


        $sancion=new Sancion();
        $sancion->id_trabajador=$request->input('id_trabajador');
        $sancion->fecha_hora_inicio=$fecha_hora_inicio;
        $sancion->fecha_hora_termino=$fecha_hora_termino;
        $sancion->descripcion=$request->input('descripcion');

        if ($sancion->save()) {
            return redirect()->route('reporte.preview',[$id_trabajador]);
        }
    }

}
