<?php

namespace App\Http\Controllers\Admin\Asistencia;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use View;
use Carbon\Carbon;

use App\Admin\Asistencia\Horario;
use App\Admin\Asistencia\Turno;


class HorarioTurnoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id_horario)
    {
        //
        $horario=Horario::find($id_horario);
        $horario->url_cambiar_dia=route('turno.cambiar.dia',[$id_horario]);

        foreach ($horario->turnos as $id => $turno) {
            $turno->url_editar=route('turno.dia-semana.editar',[$id_horario,$turno->id]);
            $turno->url_eliminar=route('turno.dia-semana.eliminar',[$id_horario,$turno->id]);
        }

        $dias_semana=collect();
        $dias_semana->push((object)array('id'=>Carbon::SUNDAY,'texto'=>'domingo'));
        $dias_semana->push((object)array('id'=>Carbon::MONDAY,'texto'=>'lunes'));
        $dias_semana->push((object)array('id'=>Carbon::TUESDAY,'texto'=>'martes'));
        $dias_semana->push((object)array('id'=>Carbon::WEDNESDAY,'texto'=>'miercoles'));
        $dias_semana->push((object)array('id'=>Carbon::THURSDAY,'texto'=>'jueves'));
        $dias_semana->push((object)array('id'=>Carbon::FRIDAY,'texto'=>'viernes'));
        $dias_semana->push((object)array('id'=>Carbon::SATURDAY,'texto'=>'sabado'));

        // $horario->action_form=route('turno.almacenar',[$id_horario]);
        $url_nuevo_turno_dia_semana=route('turno.dia-semana.crear',[$id_horario]);
        $url_nuevo_turno_programado=route('turno.programado.crear',[$id_horario]);
        $url_volver=route('horario.listado',$horario->id_trabajador);

        $data=compact('horario','url_nuevo_turno_dia_semana','url_nuevo_turno_programado','url_volver','dias_semana');
        return View::make('admin.asistencia.horario.cronograma',$data);
    }

    public function cambiar(Request $request,$id_horario){
        $dia_semana_actual=$request->input('dia_semana_actual');
        $dia_semana_nuevo=$request->input('dia_semana_nuevo');

        if ($request->input('dia_semana_actual')==null || $request->input('dia_semana_actual')) {
            $mensaje=array('tipo'=>'warning','mensaje'=>'Debe especificar dia actual y nuevo dia');
            return redirect()->route('turno.listado',[$id_horario])->with('notificacion',$mensaje);
        }


        // dd($dia_semana_actual." y ".$dia_semana_nuevo);

        if ($dia_semana_nuevo<0 && $dia_semana_nuevo>7 || $dia_semana_actual<0 && $dia_semana_actual>7) {
            $mensaje=array('tipo'=>'warning','mensaje'=>'Debe seleccionar los campos necesarios');
            return redirect()->route('turno.cronograma',[$id_horario])->with('notificacion',$mensaje);
        }else if($dia_semana_actual==$dia_semana_nuevo){
            $mensaje=array('tipo'=>'warning','mensaje'=>'Debe seleccionar dias diferentes');
            return redirect()->route('turno.cronograma',[$id_horario])->with('notificacion',$mensaje);
        }

        $horario=Horario::findOrFail($id_horario);

        foreach ($horario->turnos->all() as $id => $turno) {
            if ($turno->dia_semana==$dia_semana_actual) {
                $turno->dia_semana=$dia_semana_nuevo;

                if (!$horario->existeCruceDeTurnos($turno)) {
                    $turno->update();
                }
            }
        }

        $mensaje=array('tipo'=>'success','mensaje'=>'Cambio de turnos realizado con exito');
        return redirect()->route('turno.listado',[$id_horario])->with('notificacion',$mensaje);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id_horario)
    {
        //
        // $horario=Turno::findOrFail($id_horario);
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function crearTurnoDiaSemana($id_horario){
        $turno=new Turno();
        $turno->id_horario=$id_horario;
        $turno->fecha_hora_entrada=Carbon::today()->addHours(8);
        $turno->fecha_hora_salida=Carbon::today()->addHours(13);
        $turno->dia_semana=-1;//Carbon::today()->dayOfWeek;
        $turno->action_form=route('turno.dia-semana.almacenar',[$id_horario]);
        $url_volver=route('turno.listado',$id_horario);

        $dias_semana=collect();
        $dias_semana->push((object)array('id'=>Carbon::SUNDAY,'texto'=>'domingo'));
        $dias_semana->push((object)array('id'=>Carbon::MONDAY,'texto'=>'lunes'));
        $dias_semana->push((object)array('id'=>Carbon::TUESDAY,'texto'=>'martes'));
        $dias_semana->push((object)array('id'=>Carbon::WEDNESDAY,'texto'=>'miercoles'));
        $dias_semana->push((object)array('id'=>Carbon::THURSDAY,'texto'=>'jueves'));
        $dias_semana->push((object)array('id'=>Carbon::FRIDAY,'texto'=>'viernes'));
        $dias_semana->push((object)array('id'=>Carbon::SATURDAY,'texto'=>'sabado'));

        $data=compact('turno','url_volver','dias_semana');
        return View::make('admin.asistencia.horario.turno-dia-semana',$data);
    }

    public function editarTurnoDiaSemana($id_horario,$id_turno){
        $turno=Turno::findOrFail($id_turno);
        $turno->timeEnCarbon();
        $turno->url_eliminar=route('turno.dia-semana.eliminar',[$id_horario,$turno->id]);
        // $turno->id_horario=$id_horario;
        // $turno->fecha_hora_entrada=Carbon::today()->addHours(8);
        // $turno->fecha_hora_salida=Carbon::today()->addHours(13);
        // $turno->dia_semana=Carbon::today()->dayOfWeek;
        $turno->action_form=route('turno.dia-semana.actualizar',[$id_horario,$id_turno]);
        $url_volver=route('turno.listado',$id_horario);

        $dias_semana=collect();
        $dias_semana->push((object)array('id'=>Carbon::SUNDAY,'texto'=>'domingo'));
        $dias_semana->push((object)array('id'=>Carbon::MONDAY,'texto'=>'lunes'));
        $dias_semana->push((object)array('id'=>Carbon::TUESDAY,'texto'=>'martes'));
        $dias_semana->push((object)array('id'=>Carbon::WEDNESDAY,'texto'=>'miercoles'));
        $dias_semana->push((object)array('id'=>Carbon::THURSDAY,'texto'=>'jueves'));
        $dias_semana->push((object)array('id'=>Carbon::FRIDAY,'texto'=>'viernes'));
        $dias_semana->push((object)array('id'=>Carbon::SATURDAY,'texto'=>'sabado'));

        $data=compact('turno','url_volver','dias_semana');
        return View::make('admin.asistencia.horario.turno-dia-semana',$data);
    }

    public function storeTurnoDiaSemana(Request $request,$id_horario){

        $this->validate($request, [
            'dia_semana' => 'required',
            // 'body' => 'required',
        ]);

        

        //verifica que no haya cruce en bd con otros turnos programados. en el mismo dia de semana
        $horario=Horario::findOrFail($id_horario);

        $fecha_hora_entrada=Carbon::createFromFormat('H:i:s',$request->input('hora_entrada'));
        $fecha_hora_salida=Carbon::createFromFormat('H:i:s',$request->input('hora_salida'));

        $turno=new Turno();
        $turno->id_horario=$id_horario;
        $turno->dia_semana=$request->input('dia_semana');
        $turno->fecha_hora_entrada=$fecha_hora_entrada;
        $turno->fecha_hora_salida=$fecha_hora_salida;

        // $turno->timeEnCarbon();

        if($horario->existeCruceDeTurnos($turno)){
            $mensaje=array('tipo'=>'danger','mensaje'=>'Cruce de horas');
            return redirect()->route('turno.listado',[$id_horario])->with('notificacion',$mensaje);
        }

        if ($turno->save()) {
            $mensaje=array('tipo'=>'success','mensaje'=>'Guardado con exito');
            return redirect()->route('turno.listado',[$id_horario])->with('notificacion',$mensaje);
        }

    }



    public function updateTurnoDiaSemana(Request $request,$id_horario,$id_turno){
        //verifica que no haya cruce en bd con otros turnos programados. en el mismo dia de semana
        $horario=Horario::findOrFail($id_horario);

        //dd($horario);

        $fecha_hora_entrada=Carbon::createFromFormat('H:i:s',$request->input('hora_entrada'));
        $fecha_hora_salida=Carbon::createFromFormat('H:i:s',$request->input('hora_salida'));

        $turno=Turno::findOrFail($id_turno);
        $turno->id_horario=$id_horario;
        $turno->dia_semana=$request->input('dia_semana');
        $turno->fecha_hora_entrada=$fecha_hora_entrada;
        $turno->fecha_hora_salida=$fecha_hora_salida;


        if($horario->existeCruceDeTurnos($turno)){
            $mensaje=array('tipo'=>'danger','mensaje'=>'Cruce de horas');
            return redirect()->route('turno.listado',[$id_horario])->with('notificacion',$mensaje);
        }

        if ($turno->save()) {
            $mensaje=array('tipo'=>'success','mensaje'=>'Guardado con exito');
            return redirect()->route('turno.listado',[$id_horario])->with('notificacion',$mensaje);
        }

    }

    public function destroyTurnoDiaSemana(Request $request,$id_horario,$id_turno){
        //verifica que no haya cruce en bd con otros turnos programados. en el mismo dia de semana
        // $horario=Horario::findOrFail($id_horario);

        // $fecha_hora_entrada=Carbon::createFromFormat('H:i:s',$request->input('hora_entrada'));
        // $fecha_hora_salida=Carbon::createFromFormat('H:i:s',$request->input('hora_salida'));

        $turno=Turno::findOrFail($id_turno);
        // $turno->id_horario=$id_horario;
        // $turno->dia_semana=$request->input('dia_semana');
        // $turno->fecha_hora_entrada=$fecha_hora_entrada;
        // $turno->fecha_hora_salida=$fecha_hora_salida;


       /* if(!$horario->isTiempoDisponible($turno)){
            $mensaje=array('tipo'=>'danger','mensaje'=>'Cruce de horas');
            return redirect()->route('turno.listado',[$id_horario])->with('notificacion',$mensaje);
        }*/

        if ($turno->delete()) {
            $mensaje=array('tipo'=>'success','mensaje'=>'Eliminado con exito');
            return redirect()->route('turno.listado',[$id_horario])->with('notificacion',$mensaje);
        }

    }



    public function programado($id_horario){
        $turno=new Turno();
        $turno->id_horario=$id_horario;
        $turno->fecha_hora_inicio=Carbon::today()->addHours(8);
        $turno->fecha_hora_termino=Carbon::today()->addHours(13);
        $turno->dia_semana=Carbon::today()->dayOfWeek;
        $turno->action_form=route('turno.programado.almacenar',[$id_horario]);
        $url_volver=route('turno.listado',$id_horario);

        $data=compact('turno','url_volver','dias_semana');
        return View::make('admin.asistencia.horario.turno-programado',$data);
    }

    public function storeProgramado(Request $request,$id_horario){
        //verifica que no haya cruce en bd con otros turnos programados. en la misma fecha.
    }

}
