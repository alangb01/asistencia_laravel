<?php

namespace App\Http\Controllers\Admin\Asistencia;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use View;
use Carbon\Carbon;
use Validator;

use App\Admin\Asistencia\Suspencion;

class SuspencionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $url_nuevo=route('suspencion.crear');
        $listado_suspenciones=Suspencion::paginate(10);

        foreach ($listado_suspenciones as $id => $suspencion) {
            $suspencion->id_listado=$id+1+($listado_suspenciones->currentPage()-1)*$listado_suspenciones->perPage();
            $suspencion->url_editar=route('suspencion.editar',$suspencion->id);
            $suspencion->url_eliminar=route('suspencion.eliminar',$suspencion->id);
        }
        

        $data=compact('listado_suspenciones','url_nuevo');
        return View::make('admin.asistencia.suspencion.listado',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $suspencion=new Suspencion();
        $suspencion->action_form=route('suspencion.almacenar');
        $suspencion->fecha_hora_inicio=Carbon::now();
        $suspencion->fecha_hora_termino=Carbon::now();

        $url_volver=route('suspencion.listado');

        //si hay inputs enviados
        // dd(Input::get('nombre'));
        // dd($validator);
        

        $data=compact('suspencion','url_volver');   
        return View::make('admin.asistencia.suspencion.formulario',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validator = Validator::make($request->all(), [
            'fecha_inicio' => 'required|date',
            // 'hora_inicio' => 'required',
            'fecha_termino' => 'required|date',
            // 'hora_termino' => 'required',
            'descripcion' => 'required|max:100'
        ]);

        //si no cumple las validaciones, regresa
        if ($validator->fails()) {
            return redirect()->route('suspencion.crear')
                        ->withErrors($validator)
                        ->withInput();
        }

        $fecha_hora_inicio=Carbon::createFromFormat('Y-m-d H:i:s',$request->input('fecha_inicio')." 00:00:00");
        $fecha_hora_termino=Carbon::createFromFormat('Y-m-d H:i:s',$request->input('fecha_termino')." 00:00:00");

        $suspencion=new Suspencion();
        $suspencion->fecha_hora_inicio=$fecha_hora_inicio;
        $suspencion->fecha_hora_termino=$fecha_hora_termino;
        $suspencion->descripcion=$request->input('descripcion');
       
        if ($suspencion->save()) {
            $mensaje=array('tipo'=>'success','mensaje'=>'Guardado con exito');
            return redirect()->route('suspencion.listado')->with('notificacion',$mensaje);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //

        $suspencion=Suspencion::findOrFail($id);
        $suspencion->action_form=route('suspencion.actualizar',$id);

        $url_volver=route('suspencion.listado');

        //si hay inputs enviados
        // dd(Input::get('nombre'));
        // dd($validator);
        

        $data=compact('suspencion','url_volver');   
        return View::make('admin.asistencia.suspencion.formulario',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $validator = Validator::make($request->all(), [
            'fecha_inicio' => 'required|date',
            // 'hora_inicio' => 'required',
            'fecha_termino' => 'required|date',
            // 'hora_termino' => 'required',
            'descripcion' => 'required|max:100'
        ]);

        //si no cumple las validaciones, regresa
        if ($validator->fails()) {
            return redirect()->route('suspencion.crear')
                        ->withErrors($validator)
                        ->withInput();
        }

        $fecha_hora_inicio=Carbon::createFromFormat('Y-m-d H:i:s',$request->input('fecha_inicio')." 00:00:00");
        $fecha_hora_termino=Carbon::createFromFormat('Y-m-d H:i:s',$request->input('fecha_termino')." 00:00:00");

        $suspencion=Suspencion::findOrFail($id);
        $suspencion->fecha_hora_inicio=$fecha_hora_inicio;
        $suspencion->fecha_hora_termino=$fecha_hora_termino;
        $suspencion->descripcion=$request->input('descripcion');
       
        if ($suspencion->save()) {
            $mensaje=array('tipo'=>'success','mensaje'=>'Editado con exito');
            return redirect()->route('suspencion.listado')->with('notificacion',$mensaje);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $suspencion=Suspencion::findOrFail($id);
        if ($suspencion->delete()) {
            $mensaje=array('tipo'=>'success','mensaje'=>'Eliminado con exito');
            return redirect()->route('suspencion.listado')->with('notificacion',$mensaje);
        }else{
            $mensaje=array('tipo'=>'warning','mensaje'=>'No se pudo eliminar');
            return redirect()->route('suspencion.listado')->with('notificacion',$mensaje);
        }

    }
}
