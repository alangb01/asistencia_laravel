<?php

namespace App\Http\Controllers\Admin\Asistencia;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use View;
use Validator;
use Carbon\Carbon;

use App\Admin\Asistencia\Horario;
use App\Admin\Asistencia\Trabajador;

class HorarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request,$id_trabajador)
    {
        //HORARIO PROGRAMADO

        // echo "listado de horarios creados";
        $trabajador=Trabajador::findOrFail($id_trabajador);

        foreach ($trabajador->horarios as $id => $horario) {
            $horario->url_clonar=route('horario.clonar',[$id_trabajador,$horario->id]);

            $horario->url_cronograma=route('turno.listado',[$horario->id]);

            $horario->url_editar=route('horario.editar',[$id_trabajador,$horario->id]);
            $horario->url_eliminar=route('horario.eliminar',[$id_trabajador,$horario->id]);
        }
        $url_horario_crear=route('horario.crear',$id_trabajador);
        $url_volver=route('reporte.trabajadores');

        $data=compact('trabajador','url_horario_crear','url_volver');
        return View::make('admin.asistencia.horario.horarios',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id_trabajador)
    {
        //registrar horario
        $horario=new Horario();
        $horario->id_trabajador=$id_trabajador;
        $horario->fecha_inicio=Carbon::today();
        $horario->fecha_termino=Carbon::today();

        $horario->action_form=route('horario.almacenar',[$id_trabajador]);
        // $horario->fecha_termino=Carbon::createFromTimestamp(0);
        // $horario->fecha_inicio=Carbon::create();

        $url_volver=route('horario.listado',[$id_trabajador]);
        $data=compact('horario','url_volver');
        return View::make('admin.asistencia.horario.formulario',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$id_trabajador)
    {
        //VIENE DE LISTADO Y GENERA UN REGISTRO DE HORARIO, SIN FECHA DE INICIO NI TERMINO
        $validator = Validator::make($request->all(), [
            'fecha_inicio' => 'required|date',
            'fecha_termino' => 'required|date'
        ]);

        //si no cumple las validaciones, regresa
        if ($validator->fails()) {
            return redirect()->route('horario.crear',$id_trabajador)
                        ->withErrors($validator)
                        ->withInput();
        }



        $horario=new Horario();
        $horario->id_trabajador=$request->input('id_trabajador');
        $horario->fecha_inicio=Carbon::createFromFormat('Y-m-d',$request->input('fecha_inicio'));
        $horario->fecha_termino=Carbon::createFromFormat('Y-m-d',$request->input('fecha_termino'));
        // $horario->descripcion=$request->input('descripcion');
        
        if ($horario->existeCruceDeHorarios($horario)) {
           $mensaje=array('tipo'=>'warning','mensaje'=>'Ya existe horario para en las fechas indicadas');
            return redirect()->route('horario.crear',$id_trabajador)
                        ->withErrors($validator)
                        ->withInput()->with('notificacion',$mensaje);
        }

        if ($horario->save()) {
            $mensaje=array('tipo'=>'success','mensaje'=>'Guardado con exito');
            // die("x");
            return redirect()->route('horario.listado',[$id_trabajador])->with('notificacion',$mensaje);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id_trabajador,$id_horario)
    {
        //
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id_trabajador,$id_horario)
    {
        //
        $horario=Horario::findOrFail($id_horario);

        $horario->action_form=route('horario.editar',[$id_trabajador,$id_horario]);
        $url_volver=route('horario.listado',[$id_trabajador]);
        $data=compact('horario','url_volver');
        return View::make('admin.asistencia.horario.formulario',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id_trabajador, $id_horario)
    {
        //
        $validator = Validator::make($request->all(), [
            'fecha_inicio' => 'required|date',
            'fecha_termino' => 'required|date'
        ]);

        //si no cumple las validaciones, regresa
        if ($validator->fails()) {
            return redirect()->route('horario.editar',[$id_trabajador,$id_horario])
                        ->withErrors($validator)
                        ->withInput();
        }

        $horario=Horario::findOrFail($id_horario);
        // $horario->id_trabajador=$request->input('id_trabajador');
        $horario->fecha_inicio=Carbon::createFromFormat('Y-m-d',$request->input('fecha_inicio'));
        $horario->fecha_termino=Carbon::createFromFormat('Y-m-d',$request->input('fecha_termino'));
        // $horario->descripcion=$request->input('descripcion');
        if ($horario->existeCruceDeHorarios($horario)) {
           $mensaje=array('tipo'=>'warning','mensaje'=>'Ya existe horario para en las fechas indicadas');
            return redirect()->route('horario.crear',$id_trabajador)
                        ->withErrors($validator)
                        ->withInput()->with('notificacion',$mensaje);
        }
        
        if ($horario->save()) {
            $mensaje=array('tipo'=>'success','mensaje'=>'Guardado con exito');
            // die("x");
            return redirect()->route('horario.listado',[$id_trabajador])->with('notificacion',$mensaje);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id_trabajador,$id_horario)
    {
        //
        $horario=Horario::findOrFail($id_horario);

        foreach ($horario->turnos as $id => $turno) {
            // $turno->destroy();
        }

        if ($horario->delete()) {
            $mensaje=array('tipo'=>'success','mensaje'=>'Eliminado con exito');
        }else{
            $mensaje=array('tipo'=>'warning','mensaje'=>'No se pudo eliminar');
        }
        return redirect()->route('horario.listado',[$id_trabajador])->with('notificacion',$mensaje);
    }


    public function clonar($id_trabajador,$id_horario){
        $horario_origen=Horario::find($id_horario);
        $turnos_origen=$horario_origen->turnos()->get();

        //clonar horario - registro en bd
        $nuevo_horario=$horario_origen->replicate();
        $nuevo_horario->save();

        
        
      
        foreach ($turnos_origen as $id => $turno_origen) {
            $nuevo_turno=$turno_origen->replicate();
            $nuevo_turno->id_horario=$nuevo_horario->id;
            $nuevo_turno->save();
        }

        
        return redirect()->route('horario.editar',[$id_trabajador,$nuevo_horario->id]);
        // $nuevo_horario->turnos=$horario_origen->turnos->replicate();
    }
}
