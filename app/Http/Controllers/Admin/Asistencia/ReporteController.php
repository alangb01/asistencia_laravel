<?php



namespace App\Http\Controllers\Admin\Asistencia;



use Illuminate\Http\Request;



use App\Http\Requests;

use App\Http\Controllers\Controller;



use Illuminate\Support\Facades\View;

// use Illuminate\Support\Facades\Input;

use Validator;

use Carbon\Carbon;



use Fpdf;

use Maatwebsite\Excel\Facades\Excel;

// use PDF;

// use Barryvdh\DomPDF\Facade as PDF;



use App\Admin\Asistencia\Reporte;

use App\Admin\Asistencia\Trabajador;

use App\Admin\Asistencia\Marcacion;

use App\Admin\Asistencia\Permiso;

use App\Admin\Asistencia\Sancion;

use App\Admin\Personal\Usuario;

use App\Admin\Util\Util;



use Illuminate\Support\Facades\Auth;



class ReporteController extends Controller

{

    /**

     * Display a listing of the resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function index(Request $request)

    {

        //LISTADO DE TRABAJADORES

        $filtro=array('estado'=>'AC','orden'=>array('apellido'=>'asc','nombre'=>'asc'));

        $trabajadores=Trabajador::filtrar($filtro)->paginate(10);



        foreach ($trabajadores as $id => $trabajador) {

            $trabajador->url_horario=route('horario.listado',$trabajador->id);

            $trabajador->url_reporte_preview=route('reporte.preview',$trabajador->id);

            // $trabajador->url_reporte_anteriores=route('reporte.anteriores',$trabajador->id);

            $trabajador->url_marcacion_justificar=route('reporte.marcacion.justificar',[$trabajador->id,"volver"=>'lista-trabajadores']);

        }



        $data=compact('trabajadores');

        return View::make('admin.asistencia.reporte.trabajadores',$data);



    }



    /**

     * Show the form for creating a new resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function create()

    {

        //

    }



    /**

     * Store a newly created resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @return \Illuminate\Http\Response

     */

    public function store(Request $request)

    {

        //

    }



    /**

     * Display the specified resource.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function show($id)

    {

        //

    }



    /**

     * Show the form for editing the specified resource.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function edit($id)

    {

        //

    }



    /**

     * Update the specified resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function update(Request $request, $id)

    {

        //

    }



    /**

     * Remove the specified resource from storage.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function destroy($id)

    {

        //

    }





    //

    public function historial($id_trabajador){



    }



    public function preview(Request $request,$id_trabajador)

    {

        $data=$this->prepararReporte($request,$id_trabajador);

        return View::make('admin.asistencia.reporte.preview',$data);



    }



    public function exportarExcel(Request $request, $id_trabajador){

        $tiempo_creacion=time();

        $data=$this->prepararReporte($request,$id_trabajador);

        $reporte=$data['reporte'];



        $nombre_archivo="reporte-[".$reporte->trabajador->dni."]-";

        $nombre_archivo.=$reporte->fecha_inicio->format("Y-m-d");

        $nombre_archivo.="--";

        $nombre_archivo.=$reporte->fecha_final->format("Y-m-d");

        // dd($reporte->listadoMarcaci);

        Excel::create($nombre_archivo, function($excel) use($reporte){

            //RESUMEN DE ESTADISTICAS

            /**

            * Dias programados    26  Tiempo programado   192h

            * Dias feriados   0   Tiempo feriado  0s

            * Dias laborable [Efectivo]   26  Tiempo laborable [Efectivo] 192h

            * Dias asistidos  7   Tiempo asistido 48h 25m 23s

            * Ratio dias  26.92%  Ratio tiempo    25.22%

            */

            $excel->sheet('Estadisticas generales', function($sheet) use($reporte){

                $listado_marcaciones = $reporte->marcacionesProcesadas();

                

                $titulos=array("Estadisticas");

                $titulos=array_map("strtoupper", $titulos);

                $sheet->row(1,$titulos);



                $sheet->cells('A1', function($cells) {

                    $cells->setBackground('#000000');

                    $cells->setFontColor('#ffffff');

                    $cells->setFont(array(

                        //'family'     => 'Calibri',

                        //'size'       => '16',

                        'bold'       =>  true

                    ));

                });



                // $sheet->cells('',function($cells){})



                $item=array('Dias programados',$reporte->dias_programados,

                    'Tiempo programado',$reporte->tiempoProgramadoToString());

                $sheet->row(2,$item);



                $item=array('Dias feriados/suspendidos',$reporte->dias_suspenciones,

                    'Tiempo feriado/suspendido',$reporte->tiempoSuspencionesToString());

                $sheet->row(3,$item);



                $item=array('Dias laborables[efectivo]',$reporte->dias_laborables,

                    'Tiempo laborable[efectivo]',$reporte->tiempoLaborableToString());

                $sheet->row(4,$item);



                $item=array('Dias asistidos',$reporte->dias_asistidos,

                    'Tiempo asistido',$reporte->tiempoAsistidoToString());

                $sheet->row(5,$item);



                $item=array('Ratio dias',$reporte->ratioDias('%'),

                    'Ratio tiempo',$reporte->ratioTiempo('%'));

                $sheet->row(6,$item);



                $sheet->cells('A1:A6', function($cells) {

                    $cells->setFont(array(

                        //'family'     => 'Calibri',

                        //'size'       => '16',

                        'bold'       =>  true

                    ));



                });



                $sheet->cells('C1:C6', function($cells) {

                    $cells->setFont(array(

                        //'family'     => 'Calibri',

                        //'size'       => '16',

                        'bold'       =>  true

                    ));



                });



               /* $sheet->cells('A1:F1', function($cells) {

                    $cells->setBackground('#000000');

                    $cells->setFontColor('#ffffff');

                    $cells->setFont(array(

                        //'family'     => 'Calibri',

                        //'size'       => '16',

                        'bold'       =>  true

                    ));



                });*/

               /* foreach ($listado_marcaciones as $id => $marcacion) {

                    $celdas=array(

                        $marcacion->diaToString(),

                        $marcacion->fecha_hora_marcacion->format("Y-m-d"),

                        $marcacion->fecha_hora_marcacion->format("h:i:s a"),

                        $marcacion->tipoToString(),

                        $marcacion->justificacionToString(),

                        $marcacion->observacionToString(),

                        );

                    $sheet->row($id+2,$celdas);

                }*/

            });





            //LISTADO REPORTE DIARIO

            /*$excel->sheet('reporte-diario', function($sheet) use($reporte){

                $listado_reportes_diarios = $reporte->reportesDiariosProcesados();

                

                $titulos=array("dia","fecha","horario","turnos","feriado","asistencia","marcaciones");

                $titulos=array_map("strtoupper", $titulos);

                $sheet->row(1,$titulos);

                $sheet->cells('A1:G1', function($cells) {

                    $cells->setBackground('#000000');

                    $cells->setFontColor('#ffffff');

                    $cells->setFont(array(

                        //'family'     => 'Calibri',

                        //'size'       => '16',

                        'bold'       =>  true

                    ));



                });

                foreach ($listado_reportes_diarios as $id => $reporte_diario) {



                    $texto_horarios=$reporte_diario->listado_horarios->count()>0?'Asignado':'---';

                    

                    $texto_turnos=$reporte_diario->listado_turnos->count()>0?"":"---";

                    foreach ($reporte_diario->listado_turnos as $id_t => $turno) {

                        $texto_turnos.=$id>0?"\n":"";

                        $texto_turnos.=$turno->fecha_hora_entrada->format("h:i:s a");

                        $texto_turnos.=" a ";

                        $texto_turnos.=$turno->fecha_hora_salida->format("h:i:s a");

                        $texto_turnos.="[".$turno->tiempo()."]";

                    }



                    $texto_suspenciones=$reporte_diario->listado_suspenciones->count()>0?"":"---";



                    foreach ($reporte_diario->listado_suspenciones as $id_s => $suspencion) {

                        $texto_suspenciones.=$id>0?"\n":"";

                        $texto_suspenciones.=$suspencion->descripcion;

                    }



                    $texto_asistencias=$reporte_diario->listado_asistencias->count()>0?"":"---";



                    foreach ($reporte_diario->listado_asistencias as $id_a => $asistencia) {

                        $texto_asistencias.=$id_a>0?"\n":"";

                        $texto_asistencias.=$asistencia->entrada->fecha_hora_marcacion->format("h:i:s a");

                        $texto_asistencias.=" a ";

                        $texto_asistencias.=$asistencia->salida->fecha_hora_marcacion->format("h:i:s a");

                        $texto_asistencias.="[".$asistencia->tiempoAcumulado()."]";

                    }



                    $texto_marcaciones="";

                    foreach ($reporte_diario->listado_marcaciones as $id_m => $marcacion) {

                        if ($marcacion->observacion!="") {

                            $texto_marcaciones.=$id_m>0?"\n":"";

                            $texto_marcaciones.=$marcacion->fecha_hora_marcacion->format("h:i:s a");

                            $texto_marcaciones.="[".$marcacion->tipoToString()."-";

                            $texto_marcaciones.=$marcacion->justificacionToString()."]";

                            $texto_marcaciones.="(".$marcacion->observacion.")";

                        }

                    }



                    $texto_marcaciones==""?$texto_marcaciones="---":null;





                    $celdas=array(

                        $reporte_diario->diaToString(),

                        $reporte_diario->fecha->format("Y-m-d"),

                        $texto_horarios,

                        $texto_turnos,

                        $texto_suspenciones,

                        $texto_asistencias,

                        $texto_marcaciones

                        );

                    $sheet->row($id+2,$celdas);

                    //$sheet->getRowDimension(1)->setRowHeight(10);

                }

            });

            */



            //LISTADO MARCACIONES

            $excel->sheet('marcaciones', function($sheet) use($reporte){

                $listado_marcaciones = $reporte->marcacionesProcesadas();

                

                $titulos=array("dia","fecha","hora","tipo","justificacion","observacion");

                $titulos=array_map("strtoupper", $titulos);

                $sheet->row(1,$titulos);

                $sheet->cells('A1:F1', function($cells) {

                    $cells->setBackground('#000000');

                    $cells->setFontColor('#ffffff');

                    $cells->setFont(array(

                        //'family'     => 'Calibri',

                        //'size'       => '16',

                        'bold'       =>  true

                    ));



                });

                foreach ($listado_marcaciones as $id => $marcacion) {

                    $celdas=array(

                        $marcacion->diaToString(),

                        $marcacion->fecha_hora_marcacion->format("Y-m-d"),

                        $marcacion->fecha_hora_marcacion->format("h:i:s a"),

                        $marcacion->tipoToString(),

                        $marcacion->justificacionToString(),

                        $marcacion->observacionToString(),

                        );

                    $sheet->row($id+2,$celdas);

                }

            });



            //LISTADO ASISTENCIAS

            $excel->sheet('asistencias', function($sheet) use($reporte){

                $listado_asistencias = $reporte->asistenciasProcesadas();

                

                $titulos=array("dia","fecha","entrada","salida","tiempo");

                $titulos=array_map("strtoupper", $titulos);

                $sheet->row(1,$titulos);

                $sheet->cells('A1:E1', function($cells) {

                    $cells->setBackground('#000000');

                    $cells->setFontColor('#ffffff');

                    $cells->setFont(array(

                        //'family'     => 'Calibri',

                        //'size'       => '16',

                        'bold'       =>  true

                    ));



                });

                foreach ($listado_asistencias as $id => $asistencia) {

                    $celdas=array(

                        $asistencia->diaToString(),

                        $asistencia->fecha()->format("Y-m-d"),

                        $asistencia->entrada->fecha_hora_marcacion->format("h:i:s a"),

                        $asistencia->salida->fecha_hora_marcacion->format("h:i:s a"),

                        $asistencia->tiempoAcumulado()

                        );

                    $sheet->row($id+2,$celdas);

                }

            });



            //LISTADO SUSPENCIONEs

            $excel->sheet('suspenciones', function($sheet) use($reporte){

                $listado_suspenciones = $reporte->suspencionesProcesadas();

                

                $titulos=array("desde","hasta","Motivo");

                $titulos=array_map("strtoupper", $titulos);

                $sheet->row(1,$titulos);

                $sheet->cells('A1:C1', function($cells) {

                    $cells->setBackground('#000000');

                    $cells->setFontColor('#ffffff');

                    $cells->setFont(array(

                        //'family'     => 'Calibri',

                        //'size'       => '16',

                        'bold'       =>  true

                    ));



                });

                foreach ($listado_suspenciones as $id => $suspencion) {

                    $celdas=array(

                        $suspencion->fecha_hora_inicio->format("Y-m-d"),

                        $suspencion->fecha_hora_termino->format("Y-m-d"),

                        $suspencion->descripcion

                        );

                    $sheet->row($id+2,$celdas);

                }

            });



            $excel->setActiveSheetIndex(0);

        })->export('xls');

    }



    public function guardar(Request $request,$id_trabajador){

        // echo Fpdf::class;

        $tiempo_creacion=time();

        $data=$this->prepararReporte($request,$id_trabajador);

        $reporte=$data['reporte'];



        // $pdf=new FPDF('P','mm',array(100,150));

        // Fpdf::output();

        // exit();

        // Fpdf::FPDF('P','mm','A5');

        

        Fpdf::setMargins(10, 15);

        Fpdf::AliasNbPages();

        Fpdf::AddPage();



        //TITULO

        Fpdf::SetTextColor(0x00, 0x00, 0x00);

        Fpdf::setFont("Arial", "B", 9);

        Fpdf::Cell(0, 5, "REPORTE DE TRABAJADOR - CODIGO:".$tiempo_creacion, 0, 1, 'C');



        Fpdf::setFont("Arial", "", 9);

        Fpdf::Ln();



        Fpdf::setFont("Arial", "B", 9);

        Fpdf::Cell(30, 5, 'DNI: ',1,0,"");

        Fpdf::setFont("Arial", "", 9);

        Fpdf::Cell(20, 5, ucwords(utf8_decode($reporte->trabajador->dni)),1,0,"");

            

        Fpdf::setFont("Arial", "B", 9);

        Fpdf::Cell(20, 5, 'CARGO: ',1,0,"");

        Fpdf::setFont("Arial", "", 9);

        Fpdf::Cell(0, 5, ucwords(utf8_decode($reporte->trabajador->cargo)),1,0,"");



        





        Fpdf::Ln();

        Fpdf::setFont("Arial", "B", 9);

        Fpdf::Cell(30, 5, 'TRABAJADOR: ',1,0,"");

        Fpdf::setFont("Arial", "", 9);

        Fpdf::Cell(0, 5, ucwords(utf8_decode($reporte->trabajador)),1,0,"");

        



        Fpdf::Ln();



        Fpdf::setFont("Arial", "B", 9);

        Fpdf::Cell(30, 5, 'PERIODO:', 1, 0);

        Fpdf::setFont("Arial", "", 9);

        Fpdf::Cell(0, 5, $reporte->fecha_inicio->format('Y-m-d')." al ".$reporte->fecha_final->format('Y-m-d'), 1, 0);

Fpdf::Ln();

       /* 

        Fpdf::Ln();

        Fpdf::setFont("Arial", "B", 9);

        Fpdf::Cell(85, 5, 'DIAS', 1, 0,"C");

        Fpdf::Cell(85, 5, 'HORAS', 1, 0,"C");*/

        Fpdf::Ln();

        Fpdf::setFont("Arial", "B", 9);

        Fpdf::Cell(45, 5, 'Dias programados:', 1, 0);

        Fpdf::setFont("Arial", "", 9);

        Fpdf::Cell(50, 5, $reporte->dias_programados, 1, 0);

        Fpdf::setFont("Arial", "B", 9);

        Fpdf::Cell(45, 5, 'Tiempo programado:', 1, 0);

        Fpdf::setFont("Arial", "", 9);

        Fpdf::Cell(50, 5, $reporte->tiempoProgramadoToString(), 1, 0);

        Fpdf::Ln();



        Fpdf::setFont("Arial", "B", 9);

        Fpdf::Cell(45, 5, 'Dias feriados/suspendidos:', 1, 0);

        Fpdf::setFont("Arial", "", 9);

        Fpdf::Cell(50, 5, $reporte->dias_suspenciones, 1, 0);

        Fpdf::setFont("Arial", "B", 9);

        Fpdf::Cell(45, 5, 'Tiempo feriado/suspendido:', 1, 0);

        Fpdf::setFont("Arial", "", 9);

        Fpdf::Cell(50, 5, $reporte->tiempoSuspencionesToString(), 1, 0);

        Fpdf::Ln();

        Fpdf::setFont("Arial", "B", 9);

        Fpdf::Cell(45, 5, 'Dias laborables:', 1, 0);

        Fpdf::setFont("Arial", "", 9);

        Fpdf::Cell(50, 5, $reporte->dias_laborables, 1, 0);

        Fpdf::setFont("Arial", "B", 9);

        Fpdf::Cell(45, 5, 'Tiempo laborable:', 1, 0);

        Fpdf::setFont("Arial", "", 9);

        Fpdf::Cell(50, 5, $reporte->tiempoLaborableToString(), 1, 0);

        Fpdf::Ln();



        Fpdf::setFont("Arial", "B", 9);

        Fpdf::Cell(45, 5, 'Dias asistidos:', 1, 0);

        Fpdf::setFont("Arial", "", 9);

        Fpdf::Cell(50, 5, $reporte->dias_asistidos, 1, 0);

        Fpdf::setFont("Arial", "B", 9);

        Fpdf::Cell(45, 5, 'Tiempo asistido:', 1, 0);

        Fpdf::setFont("Arial", "", 9);

        Fpdf::Cell(50, 5, $reporte->tiempoAsistidoToString(), 1, 0);

        Fpdf::Ln();

        

        Fpdf::setFont("Arial", "B", 9);

        Fpdf::Cell(45, 5, 'Ratio dias:', 1, 0);

        Fpdf::setFont("Arial", "", 9);

        Fpdf::Cell(50, 5,$reporte->ratioDias("%"), 1, 0);

        Fpdf::setFont("Arial", "B", 9);

        Fpdf::Cell(45, 5, 'Ratio tiempo:', 1, 0);

        Fpdf::setFont("Arial", "", 9);

        Fpdf::Cell(50, 5, $reporte->ratioTiempo("%"), 1, 0);



        Fpdf::Ln();

        Fpdf::Ln();



        // LISTADO DE MARCACIONES

        Fpdf::SetTextColor(0x00, 0x00, 0x00);

        Fpdf::setFont("Arial", "B", 9);

        Fpdf::Cell(0, 5, "REGISTRO DE MARCACIONES", 0, 1, 'C');

        

        Fpdf::Ln();



        Fpdf::Cell(20, 5, 'DIA', 1, 0);

        Fpdf::Cell(20, 5, 'FECHA', 1, 0);

        Fpdf::Cell(20, 5, 'HORA', 1, 0);

        Fpdf::Cell(20, 5, 'TIPO', 1, 0);

        Fpdf::Cell(30, 5, 'JUSTIFICACION', 1, 0);

        Fpdf::Cell(0, 5, 'OBSERVACION', 1, 0);



        Fpdf::Ln();

        Fpdf::setFont("Arial", "", 9);

        if (count($reporte->marcacionesProcesadas())>0) {

            foreach ($reporte->marcacionesProcesadas() as $id => $marcacion) {

                if($marcacion::ESTADO_VERIFICAR==$marcacion->estado){

                    Fpdf::SetTextColor(255, 25, 25);

                    Fpdf::SetFillColor(0,0,0);

                }else{

                    Fpdf::SetTextColor(0x00, 0x00, 0x00);

                    Fpdf::SetFillColor(255,255,255);

                }

                Fpdf::Cell(20, 5, utf8_decode($marcacion->diaToString()), 1, 0);

                Fpdf::Cell(20, 5, utf8_decode($marcacion->fecha_hora_marcacion->format('Y-m-d')), 1, 0);

                Fpdf::Cell(20, 5, utf8_decode($marcacion->fecha_hora_marcacion->format('h:i:s a')), 1, 0);

                Fpdf::Cell(20, 5, utf8_decode($marcacion->tipoToString()), 1, 0);

                Fpdf::Cell(30, 5, utf8_decode($marcacion->justificacionToString()), 1, 0);

                Fpdf::Cell(0, 5, utf8_decode($marcacion->observacionToString()), 1, 0);



                Fpdf::Ln();

            }

        }else{

            Fpdf::Cell(0, 5, utf8_decode("Sin marcaciones"), 1, 0);

        }





        Fpdf::Ln();

        Fpdf::Ln();



        // LISTADO DE ASISTENCIAS

        Fpdf::SetTextColor(0x00, 0x00, 0x00);

        Fpdf::setFont("Arial", "B", 9);

        Fpdf::Cell(0, 5, "REGISTRO DE ASISTENCIAS", 0, 1, 'C');

        

        Fpdf::Ln();



        Fpdf::Cell(20, 5, 'DIA', 1, 0);

        Fpdf::Cell(20, 5, 'FECHA', 1, 0);

        Fpdf::Cell(20, 5, 'INGRESO', 1, 0);

        Fpdf::Cell(20, 5, 'SALIDA', 1, 0);

        Fpdf::Cell(0, 5, 'ACUMULADO', 1, 0);

        Fpdf::Ln();

        Fpdf::setFont("Arial", "", 9);

        if (count($reporte->asistenciasProcesadas())>0) {

            foreach ($reporte->asistenciasProcesadas() as $id => $asistencia) {



                Fpdf::Cell(20, 5, utf8_decode($asistencia->diaToString()), 1, 0);

                Fpdf::Cell(20, 5, utf8_decode($asistencia->fecha()->format('Y-m-d')), 1, 0);

                Fpdf::Cell(20, 5, utf8_decode($asistencia->entrada->fecha_hora_marcacion->format('h:i:s a')), 1, 0);

                Fpdf::Cell(20, 5, utf8_decode($asistencia->salida->fecha_hora_marcacion->format('h:i:s a')), 1, 0);

                Fpdf::Cell(0, 5, utf8_decode($asistencia->tiempoAcumulado()), 1, 0);

                Fpdf::Ln();

            }

        }else{

            Fpdf::Cell(0, 5, utf8_decode("Sin asistencias"), 1, 0);

        }



        



        Fpdf::Ln();

        Fpdf::Ln();



       // LISTADO DE REPORTE DIARIO

        /*Fpdf::SetTextColor(0x00, 0x00, 0x00);

        Fpdf::setFont("Arial", "B", 9);

        Fpdf::Cell(0, 5, "REGISTRO DIARIO", 0, 1, 'C');

        

        Fpdf::Ln();



        Fpdf::Cell(20, 5, 'DIA', 1, 0);

        Fpdf::Cell(20, 5, 'FECHA', 1, 0);

        Fpdf::Cell(20, 5, 'TURNOS', 1, 0);

        Fpdf::Cell(20, 5, 'FERIADO', 1, 0);

        Fpdf::Cell(20, 5, 'ASISTENCIAS', 1, 0);

        Fpdf::Cell(0, 5, 'MARCACIONES', 1, 0);

        Fpdf::Ln();

        Fpdf::setFont("Arial", "", 9);

        if (count($reporte->reportesDiariosProcesados())>0) {

            foreach ($reporte->reportesDiariosProcesados() as $id => $reporte_diario) {

                Fpdf::Cell(20, 5, ($reporte_diario->diaToString()), 1, 0);

                Fpdf::Cell(20, 5, ($reporte_diario->fecha->format('Y-m-d')), 1, 0);



                if ($reporte_diario->listado_horarios->count()>0) {

                    $cadena="con horario";

                }else{

                   $cadena="sin horario";

                }

                Fpdf::Cell(20, 5, $cadena, 1, 0);



                if($reporte_diario->listado_turnos->count()>0){

                    $cadena="";

                    foreach($reporte_diario->listado_turnos as $id_t=>$turno){

                        $cadena.=($id_t>0)?"\n":"";

                        $cadena.=$turno->fecha_hora_entrada->format('H:i a')." a ".$turno->fecha_hora_salida->format('H:i a')."[".$turno->tiempo()."]";

                    }

                }else{

                    $cadena="---";

                }



                Fpdf::Cell(10, 5, $cadena, 1, 0);

                //SUSPENCION

                if($reporte_diario->listado_suspenciones->count()>0){

                    $cadena="";

                    foreach($reporte_diario->listado_suspenciones as $id_su=>$suspencion){

                        $cadena.=($id_t>0)?"\n":"";

                        $cadena.=$suspencion->descripcion;

                    }

                }else{

                   $cadena="---";

                }



                Fpdf::MultiCell(10, 5, $cadena, 1, 0);

                //ASISTENCIA

                if($reporte_diario->listado_asistencias->count()>0){

                    $cadena="";

                    foreach ($reporte_diario->listado_asistencias as $id_a => $asistencia){

                        $cadena.=($id_t>0)?"\n":"";

                        $cadena.=$asistencia->entrada->fecha_hora_marcacion->format("h:i a")."-".$asistencia->salida->fecha_hora_marcacion->format("h:i a")."[".$asistencia->tiempoAcumulado()."]";

                        

                    }

                }else{

                    $cadena="---";

                }



                 Fpdf::MultiCell(10, 5, $cadena, 1, 0);

                //MARCACION

                if($reporte_diario->listado_marcaciones->count()>0){

                    $cadena="";

                    foreach ($reporte_diario->listado_marcaciones as $id_m => $marcacion){



                        if(trim($marcacion->observacion)!=""){

                            $cadena.=($id_t>0)?"\n":"";

                            $cadena.="[".$marcacion->tipoToString()." - ".$marcacion->justificacionToString()."]";

                            $cadena.=$marcacion->fecha_hora_marcacion->format("H:i:s");

                            $cadena.="(".$marcacion->observacion.")";

                        }

                    }

                }else{

                    $cadena="---";

                }



                 Fpdf::MultiCell(10, 5, $cadena, 1, 0);



                Fpdf::Ln();

            }

        }else{

            Fpdf::Cell(0, 5, utf8_decode("Sin asistencias"), 1, 0);

        }



        */



        Fpdf::Ln();

        Fpdf::Ln();

         // LISTADO DE SUSPENCIONES DE LABORES

        Fpdf::SetTextColor(0x00, 0x00, 0x00);

        Fpdf::setFont("Arial", "B", 9);

        Fpdf::Cell(0, 5, "REGISTRO DE FERIADOS/SUSPENCIONES", 0, 1, 'C');

        

        Fpdf::Ln();



        Fpdf::Cell(50, 5, 'FECHA HORA INICIO', 1, 0);

        Fpdf::Cell(50, 5, 'FECHA HORA TEMRINO', 1, 0);

        Fpdf::Cell(0, 5, 'DESCRIPCION', 1, 0);



        Fpdf::Ln();

        Fpdf::setFont("Arial", "", 9);

        if (count($reporte->suspencionesProcesadas())>0) {

            foreach ($reporte->suspencionesProcesadas() as $id => $suspencion) {

                /*if($marcacion::ESTADO_VERIFICAR==$marcacion->estado){

                    Fpdf::SetTextColor(255, 25, 25);

                    Fpdf::SetFillColor(0,0,0);

                }else{

                    Fpdf::SetTextColor(0x00, 0x00, 0x00);

                    Fpdf::SetFillColor(255,255,255);

                }*/

                Fpdf::Cell(50, 5, utf8_decode($suspencion->fecha_hora_inicio->format('Y-m-d h:i a')), 1, 0);

                Fpdf::Cell(50, 5, utf8_decode($suspencion->fecha_hora_termino->format('Y-m-d h:i a')), 1, 0);

                Fpdf::Cell(0, 5, utf8_decode($suspencion->descripcionToString()), 1, 0);

                Fpdf::Ln();

            }

        }else{

            Fpdf::Cell(0, 5, utf8_decode("Sin suspenciones"), 1, 0);

        }

        Fpdf::Ln();

        Fpdf::Ln();

        



        Fpdf::output();

        exit();

    }



    private function prepararReporte($request,$id_trabajador){

        $año=date("Y");

        $mes=date("m");



        $fecha_inicio_mes_anterior=Carbon::createFromDate($año, $mes-1, 1,'America/Lima');

        $fecha_final_mes_anterior=Carbon::createFromDate($año, $mes, 0,'America/Lima');



        $fecha_inicio_mes_actual=Carbon::createFromDate($año, $mes, 1,'America/Lima');

        $fecha_final_mes_actual=Carbon::createFromDate($año, $mes+1, 0,'America/Lima');



        

        $url_mes_actual=route('reporte.preview',[$id_trabajador,

            'fecha_inicio'=>$fecha_inicio_mes_actual->format("Y-m-d"),

            'fecha_final'=>$fecha_final_mes_actual->format("Y-m-d")]);

        $url_mes_anterior=route('reporte.preview',[$id_trabajador,

            'fecha_inicio'=>$fecha_inicio_mes_anterior->format("Y-m-d"),

            'fecha_final'=>$fecha_final_mes_anterior->format("Y-m-d")]);



        

        //cargar fechas

        if ($request->get('fecha_inicio')!=null && $request->get('fecha_final')!=null) {

            $fecha_inicio=Carbon::createFromFormat("Y-m-d",$request->get('fecha_inicio'));

            $fecha_final=Carbon::createFromFormat("Y-m-d",$request->get('fecha_final'));

        //     $fecha_final=Carbon::createFromFormat("Y-m-d",$request->get('fecha_final'));

        }else if($request->get('fecha_inicio')!=null && $request->get('fecha_final')==null){

            $fecha_inicio=Carbon::createFromFormat("Y-m-d",$request->get('fecha_inicio'));

            $fecha_final=Carbon::createFromFormat("Y-m-d",$request->get('fecha_inicio'));

        }else if($request->get('fecha_inicio')==null && $request->get('fecha_final')!=null){

            $fecha_inicio=Carbon::createFromFormat("Y-m-d",$request->get('fecha_final'));

            $fecha_final=Carbon::createFromFormat("Y-m-d",$request->get('fecha_final'));

        }else{

            $fecha_inicio=$fecha_inicio_mes_actual;

            $fecha_final=$fecha_final_mes_actual;

        }



        $mes_actual=false;

        $mes_anterior=false;

        

       

        if($fecha_inicio_mes_anterior==$fecha_inicio 

            && $fecha_final_mes_anterior==$fecha_final){

            $mes_anterior=true;

        }elseif($fecha_inicio_mes_actual==$fecha_inicio

            && $fecha_final_mes_actual==$fecha_final){

            $mes_actual=true;

        }

        



        



        //$url_actual=route('reporte.preview',);



        $reporte=new Reporte();

        $reporte->id_trabajador=$id_trabajador;



        $reporte->fecha_inicio=$fecha_inicio;

        $reporte->fecha_final=$fecha_final;



        $reporte_data=$reporte->preparar();



        foreach ($reporte->marcacionesProcesadas() as $i => $marcacion) {

            $marcacion->url_cambiar_tipo=route('reporte.marcacion.cambiar',[$id_trabajador,$marcacion->id]);

            $marcacion->url_eliminar=route('reporte.marcacion.cambiar',[$id_trabajador,$marcacion->id]);

        }

        

        foreach ($reporte->permisosProcesados() as $id_p => $permiso) {

            // $permiso->url_editar=route('reporte.permiso')

            $permiso->url_eliminar=route('reporte.permiso.eliminar',[$id_trabajador,$permiso->id]);

        }



        $reporte->url_marcacion_justificar=route('reporte.marcacion.justificar',$id_trabajador);

        // $reporte->url_permiso_dias_crear=route('reporte.permiso-dias.crear',$id_trabajador);

        $reporte->url_permiso_horas_crear=route('reporte.permiso-horas.crear',$id_trabajador);

        $reporte->url_sancion_crear=route('reporte.sancion.crear',$id_trabajador);



        $reporte->url_exportar_pdf=route('reporte.exportar.pdf',[$id_trabajador,

            "fecha_inicio"=>$fecha_inicio->format("Y-m-d"),"fecha_final"=>$fecha_final->format("Y-m-d")]);

        $reporte->url_exportar_excel=route('reporte.exportar.excel',[$id_trabajador,

            "fecha_inicio"=>$fecha_inicio->format("Y-m-d"),"fecha_final"=>$fecha_final->format("Y-m-d")]);



        $url_volver=route('reporte.trabajadores');



        $data=compact('reporte','url_volver','url_marcacion_justificar','url_mes_actual','url_mes_anterior','mes_actual','mes_anterior');

        return $data;

    }

}

