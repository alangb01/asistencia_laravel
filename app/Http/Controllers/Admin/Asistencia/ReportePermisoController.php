<?php

namespace App\Http\Controllers\Admin\Asistencia;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\View;
// use Illuminate\Support\Facades\Input;
use Validator;
use Carbon\Carbon;

use Fpdf;
// use PDF;
// use Barryvdh\DomPDF\Facade as PDF;

use App\Admin\Asistencia\Reporte;
use App\Admin\Asistencia\Trabajador;
use App\Admin\Asistencia\Marcacion;
use App\Admin\Asistencia\Permiso;
use App\Admin\Asistencia\Sancion;

class ReportePermisoController extends Controller
{
    

     public function crearPermisoHoras(Request $request,$id_trabajador){
        $trabajador=Trabajador::findOrFail($id_trabajador);
        // $
        // $trabajador->action_form=route('reporte.marcacion.almacenar',$id_trabajador);
        $permiso=new Permiso();
        $permiso->id_trabajador=$id_trabajador;
        $permiso->fecha_hora_inicio=Carbon::today()->addHours(8);
        $permiso->fecha_hora_termino=Carbon::today()->addHours(13);

        $permiso->action_form=route('reporte.permiso-horas.almacenar',$id_trabajador);


        $url_volver=route('reporte.preview',$id_trabajador);

        $data=compact('permiso','url_volver');
        return View::make('admin.asistencia.reporte.permiso',$data);
    }

    public function procesarPermisoHoras(Request $request,$id_trabajador){
        //REGISTRA UN PERMISO JUSTIFICADO
        $validator = Validator::make($request->all(), [
            'fecha'=>'required|date',
            'hora_inicio'=>'required',
            'hora_termino'=>'required',
            'tipo'=>'required',
            'descripcion'=>'required',
        ]);

        //si no cumple las validaciones, regresa
        if ($validator->fails()) {
            return redirect()->route('reporte.permiso-horas.crear',[$id_trabajador])
                        ->withErrors($validator)
                        ->withInput();
        }


        $fecha_hora_inicio=Carbon::createFromFormat('Y-m-d H:i:s',$request->input('fecha')." ".$request->input('hora_inicio'));
        $fecha_hora_termino=Carbon::createFromFormat('Y-m-d H:i:s',$request->input('fecha')." ".$request->input('hora_termino'));


        $permiso=new Permiso();
        $permiso->id_trabajador=$request->input('id_trabajador');
        $permiso->fecha_hora_inicio=$fecha_hora_inicio;
        $permiso->fecha_hora_termino=$fecha_hora_termino;
        $permiso->tipo=$request->input('tipo');
        $permiso->descripcion=$request->input('descripcion');
        $permiso->cronometrado=$request->input('cronometrado')>0?true:false;

        if ($permiso->save()) {
            return redirect()->route('reporte.preview',[$id_trabajador]);
        }
    }

    public function eliminarPermiso($id_trabajador,$id_permiso){
        $permiso=Permiso::findOrFail($id_permiso);
        // dd($marcacion);
        if ($permiso->delete()) {
            return redirect()->route('reporte.preview',[$id_trabajador]);
        }
    }
}
