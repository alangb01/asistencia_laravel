<?php

namespace App\Http\Controllers\Admin\Asistencia;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\View;
// use Illuminate\Support\Facades\Input;
use Validator;
use Carbon\Carbon;

use Fpdf;
// use PDF;
// use Barryvdh\DomPDF\Facade as PDF;

use App\Admin\Asistencia\Reporte;
use App\Admin\Asistencia\Trabajador;
use App\Admin\Asistencia\Marcacion;
use App\Admin\Asistencia\Permiso;
use App\Admin\Asistencia\Sancion;

class ReporteMarcadorController extends Controller
{
    public function cambiarMarcacion($id_trabajador,$id_marcacion){
        $marcacion=Marcacion::findOrFail($id_marcacion);
        if ($marcacion->tipo_marcacion==$marcacion::TIPO_ENTRADA) {
            $marcacion->tipo_marcacion=$marcacion::TIPO_SALIDA;
        }else if($marcacion->tipo_marcacion==$marcacion::TIPO_SALIDA){
            $marcacion->tipo_marcacion=$marcacion::TIPO_ENTRADA;
        }

        if ($marcacion->update()) {
            return redirect()->route('reporte.preview',[$id_trabajador]);
        }
    }

    public function eliminarMarcacion($id_trabajador,$id_marcacion){
        $marcacion=Marcacion::findOrFail($id_marcacion);
        // dd($marcacion);
        if ($marcacion->delete()) {
            return redirect()->route('reporte.preview',[$id_trabajador]);
        }
    }

    public function marcador(Request $request,$id_trabajador){
        $trabajador=Trabajador::findOrFail($id_trabajador);
        // $
        // $trabajador->action_form=route('reporte.marcacion.almacenar',$id_trabajador);
        $marcacion=new Marcacion();
        $marcacion->id_trabajador=$id_trabajador;
        $marcacion->fecha_hora_marcacion=Carbon::today()->addHours(8);
        $marcacion->observacion="";
        


        if ($request->get('volver')=="lista-trabajadores") {
            $url_volver=route('reporte.trabajadores');
            $marcacion->action_form=route('reporte.marcacion.almacenar',[$id_trabajador,'volver'=>$request->get('volver')]);
        }else{
            $url_volver=route('reporte.preview',$id_trabajador);
            $marcacion->action_form=route('reporte.marcacion.almacenar',$id_trabajador);
        }
        

        $data=compact('marcacion','url_volver');
        return View::make('admin.asistencia.reporte.marcador',$data);
    }

    public function procesarMarcacion(Request $request,$id_trabajador){
        //GUARDA LA MARCACION REGISTRADA DESDE JUSTIFICAR MARCACION Y ACTUALIZA LA PAGINA HACIA EL MISMO REPORTE.

        $validator = Validator::make($request->all(), [
            // 'id_trabajador' => 'required',
            'fecha'=>'required',
            'hora'=>'required',
            'tipo'=>'required',
            'justificacion'=>'required',
            'observacion'=>'required',
        ]);

        //si no cumple las validaciones, regresa
        if ($validator->fails()) {
            return redirect()->route('reporte.marcacion.justificar',[$id_trabajador])
                        ->withErrors($validator)
                        ->withInput();
        }


        $fecha_hora_marcacion=Carbon::createFromFormat('Y-m-d H:i:s',$request->input('fecha')." ".$request->input('hora'));


        $marcacion=new Marcacion();
        $marcacion->id_trabajador=$request->input('id_trabajador');
        $marcacion->fecha_hora_marcacion=$fecha_hora_marcacion;
        $marcacion->tipo_marcacion=$request->input('tipo');
        $marcacion->tipo_justificacion=$request->input('justificacion');
        $marcacion->observacion=$request->input('observacion');
        $marcacion->id_dispositivo=0;

        if ($marcacion->save()) {
            if ($request->get('volver')=="lista-trabajadores") {
                return redirect()->route('reporte.trabajadores');
            }else{
                return redirect()->route('reporte.preview',[$id_trabajador]);
            }

            
        }

    }
}
