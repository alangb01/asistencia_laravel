<?php

namespace App\Http\Controllers\Marcador;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Admin\Personal\Trabajador;

class TrabajadorController extends Controller
{
    const DIR_IMAGENES="personal/trabajador/";
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        //ACTUALIZAR HUELLA DIGITAL

        $trabajador=Trabajador::findOrFail($id);
        // $trabajador->
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }



    public function buscar(Request $request,$dni){
        // echo $request->input('dni');
        $trabajador=Trabajador::where('dni','=',$dni)->first();
        $this->cargarImagen($trabajador);
       // dd($trabajador);
        $data_trabajador=array("id"=>$trabajador->id,
            "nombre"=>$trabajador->nombre,
            "apellido"=>$trabajador->apellido,
            "dni"=>$trabajador->dni,
            "cargo"=>$trabajador->cargo->nombre,
            "url_foto"=>$trabajador->url_foto
            );
        // $result=array('success'=>1,'trabajador'=>$trabajador);

        return response()
            ->json(['success' => 1, 'trabajador' => $data_trabajador]);
    }

    public function cargarHuellas(Request $request){
        // echo $request->input('dni');
        $listado_trabajadores=\DB::table('personal_trabajadores')->select('id','huella')
            ->whereNotNull("huella")
            ->where('huella','!=','')
            ->get();
        
        $data_listado=collect();
        foreach ($listado_trabajadores as $id => $item) {
            if ($item->huella!=null) {
                $huella=base64_encode($item->huella);
            }else{
                $huella="";
            }
            

            $trabajador=Trabajador::findOrFail($item->id);
            $this->cargarImagen($trabajador);

            $data_trabajador=array("id"=>$trabajador->id,
                "huella"=>$huella,
                "nombre"=>$trabajador->nombre,
                "apellido"=>$trabajador->apellido,
                "dni"=>$trabajador->dni,
                "cargo"=>$trabajador->cargo!=null?$trabajador->cargo->nombre:'naa',
                "url_foto"=>$trabajador->url_foto
            );

            $data_listado->push($data_trabajador);
        }

        return response()
            ->json(['success' => 1, 'trabajadores' => $data_listado]);
    }


   /* public function cargar(Request $request,$id){
        
        $trabajador=Trabajador::findOrFail($id);
        $this->cargarImagen($trabajador);
        //dd($trabajador->nombre);

        $data_trabajador=array("id"=>$trabajador->id,
            "nombre"=>$trabajador->nombre,
            "apellido"=>$trabajador->apellido,
            "dni"=>$trabajador->dni,
            "cargo"=>$trabajador->cargo->nombre,
            "url_foto"=>$trabajador->url_foto
            );
        // $result=array('success'=>1,'trabajador'=>$trabajador);

        return response()
            ->json(['success' => 1, 'trabajador' => $data_trabajador]);
    }
*/


    public function registrar(Request $request){
        // echo $request->input('dni');
        //dd($request->input('huella'));

        $huella=base64_decode($request->input('huella'));

        $trabajador=Trabajador::findOrFail($request->input('id_trabajador'));
        $trabajador->huella=$huella;

        $data_trabajador=array("id"=>$trabajador->id,
            "nombre"=>$trabajador->nombre,
            "apellido"=>$trabajador->apellido,
            "dni"=>$trabajador->dni,
            "cargo"=>$trabajador->cargo->nombre,
            "url_foto"=>$trabajador->url_foto
            );

        if ($trabajador->save()) {
            return response()
                ->json(['success' => 1, 'trabajador' => $data_trabajador]);
        }else{
            return response()
                ->json(['success' => 0]);
        }
        //$this->cargarImagen($trabajador);
        //dd($trabajador->nombre);

        // $result=array('success'=>1,'trabajador'=>$trabajador);

        
    }

    private function cargarImagen(Trabajador $trabajador){


        $archivo_imagen=self::DIR_IMAGENES.$trabajador->ruta_foto;
        if (trim($trabajador->ruta_foto)!="" && \Storage::disk('local')->exists($archivo_imagen)) {
            $contenido=\Storage::disk('local')->get($archivo_imagen);
            \Storage::disk('url')->put($archivo_imagen,$contenido);
            $trabajador->url_foto=asset($archivo_imagen);
        }
    }
}
