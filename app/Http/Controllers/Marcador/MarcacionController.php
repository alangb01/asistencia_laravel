<?php

namespace App\Http\Controllers\Marcador;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Admin\Asistencia\Trabajador;
use App\Admin\Asistencia\Marcacion;
use App\Admin\Util\Util;
use Carbon\Carbon;

class MarcacionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id_trabajador)
    {
        //
        //dd("aca");
        //return response()->json(['success'=>1]);

        $trabajador=Trabajador::findOrFail($id_trabajador);


        $marcacion=new Marcacion();
        $marcacion->id_trabajador=$id_trabajador;
        $marcacion->fecha_hora_marcacion=Carbon::now()->format('Y-m-d H:i:s');

        $ultima_marcacion=$trabajador->ultimaMarcacionHoy();
        if ($ultima_marcacion==null || $ultima_marcacion->tipo_marcacion==$marcacion::TIPO_SALIDA) {
            $marcacion->tipo_marcacion=$marcacion::TIPO_ENTRADA;
        }else{
            $marcacion->tipo_marcacion=$marcacion::TIPO_SALIDA;
        }

        $marcacion->tipo_justificacion=$marcacion::JUSTIFICACION_NORMAL;

       

        if($marcacion->save()){ 
            $marcacion=Marcacion::findOrFail($marcacion->id);


            $mañana=Carbon::now()->today();
            $tarde=Carbon::now()->today()->addHour(12);
            $noche=Carbon::now()->today()->addHour(18);

            if ($marcacion->fecha_hora_marcacion->timestamp>=$mañana->timestamp
                && $marcacion->fecha_hora_marcacion->timestamp<$tarde->timestamp) {
                # code...
                $saludo="M";
            }else if ($marcacion->fecha_hora_marcacion->timestamp>=$tarde->timestamp
                && $marcacion->fecha_hora_marcacion->timestamp<$noche->timestamp) {
                # code...
                $saludo="T";
            }else{
                $saludo="N";
            }

            $marcacion=array("dia"=>Util::diaToString($marcacion->fecha_hora_marcacion),
                "fecha"=>$marcacion->fecha_hora_marcacion->format("Y-m-d"),
                "hora"=>$marcacion->fecha_hora_marcacion->format("h:i:s a"),
                "tipo"=>$marcacion->tipoToString(),
                "nombre"=>$trabajador->nombre,
                "apellido"=>$trabajador->apellido,
                "cargo"=>$trabajador->cargo,
                "saludo"=>$saludo
                );

            return response()
                ->json(['success' => 1, 'ultima_marcacion' => $marcacion]);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
