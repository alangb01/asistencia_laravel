<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;

use App\Admin\Util\Util;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use View;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('validar.sesion');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       // $usuario=Auth::user();
        // $data=compact('usuario');
        // return View::make('admin.bienvenido',$data);
        // return view('admin.bienvenido');
        return redirect()->route('reporte.trabajadores');
    }

    public function calcularTiempoDesdeFechaHora(Request $request){
        // echo "x";
        $fecha_hora_inicio=Carbon::createFromFormat('Y-m-d H:i:s',$request->get('fecha_inicio')." ".$request->get('hora_inicio'));
        $fecha_hora_termino=Carbon::createFromFormat('Y-m-d H:i:s',$request->get('fecha_termino')." ".$request->get('hora_termino'));

        $util=new Util();
        return $util->calcularTiempoDesdeCarbon($fecha_hora_inicio,$fecha_hora_termino);
    }

    public function calcularTiempoDesdeFecha(Request $request){
        $fecha_hora_inicio=Carbon::createFromFormat('Y-m-d H:i:s',$request->get('fecha_inicio')." 00:00:00");
        $fecha_hora_termino=Carbon::createFromFormat('Y-m-d H:i:s',$request->get('fecha_termino')." 00:00:00");
        $fecha_hora_termino->addDay();

        $util=new Util();
        return $util->calcularTiempoDesdeCarbon($fecha_hora_inicio,$fecha_hora_termino);
    }

    public function calcularTiempo(Request $request){
        $fecha_hora_inicio=Util::crearCarbonDesdeParametros($request->get('fecha_inicio'),$request->get('hora_inicio'));
        $fecha_hora_termino=Util::crearCarbonDesdeParametros($request->get('fecha_termino'),$request->get('hora_termino'));
        
        return Util::calcularTiempoDesdeCarbon($fecha_hora_inicio,$fecha_hora_termino);
    }
}
