<?php 

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class ValidarSesion
{
	
	/**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        // dd(" desde verificar sesion");

        $usuario = Auth::user();

        if ($usuario->forzar_cambio_clave==1) {
        	return redirect()->route('usuario.cambiar-clave');
        }
       /* if (Auth::guard($guard)->guest()) {
            if ($request->ajax() || $request->wantsJson()) {
                return response('Unauthorized.', 401);
            } else {
                return redirect()->guest('login');
            }
        }*/

        //VERIFICAR SI DEBE CAMBIAR CLAVE
        return $next($request);
    }
}