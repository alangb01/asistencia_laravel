//envia una alerta cada vez que se clickea en eliminar un registro con la clase .btn-eliminar
$(document).on('click',"form .btn-eliminar",function(e){
	e.preventDefault();
	if (confirm('¿Desea eliminar el item indicado?')) {
		$(this).parents("form").submit();
	};
})

$(document).on('ready',function(){
	$('.datepicker').datetimepicker({
		locale: 'es',
		format: 'YYYY-MM-DD'
	});
	$('.datepicker_fecha_nacimiento').datetimepicker({
		locale: 'es',
		format: 'YYYY-MM-DD',
		viewMode: 'years',
	});

	$('.timepicker').datetimepicker({
		format: 'HH:mm:ss',
	});


	$('.datepicker-periodo').datetimepicker({
		locale: 'es',
		format: 'YYYY-MM-DD'
	});

})