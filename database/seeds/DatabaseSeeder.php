<?php

use Illuminate\Database\Seeder;

use App\User as Usuario;
// use App\Admin\Personal\Rol;
use App\Admin\Personal\Sucursal;
use App\Admin\Personal\Cargo;
use App\Admin\Personal\Trabajador;

// use App\Admin\Personal\Permiso;
// use App\Admin\Personal\PermisoDeUsuario;

// asistencia
use App\Admin\Asistencia\Marcacion;
use App\Admin\Asistencia\Suspencion;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        // $this->call(TrabajadoresTableSeeder::class);
        $this->call(UsuariosTableSeeder::class);
        // $this->call(CargoTableSeeder::class);
        // $this->call(SucursalTableSeeder::class);
        // $this->call(PermisoTableSeeder::class);


        // ASISTENCIA
        // $this->call(MarcacionTableSeeder::class);
        // $this->call(SuspencionTableSeeder::class);
        // $this->call(PermisoDeUsuarioTableSeeder::class);
    }
}


class UsuariosTableSeeder extends Seeder {

    public function run()
    {
        DB::table('users')->truncate();

        Usuario::create(['id_trabajador'=>1,
            'name' => 'charly',
        	'email' => 'alangb01@gmail.com',
            'password' => bcrypt('030496eagb'),
            'nivel_acceso'=>Usuario::NIVEL_GERENTE_GENERAL]);

       // factory(Usuario::class,20)->create();
    }
}

class TrabajadoresTableSeeder extends Seeder {

    public function run()
    {
        DB::table('personal_trabajadores')->truncate();

        Trabajador::create(['id'=>'1',
            'id_cargo' => '1',
            'dni'=>'43167361','nombre' => 'charly',
            'apellido' => 'gastelo',
            'correo_personal' => 'alangb01@gmail.com',
            'correo_trabajo' => 'alan.gastelo@actibyte.pe',
            'direccion' => 'Roma 360 Urrunaga',
            'telefono_celular' => '978772143',
            'fecha_nacimiento'=> '1985-07-04',
            'estado'=>'AC']);

        factory(Trabajador::class,20)->create();
    }
}

class CargoTableSeeder extends Seeder {

    public function run()
    {
        DB::table('personal_cargos')->truncate();

        factory(Cargo::class,20)->create();
    }
}

class SucursalTableSeeder extends Seeder {

    public function run()
    {
        DB::table('personal_sucursales')->truncate();

        factory(Sucursal::class,20)->create();
    }
}
/*class PermisoTableSeeder extends Seeder {

    public function run()
    {
        DB::table('permisos')->truncate();

        factory(Permiso::class,20)->create();
    }
}

class PermisoDeUsuarioTableSeeder extends Seeder {

    public function run()
    {
        DB::table('permisos_usuario')->truncate();

        factory(PermisoDeUsuario::class,20)->create();
    }
}*/


// ASISTENCIA
class MarcacionTableSeeder extends Seeder {

    public function run()
    {
        DB::table('asistencia_marcaciones')->truncate();

        factory(Marcacion::class,20)->create();
    }
}

class SuspencionTableSeeder extends Seeder {

    public function run()
    {
        DB::table('asistencia_suspenciones')->truncate();

        factory(Suspencion::class,5)->create();
    }
}