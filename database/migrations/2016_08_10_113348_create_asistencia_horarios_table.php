<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAsistenciaHorariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('asistencia_horarios', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('id_trabajador')->unsigned();
            $table->date('fecha_inicio');
            $table->date('fecha_termino');
            $table->string('estado');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('asistencia_horarios');
    }
}
