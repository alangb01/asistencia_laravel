<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAsistenciaReportesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('asistencia_reportes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_trabajador');
            $table->date('fecha_inicio');
            $table->date('fecha_final');
            $table->date('dias_laborables');
            $table->date('dias_asistidos');
            $table->date('tiempo_laborable');
            $table->date('tiempo_asistido_regular');
            $table->date('tiempo_asistido_justificado');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('asistencia_reportes');
    }
}
