<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePersonalTrabajadoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('personal_trabajadores', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('id_cargo')->unsigned();
            $table->integer('id_sucursal')->unsigned();
            $table->string('dni')->unique();
            $table->string('nombre');
            $table->string('apellido');
            $table->date('fecha_nacimiento')->nullable();
            $table->string('correo_personal')->nullable();
            $table->string('correo_trabajo')->nullable();
            $table->string('telefono_fijo')->nullable();
            $table->string('telefono_celular')->nullable();
            $table->string('direccion')->nullable();
            $table->string('sexo')->nullable();
            // $table->string('direccion_coordenada');
            // $table->string('direccion_referencia');
            $table->string('ruta_foto')->nullable();
            $table->string('estado');
            $table->binary('huella');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('personal_trabajadores');
    }
}
