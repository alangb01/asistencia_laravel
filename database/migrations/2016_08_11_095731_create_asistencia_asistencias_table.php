<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAsistenciaAsistenciasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('asistencia_asistencias', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('id_reporte')->unsigned();
            $table->date('fecha');
            $table->integer('id_marcacion_entrada')->unsigned();
            $table->integer('id_marcacion_salida')->unsigned();
            $table->string('estado');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('asistencia_asistencias');
    }
}
