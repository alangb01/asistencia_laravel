<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAsistenciaPermisosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('asistencia_permisos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_trabajador')->unsigned();
            $table->datetime('fecha_hora_inicio');
            $table->datetime('fecha_hora_termino');
            $table->string('tipo');
            $table->string('descripcion');
            $table->boolean('cronometrado');
            $table->string('ruta_adjunto');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('asistencia_permisos');
    }
}
