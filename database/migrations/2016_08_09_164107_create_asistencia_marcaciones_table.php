<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAsistenciaMarcacionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('asistencia_marcaciones', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_dispositivo')->unsigned();
            $table->integer('id_trabajador')->unsigned();
            $table->datetime('fecha_hora_marcacion');
            $table->integer('tipo_marcacion')->unsigned();
            $table->integer('tipo_justificacion')->unsigned();
            $table->string('observacion');
            $table->string('id_horario');
            $table->string('estado');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('asistencia_marcaciones');
    }
}
