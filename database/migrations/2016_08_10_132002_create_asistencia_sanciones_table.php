<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAsistenciaSancionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('asistencia_sanciones', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('id_trabajador')->unsigned();
            $table->datetime('fecha_hora_inicio');
            $table->datetime('fecha_hora_termino');
            $table->string('descripcion');
            $table->string('estado');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('asistencia_sanciones');
    }
}
