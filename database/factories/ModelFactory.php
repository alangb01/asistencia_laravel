<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->safeEmail,
        'password' => bcrypt(str_random(10)),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Admin\Personal\Trabajador::class, function (Faker\Generator $faker) {
    return [
        'id_cargo' => $faker->numberBetween($min=1,$max=20),
        'dni' => $faker->randomNumber($nbDigits = 8),
        'nombre' => $faker->firstName,
        'apellido' => $faker->lastName,
        'correo_personal' => $faker->email,
        'correo_trabajo' => $faker->email,
        'telefono_fijo' =>  $faker->phoneNumber,
        'telefono_celular' =>  $faker->phoneNumber,
        'direccion' =>  $faker->address,
        'fecha_nacimiento' => $faker->date($format = 'Y-m-d', $min = '-18 years', $max = '-32 years'),
        // 'foto' => "foto.jpg",
        'estado' => 'AC'
    ];  
});

$factory->define(App\Admin\Personal\Cargo::class, function (Faker\Generator $faker) {
    return [
        'nombre' => $faker->firstName,
        'estado' => 'AC'
    ];  
});

$factory->define(App\Admin\Personal\Sucursal::class, function (Faker\Generator $faker) {
    return [
        'nombre' => $faker->firstName,
        'direccion' => $faker->address,
        'telefono' => $faker->phoneNumber,
        'estado' => 'AC'
    ];  
});

/*
$factory->define(App\Admin\Personal\Permiso::class, function (Faker\Generator $faker) {
    return [
        'nombre' => $faker->firstName,
        'comando' => "comando",
        'estado' => 'AC'
    ];  
});

$factory->define(App\Admin\Personal\PermisoDeUsuario::class, function (Faker\Generator $faker) {
    return [
        'id_usuario' => $faker->numberBetween($min=1,$max=20),
        'id_permiso' => $faker->numberBetween($min=1,$max=20),
        'estado' => 'AC'
    ];  
});
*/

$factory->define(App\Admin\Asistencia\Marcacion::class, function (Faker\Generator $faker) {
    return [
        'id_trabajador' => $faker->numberBetween($min=1,$max=20),
        'fecha_hora_marcacion' => $faker->dateTimeBetween($startDate = '-2 years', $endDate = 'now', $timezone = date_default_timezone_get()),
        'tipo_marcacion' => $faker->numberBetween($min=1,$max=2),
        'tipo_justificacion' => $faker->numberBetween($min=1,$max=5),
        'observacion' => $faker->lastName,
        'estado' => 'AC'
    ];  
});



$factory->define(App\Admin\Asistencia\Suspencion::class, function (Faker\Generator $faker) {
    return [
        'fecha_hora_inicio' => $faker->dateTimeBetween($startDate = '-1 years', $endDate = '+1 years', $timezone = date_default_timezone_get()),
        'fecha_hora_termino' => $faker->dateTimeBetween($startDate = '-1 years', $endDate = '+1 years', $timezone = date_default_timezone_get()),
        'descripcion' => $faker->lastName,
        'estado' => 'AC'
    ];  
});