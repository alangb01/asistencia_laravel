@extends ("admin.plantilla")
@section('contenido_pagina')
<div class="container">
	<div class="panel panel-default">
		<!-- Default panel contents -->
		<div class="panel-heading">
			<div class="row text-center">
				<div class="col-md-3"><a href="{{ $url_volver}}" class="btn btn-primary pull-left">Volver</a></div>
				<div class="col-md-6">
					<h4>Formulario de turno</h4>
				</div>
				<div class="col-md-3">
					<form action="{{ $turno->url_eliminar }}" method="post" class=" inline pull-right">
						{{ csrf_field() }}
						{!! method_field('DELETE') !!}
						<div class="btn-group btn-group-sm">
							<button class="btn btn-danger pull-right" type="submit">Eliminar</button>
						</div>
					</form>	
				</div>
			</div>
			
			
			
		</div>
		<div class="panel-body">
	   		<form role="form" class="col-md-12" action="{{ $turno->action_form }}" method="post" accept-charset="UTF-8" enctype="multipart/form-data">
	   			{{ csrf_field() }}
	   			@if ($turno->id)
				{!!  method_field('PUT') !!}
	   			@endif
	   			<input type="hidden" name="id_horario" value="{{ $turno->horario->id }}">
	   			<div class="form-group">
			    	<label for="trabajador">trabajador:</label>
			    	{{ $turno->horario->trabajador }}
			  	</div>
			  	<div class="form-group">
			    	<label for="dia_semana">Dia de semana:</label>
			    	<div class="form-group">
			    		<select class="form-control" id="dia_semana" name="dia_semana">
			    		@if(isset($dias_semana) && count($dias_semana)>0)
						<option value="">Seleccione un dia</option>
			    		<?php foreach ($dias_semana as $id => $dia): ?>
			    		<option value="{{ $dia->id }}" {{ $turno->dia_semana==$dia->id?'selected':'' }}>{{ $dia->texto }}</option>
			    		<?php endforeach ?>
			    		@else
			    		<option value="">No hay lista de dias</option>
						@endif
					</select>
					@if($errors->has('dia_semana')!=null)
			    	<span class="help-inline">{{ $errors->first('dia_semana') }}</span>
			    	@endif
			    	</div>
			  	</div>
			  	<div class="form-group">
			    	<label for="hora">periodo:</label>
			    	<div class="form-inline">
			    		
			    		<input type="text" class="form-control timepicker" id="hora_entrada" name="hora_entrada" value="{{ $turno->fecha_hora_entrada->format('H:i:s') }}">
				    	
						<input type="text" class="form-control timepicker" id="hora_salida" name="hora_salida" value="{{ $turno->fecha_hora_salida->format('H:i:s') }}">
						@if($errors->has('hora_entrada')!=null)
				    	<span class="help-inline">{{ $errors->first('hora_entrada') }}</span>
				    	@endif
				    	@if($errors->has('hora_salida')!=null)
				    	<span class="help-inline">{{ $errors->first('hora_salida') }}</span>
				    	@endif
			    	</div>
			  	</div>
			  	<div class="form-group">
			    	<label for="fecha_termino">Tiempo transcurrido:</label>
			    	<input type="text" id="tiempo_calculado" class="form-control" value="" readonly>
			  	</div>
			  	<button type="submit" class="btn btn-success pull-right">Registrar</button>
			</form>
		</div>
	</div>
</div>
<script>
	$('.datepicker').datetimepicker({
    	locale: 'es',
    	format: 'YYYY-MM-DD'
    }).on('dp.change', function(e) {
    	calcularDiferencia();
	});;

    $('.timepicker').datetimepicker({
    	locale: 'es',
    	format: 'HH:mm:ss',
    }).on('dp.change', function(e) {
    	calcularDiferencia();
	});

    function calcularDiferencia(){
	data={
		// fecha_inicio:$("#fecha_inicio").val(),
			hora_inicio:$("#hora_entrada").val(),	
			// fecha_termino:$("#fecha_termino").val(),
			hora_termino:$("#hora_salida").val()
		};

		$.get("{{ route('util.calcular.tiempo') }}",data,function(resultado){
			$("#tiempo_calculado").val(resultado);
		})
	}

	calcularDiferencia();
</script>
@endsection