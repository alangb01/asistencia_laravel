@extends ("admin.plantilla")
@section('contenido_pagina')
<div class="container">
	@include('admin.comun.notificaciones')	
	<div class="panel panel-default">
		<!-- Default panel contents -->
		<div class="panel-heading">
			<div class="row">
		  		<div class="col-md-3">
			  		<a href="{{ $url_volver}}" class="btn btn-primary">Volver</a>
			  	</div>
			  	<div class="col-md-6 text-center">
			  		<h4>Reporte - Registrar Horario</h4>
			  	</div>
			  	<div class="col-md-3">
			  	</div>
		  	</div>
		</div>
		<div class="panel-body">
	   		<form role="form" class="col-md-12" action="{{ $horario->action_form }}" method="post" accept-charset="UTF-8" enctype="multipart/form-data">
	   			{{ csrf_field() }}
				<?php if ($horario->id>0): ?>
				{!!  method_field('PUT') !!}
				<?php endif ?>

	   			<input type="hidden" name="id_trabajador" value="{{ $horario->trabajador->id }}">
	   			<div class="form-group">
			    	<label for="trabajador">trabajador:</label>
			    	{{ $horario->trabajador }}
			  	</div>
			  	<div class="form-group ">
			    	<label for="fecha_inicio">Fecha inicio:</label>
			    	<div class="form-group">
			    		<input type="text" class="form-control datepicker date" id="fecha_inicio" name="fecha_inicio" value="{{ $horario->fecha_inicio->format('Y-m-d') }}">
				    	@if($errors->has('fecha_inicio')!=null)
				    	<span class="help-inline">{{ $errors->first('fecha_inicio') }}</span>
				    	@endif
			    		
			    	</div>
			  	</div>
			  	<div class="form-group">
			    	<label for="fecha_termino">Fecha termino:</label>
			    	<div class="form-group">
			    		<input type="text" class="form-control datepicker date" id="fecha_termino" name="fecha_termino" value="{{ $horario->fecha_termino->format('Y-m-d') }}">
				    	@if($errors->has('fecha_termino')!=null)
				    	<span class="help-inline">{{ $errors->first('fecha_termino') }}</span>
				    	@endif
			    		
			    	</div>
			  	</div>
			  	<div class="form-group">
			    	<label for="fecha_termino">Tiempo transcurrido:</label>
			    	<div class="form-group">
			    		<input type="text" id="tiempo_calculado" class="form-control" value="" readonly>
			    	</div>
			  	</div>
			  	

			  	<button type="submit" class="btn btn-success pull-right">Registrar</button>
			</form>
		</div>
	</div>
</div>

<script type="text/javascript">
    
    $('.datepicker').datetimepicker({
    	locale: 'es',
    	format: 'YYYY-MM-DD'
    }).on('dp.change', function(e) {
    	calcularDiferencia();
	});

    

function calcularDiferencia(){
	data={fecha_inicio:$("#fecha_inicio").val(),
			// hora_inicio:$("#hora_inicio").val(),	
			fecha_termino:$("#fecha_termino").val(),
			// hora_termino:$("#hora_termino").val()
		};

	$.get("{{ route('util.calcular.tiempo--fecha') }}",data,function(resultado){
		$("#tiempo_calculado").val(resultado);
	})
}
calcularDiferencia();
</script>
@endsection