@extends ("admin.plantilla")
@section('contenido_pagina')
<div class="container">
	@include('admin.comun.notificaciones')
	<div class="panel panel-default">
		<!-- Default panel contents -->
		<div class="panel-heading">
			<div class="row text-center">
				<div class="col-md-4"><a href="{{ $url_volver}}" class="btn btn-primary pull-left">Volver</a></div>
				<div class="col-md-4">
					<h4>Cronograma de horario</h4>
				</div>
				<div class="col-md-4">
					{{-- <a href="{{ $url_nuevo_turno_programado }}" class="btn btn-default pull-right">Crear turno programado</a> --}}
					<a href="{{ $url_nuevo_turno_dia_semana }}" class="btn btn-success pull-right">Crear turno </a>
				</div>
			</div>
			
			
			
		</div>
		<div class="panel-body">
		  	<div class="row col-md-12">
		  		<div class="col-md-1">DNI</div>
			  	<div class="col-md-2">{{ $horario->trabajador->dni }}</div>
			  	<div class="col-md-2">Trabajador</div>
			  	<div class="col-md-2">{{ $horario->trabajador }}</div>
			  	<div class="col-md-1">Cargo</div>
			  	<div class="col-md-2">{{ $horario->trabajador->cargo }}</div>
		  	</div>
		  	<div class="row col-md-12">
		  		<div class="col-md-1 ">Horario</div>
			  	<div class="col-md-2">{{ $horario->fecha_inicio->format('Y-m-d')." al ".$horario->fecha_termino->format('Y-m-d') }}</div>
			  	<div class="col-md-2">Horas a la semana</div>
			  	<div class="col-md-3">{{ $horario->totalHorasSemanales() }}</div>
		  	</div>
	   		<!-- <table class="table table-hover table-bordered">
	   					   	<thead>
	   					   		<tr>
	   					   			<th>Id</th>
	   					   			<th>Fecha inicio</th>
	   					   			<th>Fecha termino</th>
	   					   			<th>Opciones</th>
	   					   		</tr>
	   					   	</thead>
	   					   	<tbody>
	   					   		@if($horario->turnos!=null && count($horario->turnos)>0)
	   							@foreach($horario->turnos as $id=>$turno)
	   								<tr>
	   									<td>{{ $turno->id }}</td>
	   									<td>{{ $turno->diaToString() }}</td>
	   									<td >{{ $turno->fecha_hora_entrada->format('h:i: a') }}</td>
	   									<td>{{ $turno->fecha_hora_salida->format('h:i: a') }}</td>
	   									<td>
	   										<div class="btn-group btn-group-sm">
	   											
	   										</div>
	   									</td>
	   								</tr>
	   							@endforeach
	   							@else
	   								<tr>
	   									<td>
	   										No hay turnos asignados
	   									</td>
	   								</tr>
	   							@endif
	   					   	</tbody>
	   					  
	   				  	</table> -->
		  	<div class="col-md-12 text-center">
		  		<h4>DISTRIBUCION DE HORAS</h4>
				
		  	</div>
		  	<div class="col-md-12 text-center">
		  		<form action="{{ $horario->url_cambiar_dia }}" method="post" class=" inline">
					{{ csrf_field() }}
					{!! method_field('PUT') !!}
					<div class="form-inline">
						Mover turnos del 
			    		<select class="form-control" id="dia_semana_actual" name="dia_semana_actual">
				    		@if(isset($dias_semana) && count($dias_semana)>0)
							<option value="">Dia actual</option>
				    		<?php foreach ($dias_semana as $id => $dia): ?>
				    		<option value="{{ $dia->id }}" >{{ $dia->texto }}</option>
				    		<?php endforeach ?>
				    		@else
				    		<option value="">No hay lista de dias</option>
							@endif
						</select>
						@if($errors->has('dia_semana_actual')!=null)
				    	<span class="help-inline">{{ $errors->first('dia_semana_actual') }}</span>
				    	@endif
				    	al
						<select class="form-control" id="dia_semana_nuevo" name="dia_semana_nuevo">
				    		@if(isset($dias_semana) && count($dias_semana)>0)
							<option value="">Nuevo</option>
				    		<?php foreach ($dias_semana as $id => $dia): ?>
				    		<option value="{{ $dia->id }}" >{{ $dia->texto }}</option>
				    		<?php endforeach ?>
				    		@else
				    		<option value="">No hay lista de dias</option>
							@endif
						</select>
						@if($errors->has('dia_semana_nuevo')!=null)
				    	<span class="help-inline">{{ $errors->first('dia_semana_nuevo') }}</span>
				    	@endif
						<button class="btn btn-danger " type="submit">
							Intercambiar dia
						</button>
					</div>
				</form>	
		  	</div>
		  	
		  	<div id="cronograma" class="col-md-12">
				<?php 
					$cronograma=$horario->getCronograma();
					$bloques=$cronograma->preparar();
				  ?>
				<div class="cronograma" style="width:{{ $cronograma->ancho }}px; height:{{ $cronograma->alto }}px">
					<div>
						@foreach($bloques['bloques_dia'] as $bloque)
						<div  class="grilla encabezado {{ $bloque->tipo }}" style="top:{{ ($bloque->y) }}px;left:{{ ($bloque->x) }}px;
							width:{{ ($bloque->ancho) }}px; height:{{ ($bloque->alto) }}px;">
							<div>{!! $bloque->texto !!}</div>
							<div class="total_horas">{!! $bloque->total!=""?"(".$bloque->total.")":'' !!}</div>		
						</div>
						@endforeach
					</div>
					<div>
						@foreach($bloques['bloques_hora'] as $bloque)
						<div  class="grilla encabezado {{ $bloque->tipo }}" style="top:{{ ($bloque->y) }}px;left:{{ ($bloque->x) }}px;
							width:{{ ($bloque->ancho) }}px; height:{{ ($bloque->alto) }}px;">{!! $bloque->texto !!} </div>
						@endforeach
					</div>
					<div style="">
						@foreach($bloques['bloques_celda'] as $bloque)
						<div  class="grilla {{ $bloque->tipo }}" style="top:{{ ($bloque->y) }}px;left:{{ ($bloque->x) }}px;
							width:{{ ($bloque->ancho) }}px; height:{{ ($bloque->alto) }}px;">{!! $bloque->texto !!} </div>
						@endforeach
					</div>
					<div>
						@foreach($bloques['bloques_turno'] as $bloque)
						<!-- <div class=" {{ $bloque->tipo }}" style="top:{{ ($bloque->y) }}px;left:{{ ($bloque->x) }}px;
							width:{{ ($bloque->ancho) }}px; height:{{ ($bloque->alto) }}px;"><span>{!! $bloque->texto !!}</span> </div> -->
						<a href="{{ $bloque->turno->url_editar }}" class="{{ $bloque->tipo }}" style="top:{{ ($bloque->y) }}px;left:{{ ($bloque->x) }}px;
							width:{{ ($bloque->ancho) }}px; height:{{ ($bloque->alto) }}px;"><span>{!! $bloque->texto !!}</span> </a>
						@endforeach
					</div>
				
				</div>
				<style>
					.cronograma{
						text-align:center;
						position:relative;
						margin:auto;
						border: 1px solid #ddd;
						display: table;
					}
					.cronograma .grilla{
						position: absolute;
						outline: none;
						border: 1px solid #ddd;
						display: table-cell;
					}
					.cronograma .turno{
						position: absolute;
					}
					
					

					.cronograma .encabezado{
						font-weight: bold;
						text-transform: uppercase;
						background-color: #f5f5f5;

					}
					.cronograma .total_horas{
						text-transform: lowercase;
					}

					.cronograma .turno{
						background: #ddddff;
						border:1px solid #dda;
						display: table;
					}
					.cronograma .turno>span{
						vertical-align: middle;
						display: table-cell;
						font-size: 10px;
					}
				</style>
		  	</div>
		</div>
	</div>
</div>
@endsection