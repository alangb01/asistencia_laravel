@extends ("admin.plantilla")

@section('contenido_pagina')
<div class="container">
	@include('admin.comun.notificaciones')
	<div class="panel panel-default">
	  <!-- Default panel contents -->
	  	<div class="panel-heading ">
	  		<div class="row text-center">
	  			<div class="col-md-3">
	  				<a href="{{ $url_volver}}" class="btn btn-primary pull-left">Volver</a>
	  			</div>
		  		<div class="col-md-6"><h4>Listado de horarios asignados</h4></div>
		  		<div class="col-md-3"><a href="{{ $url_horario_crear }}" class="btn btn-success pull-right">Crear horario</a></div>
	  		</div>
			
	  	</div>
	  	<div class="panel-body">
		  	<div class="row col-md-12">
		  		<div class="col-md-1">DNI</div>
			  	<div class="col-md-1">{{ $trabajador->dni }}</div>
			  	<div class="col-md-1">Trabajador</div>
			  	<div class="col-md-4">{{ $trabajador }}</div>
			  	<div class="col-md-1">Cargo</div>
			  	<div class="col-md-4">{{ $trabajador->cargo }}</div>
		  	</div>
		</div>
	  <table class="table">
	   	<thead>
	   		<tr>
	   			<th class="col-md-1">Id</th>
	   			<th>Fecha inicio</th>
	   			<th>Fecha termino</th>
	   			<th class="col-md-3 text-center">Horario/Turnos</th>
	   			<th class="col-md-2 text-center">Opciones</th>
	   		</tr>
	   	</thead>
	   	<tbody>
	   		@if($trabajador->horarios!=null && count($trabajador->horarios)>0)
			@foreach($trabajador->horarios as $id=>$horario)
				<tr>
					<td>{{ $horario->id }}</td>
					<td>{{ $horario->fecha_inicio->format('Y-M-d') }}</td>
					<td>{{ $horario->fecha_termino->format('Y-M-d') }}</td>
					<td class="text-center">
						<div class="btn-group btn-group-sm">
							<a href="{{ $horario->url_clonar }}" class="btn btn-default">
								<span class="glyphicon glyphicon-copy" aria-hidden="true" title="Clonar horario"></span>
								Clonar horario
							</a>
						</div>
						<div class="btn-group btn-group-sm">
							<a href="{{ $horario->url_cronograma }}" class="btn btn-primary">Visualizar horario</a>
						</div>
					</td>
					<td class=" text-center">
						
						
						<form action="{{ $horario->url_eliminar }}" method="post" class=" inline">
   							{{ csrf_field() }}
							{!! method_field('DELETE') !!}
							<div class="btn-group btn-group-sm">
								<a href="{{ $horario->url_editar }}" class="btn btn-default btn-editar">
									<span class="glyphicon glyphicon-pencil" aria-hidden="true" title="Editar"></span> 
								</a>
								<button class="btn btn-default btn-eliminar" type="submit">
									<span class="glyphicon glyphicon-remove" aria-hidden="true" title="Eliminar"></span>
								</button>
							</div>
						</form>	

	   		
					</td>
				</tr>
			@endforeach
			@else
				<tr>
					<td colspan="5">
						No hay horarios asignados
					</td>
				</tr>
			@endif
	   	</tbody>
	   	<tfoot>
	   		<tr>
	   			<td colspan="10">
	   				
	   			</td>
	   		</tr>
	   	</tfoot>
	  </table>
	</div>
	
</div>
@endsection