@extends ("admin.plantilla")
@section('contenido_pagina')
<div class="container">
	@include('admin.comun.notificaciones')
	<div class="panel panel-default">
		<!-- Default panel contents -->
		<div class="panel-heading">
			<div class="row">
		  		<div class="col-md-3">
			  		<a href="{{ $url_volver}}" class="btn btn-default">Volver</a>
			  	</div>
			  	<div class="col-md-6 text-center">
			  		<h4>Reporte - Registrar sanción</h4>
			  	</div>
			  	<div class="col-md-3">
			  	</div>
			  	<div class="clear"></div>
		  	</div>
		</div>
		<div class="panel-body">
	   		<form role="form" class="col-md-12" action="{{ $sancion->action_form }}" method="post" accept-charset="UTF-8" enctype="multipart/form-data">
	   			{{ csrf_field() }}
	   			<input type="hidden" name="id_trabajador" value="{{ $sancion->trabajador->id }}">
	   			<div class="form-group">
			    	<label for="trabajador">trabajador:</label>
			    	{{ $sancion->trabajador }}
			  	</div>
			  	
			  	<div class="form-group">
			    	<label for="fecha_inicio">Fecha y hora de inicio:</label>
			    	<input type="text" class="form-control datepicker date" id="fecha_inicio" name="fecha_inicio" value="{{ $sancion->fecha_hora_inicio->format('Y-m-d') }}">
			    	@if($errors->has('fecha_inicio')!=null)
			    	<span class="help-inline">{{ $errors->first('fecha_inicio') }}</span>
			    	@endif
			    	<div class="">
			    		<input type="hidden" class="form-control timepicker" id="hora_inicio" name="hora_inicio" value="{{ $sancion->fecha_hora_inicio->format('H:i') }}">
			    	</div>
			    	@if($errors->has('hora_inicio')!=null)
			    	<span class="help-inline">{{ $errors->first('hora_inicio') }}</span>
			    	@endif
			  	</div>
			  	<div class="form-group">
			    	<label for="fecha_termino">Fecha y hora de termino:</label>
			    	<input type="text" class="form-control datepicker date" id="fecha_termino" name="fecha_termino" value="{{ $sancion->fecha_hora_termino->format('Y-m-d') }}">
			    	@if($errors->has('fecha_termino')!=null)
			    	<span class="help-inline">{{ $errors->first('fecha_termino') }}</span>
			    	@endif
			    	<div class="">
			    		<input type="hidden" class="form-control timepicker" id="hora_termino" name="hora_termino" value="{{ $sancion->fecha_hora_termino->format('H:i') }}">
			    	</div>
			    	@if($errors->has('hora_termino')!=null)
			    	<span class="help-inline">{{ $errors->first('hora_termino') }}</span>
			    	@endif
			  	</div>
			  	<div class="form-group">
			    	<label for="fecha_termino">Tiempo transcurrido:</label>
			    	<input type="text" id="tiempo_calculado" value="">
			  	</div>
			  	<div class="form-group">
			    	<label for="descripcion">descripcion:</label>
			    	<textarea name="descripcion" id="descripcion" class="form-control" >{{ $sancion->descripcion }}</textarea>
			    	@if($errors->has('descripcion')!=null)
			    	<span class="help-inline">{{ $errors->first('descripcion') }}</span>
			    	@endif
			  	</div>
			  	<!-- <div class="checkbox">
			  				    	<label><input type="checkbox"> Remember me</label>
			  	</div> -->
			  	<button type="submit" class="btn btn-default">Registrar</button>
			</form>
		</div>
	</div>
</div>
<script type="text/javascript">
    
    $('.datepicker').datetimepicker({
    	locale: 'es',
    	format: 'YYYY-MM-DD'
    }).on('dp.change', function(e) {
    	calcularDiferencia();
	});

    $('.timepicker').datetimepicker({
    	locale: 'es',
    	format: 'HH:mm:ss',
    }).on('dp.change', function(e) {
    	calcularDiferencia();
	});

	function calcularDiferencia(){
	data={fecha_inicio:$("#fecha_inicio").val(),
			// hora_inicio:$("#hora_inicio").val(),	
			fecha_termino:$("#fecha_termino").val(),
			// hora_termino:$("#hora_termino").val()
		};

		$.get("{{ route('util.calcular.tiempo--fecha') }}",data,function(resultado){
			$("#tiempo_calculado").val(resultado);
		})
	}

	calcularDiferencia();
</script>
@endsection