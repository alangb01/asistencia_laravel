{{-- LISTADO DE ASISTENCIAS --}}
<table class="table table-hover">
  	{{-- <caption class="text-center"><h4>LISTADO DE ASISTENCIAS</h4></caption> --}}
  	<caption class="text-center">
  		<div class="col-md-4"></div>
  		<div class="col-md-4"><h4>LISTADO DE ASISTENCIAS</h4></div>
  		<div class="col-md-4"></div>
  	</caption>
   	<thead>
   		<tr>
   			<th>Dia</th>
   			<th>Fecha</th>
   			<th>Entrada</th>
   			<th>Salida</th>
   			<th>Tiempo</th>
   			<th>Opciones</th>
   		</tr>
   	</thead>
   	<tbody>
   		@if($reporte->asistenciasProcesadas()!=null && count($reporte->asistenciasProcesadas())>0)
		@foreach($reporte->asistenciasProcesadas() as $id=>$asistencia)
			<tr class="{{ $asistencia->estado=='VR'?'fila_verificar':'' }}">
				<td>{{ $asistencia->diaToString() }}</td>
				<td>{{ Util::fechaToString($asistencia->fecha()) }}</td>
				<td>{{ $asistencia->entrada->fecha_hora_marcacion->format('h:m:s a') }}</td>
				<td>{{ $asistencia->salida->fecha_hora_marcacion->format('h:m:s a') }}</td>
				<td>{{ $asistencia->tiempoAcumulado() }}</td>
        <td></td>
			</tr>
		@endforeach
		@else
			<tr>
				<td>
					No hay asistencias para mostrar
				</td>
			</tr>
		@endif
   	</tbody>
   	<tfoot>
   		<tr>
   			<td colspan="10">
   				
   			</td>
   		</tr>
   	</tfoot>
</table>