<table class="table table-hover table-bordered ">
	<caption>
		<h4>Estadisticas de asistencia</h4>
	</caption>
	<tbody>
		
		<tr>
			<th>Dias programados</th>
			<td>{{ $reporte->dias_programados }}</td>
			<th>Tiempo programado</th>
			<td>{{ $reporte->tiempoProgramadoToString() }} </td>
		</tr>
		<tr>
			<th>Dias feriados</th>
			<td>{{ $reporte->dias_suspenciones }}</td>
			<th>Tiempo feriado</th>
			<td>{{ $reporte->tiempoSuspencionesToString() }} </td>
		</tr>
		<tr>
			<th>Dias laborable [Efectivo]</th>
			<td>{{ $reporte->dias_laborables }}</td>
			<th>Tiempo laborable [Efectivo]</th>
			<td>{{ $reporte->tiempoLaborableToString() }} </td>
		</tr>
		<tr>
			<th>Dias asistidos</th>
			<td>{{ $reporte->dias_asistidos }}</td>
			<th>Tiempo asistido</th>
			<td>{{ $reporte->tiempoAsistidoToString() }}</td>
		</tr>
		
		<tr>
			<th>Ratio dias</th>
			<td>{{ $reporte->ratioDias("%") }}</td>
			<th>Ratio tiempo</th>
			<td>{{ $reporte->ratioTiempo("%") }}</td>
		</tr>
	</tbody>
	<tfoot>
		<tr>
			<td colspan="4"></td>
		</tr>
	</tfoot>
</table>
