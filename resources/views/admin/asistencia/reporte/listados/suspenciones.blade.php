{{-- SUSPENCIONES GENERADAS --}}
<table class="table table-hover">
		<caption class="text-center">
  		<div class="col-md-3"></div>
  		<div class="col-md-6"><h4>LISTADO DE FERIADOS/SUSPENCIONES</h4></div>
  		<div class="col-md-3"></div>
  	</caption>
   	<thead>
   		<tr>
   			<th>Fecha Hora inicio</th>
   			<th>Fecha hora termino</th>
   			<th>Descripcion</th>
        <th>Opciones</th>
   		</tr>
   	</thead>
   	<tbody>
   		@if($reporte->suspencionesProcesadas()!=null && count($reporte->suspencionesProcesadas())>0)
		@foreach($reporte->suspencionesProcesadas() as $id=>$suspencion)
			<tr class="{{ $suspencion->estado=='VR'?'fila_verificar':'' }}">
				<td>{{ Util::diaToString($suspencion->fecha_hora_termino) }} </td>
				<td>{{ $suspencion->fecha_hora_termino->format('l Y-M-d H:i') }}</td>
				<td>{{ $suspencion->descripcionToString() }}</td>
        <td>{{ $suspencion->cruce_asistencia }}</td>
			</tr>
		@endforeach
		@else
			<tr>
				<td>
					No hay suspenciones para mostrar
				</td>
			</tr>
		@endif
   	</tbody>
   	<tfoot>
   		<tr>
   			<td colspan="10">
   				
   			</td>
   		</tr>
   	</tfoot>
</table>