{{-- REPORTE -> LISTADO MARCACIONES --}}
<table class="table table-hover table-bordered table-condensed">
  	<caption class="text-center">
  		<div class="col-md-4"></div>
  		<div class="col-md-4"><h4>DETALLE POR DIA</h4></div>
  		<div class="col-md-4">
		<!-- 	<a href="{{ $reporte->url_marcacion_justificar }}" class="btn btn-primary pull-right">Justificar marcacion</a> -->
  		</div>
  	</caption>
   	<thead>
   		<tr>
   			<th class="col-md-1">Dia</th>
   			<th class="col-md-1">Fecha</th>
   			<th class="col-md-2" colspan="2">Turnos</th>
   			<th class="col-md-2" colspan="2">Feriado</th>
   			<th class="col-md-2" colspan="2">Asistencias</th>
   			<th class="col-md-4">Marcaciones</th>		
   		</tr>
   	</thead>
   	<tbody>
   		@if($reporte->reportesDiariosProcesados()!=null && count($reporte->reportesDiariosProcesados())>0)
		@foreach($reporte->reportesDiariosProcesados() as $id=>$reporte_diario)
			@if($reporte_diario->fecha->dayOfWeek==0)
			<tr>
				   			<th class="col-md-1">Dia</th>
				   			<th class="col-md-1">Fecha</th>
				   			<th class="col-md-2" colspan="2">Turnos</th>
				   			<th class="col-md-2" colspan="2">Feriado</th>
				   			<th class="col-md-2" colspan="2">Asistencias</th>
				   			<th class="col-md-4">Marcaciones</th>
				   		</tr>
			@endif
			<tr class="{{ $reporte_diario->fecha->dayOfWeek==0?'disabled':'' }}">
				<td>{{ $reporte_diario->diaToString() }}</td>
				<td>{{ $reporte_diario->fecha->format('Y-M-d') }}</td>
				<td>
					@if($reporte_diario->listado_horarios->count()>0)
					<span class="glyphicon glyphicon-calendar" aria-hidden="true" title="horario vigente"></span>
					@else
					<span class="glyphicon glyphicon-calendar disabled" aria-hidden="true" title="Sin horario"></span>
					@endif
				</td>
				<td>
					@if($reporte_diario->listado_turnos->count()>0)
					@foreach($reporte_diario->listado_turnos as $id_t=>$turno)
				 	{{ $turno->fecha_hora_entrada->format('H:i a') }}  a  {{ $turno->fecha_hora_salida->format('H:i a') }} [{{ $turno->tiempo()}}]
					@endforeach
					@else
					---
					@endif
				</td>
				<td>
					@if($reporte_diario->listado_suspenciones->count()>0)
					<span class="glyphicon glyphicon-warning-sign suspencion " aria-hidden="true" title="Suspencion en curso"></span>
					@else
					<span class="glyphicon glyphicon-warning-sign disabled " aria-hidden="true" title="Sin suspencion"></span>
					@endif
				</td>
				<td>

					@if($reporte_diario->listado_suspenciones->count()>0)
					@foreach($reporte_diario->listado_suspenciones as $id_su=>$suspencion)
				 	<div>{{ $suspencion->descripcion }}</div>
					@endforeach
					@else
					---
					@endif
				</td>
				<td>
					@if($reporte_diario->listado_asistencias->count()>0)

					<span class="glyphicon glyphicon-ok-circle asistencia " aria-hidden="true" title="Falta"></span>
					@elseif($reporte_diario->listado_turnos->count()>0 && $reporte_diario->listado_suspenciones->count()==0)
					<span class="glyphicon glyphicon-ban-circle falta " aria-hidden="true" title="Falta"></span>
					@else
					---
					@endif
				</td>
				<td>
					@if($reporte_diario->listado_asistencias->count()>0)
					@foreach ($reporte_diario->listado_asistencias as $id_a => $asistencia)
					{{ $asistencia->entrada->fecha_hora_marcacion->format("h:i a") }} - {{ $asistencia->salida->fecha_hora_marcacion->format("h:i a") }} [{{ $asistencia->tiempoAcumulado() }}]
					{!! $id_a>0?'<br>':''!!}
					@endforeach
					@else
					---
					@endif
				</td>
				<td>
					@if($reporte_diario->listado_marcaciones->count()>0)
					@foreach ($reporte_diario->listado_marcaciones as $id_m => $marcacion)

					@if(trim($marcacion->observacion)!="")
					[{{ $marcacion->tipoToString() }} - {{ $marcacion->justificacionToString() }}]
					{{ $marcacion->fecha_hora_marcacion->format("H:i:s") }}
					{{ "(".$marcacion->observacion.")" }}
					{!! $id>0?'<br>':''!!}
					@endif
					@endforeach
					@else
					---
					@endif
				</td>
			</tr>
		@endforeach
		@else
			<tr>
				<td>
					No hay marcaciones para mostrar
				</td>
			</tr>
		@endif
   	</tbody>
   	<tfoot>
   		<tr>
   			<td colspan="10">
   				
   			</td>
   		</tr>
   	</tfoot>
</table>

<!-- <table class="table table-bordered ">
	<caption>Leyenda</caption>
	<tbody>
		<tr>
			<td><span class="glyphicon glyphicon-calendar" aria-hidden="true" title="horario vigente"></span></td>
			<td>Horario asignado</td>
		
			<td><span class="glyphicon glyphicon-calendar" aria-hidden="true" title="horario vigente"></span></td>
			<td>Turno programado</td>
		
			<td><span class="glyphicon glyphicon-calendar" aria-hidden="true" title="horario vigente"></span></td>
			<td>Sancionado</td>
		
			<td><span class="glyphicon glyphicon-calendar" aria-hidden="true" title="horario vigente"></span></td>
			<td>Asistencia</td>
		
			<td><span class="glyphicon glyphicon-calendar" aria-hidden="true" title="horario vigente"></span></td>
			<td>Permiso</td>
		</tr>
	</tbody>
</table> -->