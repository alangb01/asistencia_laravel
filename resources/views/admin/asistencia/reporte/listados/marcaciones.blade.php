{{-- REPORTE -> LISTADO MARCACIONES --}}
<table class="table table-hover ">
  	<caption class="text-center">
  		<div class="col-md-4"></div>
  		<div class="col-md-4"><h4>LISTADO DE MARCACIONES</h4></div>
  		<div class="col-md-4">
			<a href="{{ $reporte->url_marcacion_justificar }}" class="btn btn-primary pull-right">Justificar marcacion</a>
  		</div>
  	</caption>
   	<thead>
   		<tr>
   			<th>Dia</th>
   			<th>Fecha</th>
   			<th>Hora</th>
   			<th>Tipo</th>
   			<th>Justificacion</th>
   			<th>Observacion</th>
   			<th class="text-center">Opciones</th>
   		</tr>
   	</thead>
   	<tbody>
   		@if($reporte->marcacionesProcesadas()!=null && count($reporte->marcacionesProcesadas())>0)
		@foreach($reporte->marcacionesProcesadas() as $id=>$marcacion)
			<tr class="{{ $marcacion->estado=='VR'?'fila_verificar':'' }}">
				<td>{{ $marcacion->diaToString() }}</td>
				<td >{{ Util::fechaToString($marcacion->fecha_hora_marcacion) }}</td>
				<td>{{ $marcacion->fecha_hora_marcacion->format('h:i:s a') }}</td>
				<td>
					@if($marcacion->tipo_marcacion==$marcacion::TIPO_ENTRADA)
					<span class="glyphicon glyphicon-triangle-top icon_green" aria-hidden="true"></span>
					@elseif($marcacion->tipo_marcacion==$marcacion::TIPO_SALIDA)
					<span class="glyphicon glyphicon-triangle-bottom icon_red" aria-hidden="true"></span>
					@else
					<span class="glyphicon glyphicon-question-sign icon_gray" aria-hidden="true"></span>
					@endif
					{{ $marcacion->tipoToString() }}
				</td>
				<td>
					@if ($marcacion->tipo_justificacion==$marcacion::JUSTIFICACION_NORMAL)
					<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
					@elseif($marcacion->tipo_justificacion==$marcacion::JUSTIFICACION_SALUD)
					<span class="glyphicon glyphicon-heart icon_gray" aria-hidden="true"></span>
					@elseif($marcacion->tipo_justificacion==$marcacion::JUSTIFICACION_TRABAJO)
					<span class="glyphicon glyphicon-lock icon_gray" aria-hidden="true"></span>
					@elseif($marcacion->tipo_justificacion==$marcacion::JUSTIFICACION_ESTUDIO)
					<span class="glyphicon glyphicon-education icon_gray" aria-hidden="true"></span>
					@elseif($marcacion->tipo_justificacion==$marcacion::JUSTIFICACION_OTRO)
					<span class="glyphicon glyphicon-search icon_gray" aria-hidden="true"></span>
					@else
					<span class="glyphicon glyphicon-question-sign icon_gray" aria-hidden="true"></span>
					@endif
					{{ $marcacion->justificacionToString() }}
				</td>
				<td>{{ $marcacion->observacionToString() }}</td>
				<td class="text-center">
					@if ($marcacion->estado==$marcacion::ESTADO_VERIFICAR)
					<form action="{{ $marcacion->url_cambiar_tipo }}" method="post" class="inline">
							{{ csrf_field() }}
						{!! method_field('PUT') !!}
						<div class="btn-group btn-group-sm">
							<button class="btn btn-warning" type="submit" title="Cambiar entrada/salida">
								<span class="glyphicon glyphicon-random" aria-hidden="true">
							</button>
						</div>
					</form>	
					<form action="{{ $marcacion->url_eliminar }}" method="post" class=" inline">
							{{ csrf_field() }}
						{!! method_field('DELETE') !!}
						<div class="btn-group btn-group-sm">
							<button class="btn btn-danger" type="submit" title="Eliminar marcacion">
								<span class="glyphicon glyphicon-trash" aria-hidden="true">
							</button>
						</div>
					</form>
					@else
					Sin opciones
					@endif
				</td>
			</tr>
		@endforeach
		@else
			<tr>
				<td>
					No hay marcaciones para mostrar
				</td>
			</tr>
		@endif
   	</tbody>
   	<tfoot>
   		<tr>
   			<td colspan="10">
   				
   			</td>
   		</tr>
   	</tfoot>
</table>