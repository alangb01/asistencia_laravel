@extends ("admin.plantilla")
@section('titulo_pagina','Listado de reportes de trabajadores')
@section('contenido_pagina')
<div class="container">
	@include('admin.comun.notificaciones')
	<div class="panel panel-default">
	  <!-- Default panel contents -->
	  <div class="panel-heading">
	  	<div class="row text-center">
	  		<div class="col-md-3"></div>
	  		<div class="col-md-6">
	  			<h4>@yield('titulo_pagina')</h4>
	  		</div>
	  		<div class="col-md-3"></div>
	  	</div>
		{{-- <a href="{{ $url_nuevo }}" class="btn btn-default">Nuevo cargo</a> --}}
	  </div>
	  <table class="table table-hover table-condensed">
	   	<thead>
	   		<tr>
	   			<th class="col-md-1">Id</th>
	   			<th>Trabajador</th>
	   			<th>Ultima marcacion</th>
	   			<th class="col-md-4 text-center">Opciones</th>
	   		</tr>
	   	</thead>
	   	<tbody>
	   		@if(isset($trabajadores) && count($trabajadores)>0)
			@foreach($trabajadores as $id=>$trabajador)
				<tr>
					<td>{{ $trabajador->id }}</td>
					<td>{{ $trabajador }}</td>
					<td>
						@if($trabajador->ultimaMarcacion()!=null)
						{{ "[".$trabajador->ultimaMarcacion()->diaToString()."] "}}
						{{ Util::fechaToString($trabajador->ultimaMarcacion()->fecha_hora_marcacion) }} - 
						{{ $trabajador->ultimaMarcacion()->fecha_hora_marcacion->format('h:i a')}}
						@else
						No registrado
						@endif
					</td>
					<td class="col-md-4 text-center">
						<?php /*  
						<form action="{{ $trabajador->url_eliminar }}" method="post" class=" inline">
   							{{ csrf_field() }}
							{!! method_field('DELETE') !!}
							<div class="btn-group btn-group-sm">
								<a href="{{ $trabajador->url_editar }}" class="btn btn-default">Editar</a>
								<button class="btn btn-default" type="submit">Eliminar</button>
							</div>
						</form>	
						*/ ?>
						<div class="btn-group btn-group-sm">
							<a href="{{ $trabajador->url_horario }}" class="btn btn-default" title="Horario asignado">
								<span class="glyphicon glyphicon-calendar" aria-hidden="true"></span> Horarios
							</a>
						</div>
						<div class="btn-group btn-group-sm">
							<a href="{{ $trabajador->url_reporte_preview }}" class="btn btn-default bg-orange" title="Vista previa de reporte">
								<span class="glyphicon glyphicon-stats" aria-hidden="true"></span>
								<span class="glyphicon glyphicon-hourglass" aria-hidden="true"></span> Reporte
							</a>
							<!-- <a href="{{ $trabajador->url_reporte_anteriores }}" class="btn btn-default  bg-green" title="Reportes anteriores">
								<span class="glyphicon glyphicon-stats" aria-hidden="true"></span>
								<span class="glyphicon glyphicon-duplicate" aria-hidden="true"></span>
							</a> -->
						</div>

						<div class="btn-group btn-group-sm">
							<a href="{{ $trabajador->url_marcacion_justificar }}" class="btn btn-primary pull-right">
								<span class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span> Registrar marcación
							</a>
						</div>

					</td>
				</tr>
			@endforeach
			@else
				<tr>
					<td colspan="4">
						No hay trabajadores para mostrar
					</td>
				</tr>
			@endif
	   	</tbody>
	   	<tfoot>
	   		<tr>
	   			<td colspan="10">
	   				{{ $trabajadores->links() }}
	   			</td>
	   		</tr>
	   	</tfoot>
	  </table>
	</div>
	
</div>
@endsection