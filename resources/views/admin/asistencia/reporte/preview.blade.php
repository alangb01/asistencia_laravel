@extends ("admin.plantilla")
@section('titulo_pagina','Reporte de trabajador')
@section('contenido_pagina')
<div class="container">
	@include('admin.comun.notificaciones')
	<div class="panel panel-default">
		  <!-- Default panel contents -->
		<div class="panel-heading ">
		  	<div class="row">
		  		<div class="col-md-3">
			  		<a href="{{ $url_volver }}" class="btn btn-primary">Volver</a>
			  	</div>
			  	<div class="col-md-6 text-center ">
			  		<h4>@yield('titulo_pagina')</h4>
			  	</div>
			  	<div class="col-md-3">
		  			<form action="{{ $reporte->url_exportar_pdf }}" target="_blank" method="post" class=" orm-group">
							{{ csrf_field() }}
							<button class="btn btn-danger pull-right" type="submit" title="">
								<!-- <span class="glyphicon glyphicon-trash" aria-hidden="true"> -->
									Generar PDF
							</button>
					</form>	
					<form action="{{ $reporte->url_exportar_excel }}" target="_blank" method="post" class=" form-group ">
							{{ csrf_field() }}
							<button class="btn btn-success pull-right" type="submit" title="">
								<!-- <span class="glyphicon glyphicon-trash" aria-hidden="true"> -->
									Generar excel
							</button>
					</form>	
			  	</div>
			  	<div class="clear"></div>
		  	</div>
		</div>
		<div class="panel-body">
		  	<div class=" col-md-12">
		  		<div class="col-md-1">
		  			<label for="">DNI</label>
		  		</div>
			  	<div class="col-md-1">{{ $reporte->trabajador->dni }}</div>
			  	<div class="col-md-1"><label for="">Trabajador</label></div>
			  	<div class="col-md-4">{{ $reporte->trabajador }}</div>
			  	<div class="col-md-1"><label for="">Cargo</label></div>
			  	<div class="col-md-4">{{ $reporte->trabajador->cargo }}</div>
		  	</div>
		  	<div class=" col-md-12">
		  		<div class="col-md-1"><label for="">Periodo</label></div>
			  	<div class="col-md-11 form-inline">
					<form action="" method="get">
						<input type="text" class="form-control text-center datepicker-periodo" name="fecha_inicio" value="{{ $reporte->fecha_inicio->format('Y-m-d') }}">
						al
						<input type="text" class="form-control text-center datepicker-periodo" name="fecha_final" value="{{ $reporte->fecha_final->format('Y-m-d') }}">
						<input type="submit" class="btn btn-primary" value="Filtrar">

						<a href="{{ $url_mes_actual }}" class="btn  {{ $mes_actual?'btn-warning':'btn-default'}}">MES ACTUAL</a>
						<a href="{{ $url_mes_anterior }}" class="btn {{ $mes_anterior?'btn-warning':'btn-default'}}">MES ANTERIOR</a>

					</form>
			  	</div>


		  	</div>
			<hr class="row col-md-12">
		</div>
		@include('admin.asistencia.reporte.listados.estadisticas')
	 	@include('admin.asistencia.reporte.listados.marcaciones')
	 	@include('admin.asistencia.reporte.listados.asistencias')
	 	{{-- @include('admin.asistencia.reporte.listados.reporte-diario') --}}
	 	@include('admin.asistencia.reporte.listados.suspenciones')
		
	 	{{-- @include('admin.asistencia.reporte.listados.permisos') --}}
	 	<?php /*
<ul class="nav nav-tabs">
		  	<li role="presentation" class="active"><a data-toggle="tab" href="#resumen">Resumen</a></li>
		  	<li role="presentation"><a data-toggle="tab" href="#asistencias">Asistencias</a></li>
		  	<li role="presentation"><a data-toggle="tab" href="#marcaciones">Marcaciones</a></li>
		  	<li role="presentation"><a data-toggle="tab" href="#permisos">Permisos</a></li>
		  	<li role="presentation"><a data-toggle="tab" href="#suspenciones">Suspenciones</a></li>
		  	<li role="presentation"><a data-toggle="tab" href="#sanciones">Sanciones</a></li>
		</ul>
	 	
	 	<div class="tab-content">
		  	<div id="resumen" class="tab-pane fade in active">
		    	@include('admin.asistencia.reporte.listados.reporte-diario')
		  	</div>
			<div id="asistencias" class="tab-pane fade">
				@include('admin.asistencia.reporte.listados.asistencias')
			</div>
			<div id="marcaciones" class="tab-pane fade">
				@include('admin.asistencia.reporte.listados.marcaciones')
			</div>
			<div id="permisos" class="tab-pane fade">
				@include('admin.asistencia.reporte.listados.permisos')
			</div>
			<div id="suspenciones" class="tab-pane fade">
				@include('admin.asistencia.reporte.listados.suspenciones')
			</div>
			<div id="sanciones" class="tab-pane fade">
				@include('admin.asistencia.reporte.listados.sanciones')
			</div>
		</div>

	 	*/ ?>
		
	</div>
</div>
@endsection