@extends ("admin.plantilla")
@section('contenido_pagina')
<div class="container-fluid">
	@include('admin.comun.notificaciones')
	<div class="panel panel-default">
		<!-- Default panel contents -->
		<div class="panel-heading">
			<div class="row">
		  		<div class="col-md-3">
			  		<a href="{{ $url_volver}}" class="btn btn-default">Volver</a>
			  	</div>
			  	<div class="col-md-6 text-center">
			  		<h4>Reporte - Registrar permiso</h4>
			  	</div>
			  	<div class="col-md-3">
			  	</div>
		  	</div>
		</div>
		<div class="panel-body">
	   		<form role="form" class="col-md-12" action="{{ $permiso->action_form }}" method="post" accept-charset="UTF-8" enctype="multipart/form-data">
	   			{{ csrf_field() }}
	   			<input type="hidden" name="id_trabajador" value="{{ $permiso->trabajador->id }}">
	   			<div class="form-group">
			    	<label for="trabajador">trabajador:</label>
			    	{{ $permiso->trabajador }}
			  	</div>
			  	<div class="form-group">
			    	<label for="tipo">tipo:</label>
			    	<div class="input-group">
						<label for="tipo_salud" class="radio-inline ">
					    <input type="radio" name="tipo" id="tipo_salud" value="{{ $permiso::TIPO_DIA_LIBRE }}" {{ $permiso->tipo==$permiso::TIPO_DIA_LIBRE?'checked':'' }} autocomplete="off"> Dia libre
					  </label>
					  <label for="tipo_estudio" class="radio-inline">
					    <input type="radio" name="tipo" id="tipo_estudio" value="{{ $permiso::TIPO_LICENCIA }}" {{ $permiso->tipo==$permiso::TIPO_DIA_LIBRE?'checked':'' }} autocomplete="off"> Descanzo
					  </label>
					  <label for="tipo_trabajo" class="radio-inline">
					    <input type="radio" name="tipo" id="tipo_trabajo" value="{{ $permiso::TIPO_PERMISO }}" {{ $permiso->tipo==$permiso::TIPO_DIA_LIBRE?'checked':'' }} autocomplete="off"> Permiso
					  </label>
					   <label for="tipo_otro" class="radio-inline">
					    <input type="radio" name="tipo" id="tipo_otro" value="{{ $permiso::TIPO_VACACIONES }}" {{ $permiso->tipo==$permiso::TIPO_DIA_LIBRE?'checked':'' }} autocomplete="off"> Vacaciones
					  </label>

					</div>
			    	@if($errors->has('tipo')!=null)
			    	<span class="help-inline">{{ $errors->first('tipo') }}</span>
			    	@endif
			  	</div>
			  	<div class="form-group">
			    	<label for="fecha_inicio">Fecha :</label>
			    	<div class="form-inline">
			    		<input type="text" class="form-control datepicker date" id="fecha" name="fecha" value="{{ $permiso->fecha_hora_inicio->format('Y-m-d') }}">
				    	@if($errors->has('fecha_inicio')!=null)
				    	<span class="help-inline">{{ $errors->first('fecha_inicio') }}</span>
				    	@endif
			    		
			    	</div>
			  	</div>
			  	<div class="form-group">
			    	<label for="hora">HORA :</label>
			    	<div class="form-inline">
			    		<input type="text" class="form-control timepicker" id="hora_inicio" name="hora_inicio" value="{{ $permiso->fecha_hora_inicio->format('H:i:s') }}">
			    		<input type="text" class="form-control timepicker" id="hora_termino" name="hora_termino" value="{{ $permiso->fecha_hora_termino->format('H:i:s') }}">
				    	@if($errors->has('hora_inicio')!=null)
				    	<span class="help-inline">{{ $errors->first('hora_inicio') }}</span>
				    	@endif
				    	@if($errors->has('hora_termino')!=null)
				    	<span class="help-inline">{{ $errors->first('hora_termino') }}</span>
				    	@endif
			    	</div>
			  	</div>
			  	<div class="form-group">
			    	<label for="fecha_termino">Tiempo transcurrido:</label>
			    	<div class="form-inline">
			    		<input type="text" id="tiempo_calculado" class="form-control" value="" readonly>
			    	</div>
			  	</div>
			  	

			  	<div class="form-group">
			    	<label for="descripcion">descripcion:</label>
			    	<textarea name="descripcion" id="descripcion" class="form-control" >{{ $permiso->descripcion }}</textarea>
			    	@if($errors->has('descripcion')!=null)
			    	<span class="help-inline">{{ $errors->first('descripcion') }}</span>
			    	@endif
			  	</div>

			  	{{-- <div class="form-group">
			    	<label for="">Cronometrado:</label>
			    	<label for="cronometrado"class="checkbox-inline">
					  <input type="checkbox" id="cronometrado" name="cronometrado" value="1"> Tiempo contabilizado como asistido
					</label>
			  	</div> --}}
			  	<!-- <div class="checkbox">
			  				    	<label><input type="checkbox"> Remember me</label>
			  	</div> -->
			  	<button type="submit" class="btn btn-success pull-right">Registrar</button>
			</form>
		</div>
	</div>
</div>

<script type="text/javascript">
    
    $('.datepicker').datetimepicker({
    	locale: 'es',
    	format: 'YYYY-MM-DD'
    }).on('dp.change', function(e) {
    	calcularDiferencia();
	});

    $('.timepicker').datetimepicker({
    	locale: 'es',
    	format: 'HH:mm:ss',
    }).on('dp.change', function(e) {
    	calcularDiferencia();
	});


function calcularDiferencia(){
	data={fecha_inicio:$("#fecha_inicio").val(),
			hora_inicio:$("#hora_inicio").val(),	
			fecha_termino:$("#fecha_inicio").val(),
			hora_termino:$("#hora_termino").val()
		};

	$.get("{{ route('util.calcular.tiempo--fecha-hora') }}",data,function(resultado){
		$("#tiempo_calculado").val(resultado);
	})
}
calcularDiferencia();
</script>
@endsection