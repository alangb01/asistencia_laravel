@extends ("admin.plantilla")
@section('contenido_pagina')
<div class="container-fluid">
	@include('admin.comun.notificaciones')
	<div class="panel panel-default">
	  <!-- Default panel contents -->
	  <div class="panel-heading">
	  	Historial de reportes generados
		{{-- <a href="{{ $url_nuevo }}" class="btn btn-default">Nuevo cargo</a> --}}
	  </div>
	  <table class="table table-hover table-condensed">
	   	<thead>
	   		<tr>
	   			<th>Id</th>
	   			<th>Trabajador</th>
	   			<th>Ultima marcacion</th>
	   			<th>Opciones</th>
	   		</tr>
	   	</thead>
	   	<tbody>
	   		@if(isset($trabajadores) && count($trabajadores)>0)
			@foreach($trabajadores as $id=>$trabajador)
				<tr>
					<td>{{ $trabajador->id }}</td>
					<td >{{ $trabajador }}</td>
					<td>{{ $trabajador->ultimaMarcacion }}</td>
					<td>
						<?php /*  
						<form action="{{ $trabajador->url_eliminar }}" method="post" class=" inline">
   							{{ csrf_field() }}
							{!! method_field('DELETE') !!}
							<div class="btn-group btn-group-sm">
								<a href="{{ $trabajador->url_editar }}" class="btn btn-default">Editar</a>
								<button class="btn btn-default" type="submit">Eliminar</button>
							</div>
						</form>	
						*/ ?>

						<div class="btn-group btn-group-sm">
							<a href="{{ $trabajador->url_reportes_anteriores }}" class="btn btn-default">Reportes anteriores</a>
							<a href="{{ $trabajador->url_reporte_previsualizacion }}" class="btn btn-default">Vista previa</a>
						</div>
					</td>
				</tr>
			@endforeach
			@else
				<tr>
					<td>
						No hay trabajadores para mostrar
					</td>
				</tr>
			@endif
	   	</tbody>
	   	<tfoot>
	   		<tr>
	   			<td colspan="10">
	   				
	   			</td>
	   		</tr>
	   	</tfoot>
	  </table>
	</div>
	
</div>
@endsection