@extends ("admin.plantilla")
@section('contenido_pagina')
<div class="container">
	@include('admin.comun.notificaciones')
	<div class="panel panel-default">

		<!-- Default panel contents -->
		<div class="panel-heading">
			<div class="row">
		  		<div class="col-md-3">
			  		<a href="{{ $url_volver}}" class="btn btn-primary">Volver</a>
			  	</div>
			  	<div class="col-md-6 text-center">
			  		<h4>Reporte - Registrar marcacion</h4>
			  	</div>
			  	<div class="col-md-3">
			  	</div>
		  	</div>
		</div>
		<div class="panel-body">
	   		<form role="form" class="col-md-12" action="{{ $marcacion->action_form }}" method="post" accept-charset="UTF-8" enctype="multipart/form-data">
	   			{{ csrf_field() }}
	   			<input type="hidden" name="id_trabajador" value="{{ old('id_trabajador')!=''?old('id_trabajador'):$marcacion->trabajador->id }}">
	   			<div class="form-group">
			    	<label for="trabajador">trabajador:</label>
			    	{{ $marcacion->trabajador }}
			  	</div>
			  	<div class="form-group">
			    	<label for="fecha">Fecha - hora:</label>
			    	<div class="form-inline">
			    		<input type="text" class="form-control datepicker date" id="fecha" name="fecha" value="{{ old('fecha')!=''?old('fecha'):$marcacion->fecha_hora_marcacion->format('Y-m-d') }}">
				    	@if($errors->has('fecha')!=null)
				    	<span class="help-inline">{{ $errors->first('fecha') }}</span>
				    	@endif
				    	<input type="text" class="form-control timepicker" id="hora" name="hora" value="{{ old('hora')!=''?old('hora'):$marcacion->fecha_hora_marcacion->format('H:i') }}">
				    	@if($errors->has('hora')!=null)
				    	<span class="help-inline">{{ $errors->first('hora') }}</span>
				    	@endif
			    	</div>
			  	</div>
			  	<div class="form-group">
			    	<label for="">Tipo:</label>
				  	<div class="input-group">
					  <label for="tipo_entrada" class="radio-inline">
					    <input type="radio" name="tipo" id="tipo_entrada" value="{{ $marcacion::TIPO_ENTRADA }}" autocomplete="off" {{ old('tipo')==$marcacion::TIPO_ENTRADA?'checked':'' }}> Entrada
					  </label>
					  <label for="tipo_salida" class="radio-inline">
					    <input type="radio" name="tipo" id="tipo_salida" value="{{ $marcacion::TIPO_SALIDA }}" autocomplete="off" {{ old('tipo')==$marcacion::TIPO_SALIDA?'checked':'' }}> Salida
					  </label>
					</div>
			    	@if($errors->has('tipo')!=null)
			    	<span class="help-inline">{{ $errors->first('tipo') }}</span>
			    	@endif
			  	</div>
			  	<div class="form-group">
			    	<label for="justificacion">Justificacion:</label>
			    	<div class="input-group">
					  <label for="justificacion_trabajo" class="radio-inline">
					    <input type="radio" name="justificacion" id="justificacion_trabajo" value="{{ $marcacion::JUSTIFICACION_TRABAJO }}" autocomplete="off" {{ old('justificacion')==$marcacion::JUSTIFICACION_TRABAJO?'checked':'' }}> Trabajo
					  </label>
					  <label for="justificacion_estudio" class="radio-inline">
					    <input type="radio" name="justificacion" id="justificacion_estudio" value="{{ $marcacion::JUSTIFICACION_ESTUDIO }}" autocomplete="off" {{ old('justificacion')==$marcacion::JUSTIFICACION_ESTUDIO?'checked':'' }}> Estudio
					  </label>
					  <label for="justificacion_salud" class="radio-inline ">
					    <input type="radio" name="justificacion" id="justificacion_salud" value="{{ $marcacion::JUSTIFICACION_SALUD }}" autocomplete="off" {{ old('justificacion')==$marcacion::JUSTIFICACION_SALUD?'checked':'' }}> Salud
					  </label>
					   <label for="justificacion_otro" class="radio-inline">
					    <input type="radio" name="justificacion" id="justificacion_otro" value="{{ $marcacion::JUSTIFICACION_OTRO }}" autocomplete="off" {{ old('justificacion')==$marcacion::JUSTIFICACION_OTRO?'checked':'' }}> Otro
					  </label>
					</div>
			    	@if($errors->has('justificacion')!=null)
			    	<span class="help-inline">{{ $errors->first('justificacion') }}</span>
			    	@endif
			  	</div>

			  	<div class="form-group">
			    	<label for="observacion">observacion:</label>
			    	<textarea name="observacion" id="observacion" class="form-control" >{{ old('observacion')!=""?old('observacion'):$marcacion->observacion }}</textarea>
			    	@if($errors->has('observacion')!=null)
			    	<span class="help-inline">{{ $errors->first('observacion') }}</span>
			    	@endif
			  	</div>
			  	<!-- <div class="checkbox">
			  				    	<label><input type="checkbox"> Remember me</label>
			  	</div> -->
			  	<button type="submit" class="btn btn-success pull-right">Registrar</button>
			</form>
		</div>
	</div>
</div>
<script type="text/javascript">
    function previsualizar(input_file_id,img_preview_id) {
        var oFReader = new FileReader();
        oFReader.readAsDataURL(document.getElementById(input_file_id).files[0]);

        oFReader.onload = function (oFREvent) {
        	if (!$("#"+img_preview_id).length) {
        		$("#"+input_file_id).parent().append($("<img id=\""+img_preview_id+"\" class=\"preview_foto\">"));
        	}

        	$("#"+img_preview_id).attr('src',oFREvent.target.result);	
        };
    };

    $(document).on('change','#ruta_foto',function(){
    	previsualizar("ruta_foto","preview_foto");
    })

    
</script>
@endsection