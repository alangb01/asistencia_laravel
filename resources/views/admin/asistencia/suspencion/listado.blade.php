@extends ("admin.plantilla")
@section('titulo_pagina','Listado de suspenciones de labores')
@section('contenido_pagina')
@include('admin.comun.notificaciones')
<div class="container">
	<div class="panel panel-default">
	  <!-- Default panel contents -->
	  <div class="panel-heading  ">
	  	<div class="row text-center">
	  		<div class="col-md-3"></div>
		  	<div class="col-md-6"><h4>@yield('titulo_pagina')</h4></div>
			<div class="col-md-3">
				<a href="{{ $url_nuevo }}" class="btn btn-primary pull-right">Nueva suspencion</a>
			</div>
	  	</div>
	  </div>
	  <table class="table table-hover table-condensed">
	   	<thead>
	   		<tr>
	   			<th class="col-md-1">Id</th>
	   			<th>suspencion</th>
	   			<th>inicio</th>
	   			<th>termino</th>
	   			<th>tiempo</th>
	   			<th class="col-md-2 text-center">Opciones</th>
	   		</tr>
	   	</thead>
	   	<tbody>
	   		@if(isset($listado_suspenciones) && count($listado_suspenciones)>0)
			@foreach($listado_suspenciones as $id=>$suspencion)
				<tr>
					<td class="col-md-1">{{ $suspencion->id }}</td>
					<td>{{ $suspencion->descripcion }}</td>
					<td>{{ $suspencion->fecha_hora_inicio->format('Y-M-d') }}</td>
					<td>{{ $suspencion->fecha_hora_termino->format('Y-M-d') }}</td>
					<td>{{ $suspencion->tiempo() }}</td>
					<td class="col-md-2 text-center">
						<form action="{{ $suspencion->url_eliminar }}" method="post" class=" inline">
   							{{ csrf_field() }}
							{!! method_field('DELETE') !!}
							<div class="btn-group btn-group-sm">
								<a href="{{ $suspencion->url_editar }}" class="btn btn-default btn-editar">
									<span class="glyphicon glyphicon-pencil" aria-hidden="true" title="Editar">
								</a>
								<button class="btn btn-default btn-eliminar" type="submit">
									<span class="glyphicon glyphicon-remove" aria-hidden="true" title="Editar">
								</button>
							</div>
						</form>
					</td>
				</tr>
			@endforeach
			@else
				<tr>
					<td colspan="6">
						No hay suspenciones registradas
					</td>
				</tr>
			@endif
	   	</tbody>
	   	<tfoot>
	   		<tr>
	   			<td colspan="10">
	   				{{ $listado_suspenciones->links() }}
	   			</td>
	   		</tr>
	   	</tfoot>
	  </table>
	</div>
	
</div>
@endsection