@extends ("admin.plantilla")
@section('titulo_pagina',$suspencion->id>0?'Editar suspención de labores':'Nueva suspención de labores')
@section('contenido_pagina')
<div class="container">
	<div class="panel panel-default">
		<!-- Default panel contents -->
		<div class="panel-heading">
			<div class="row text-center">
				<div class="col-md-3">
					<a href="{{ $url_volver}}" class="btn btn-primary pull-left">Volver</a>
				</div>
				<div class="col-md-6">
					@if ($suspencion->id)
					<h4>Nueva suspencion de labores</h4>
					@else
					<h4>Editar suspencion de labores</h4>
		   			@endif
				</div>
				<div class="col-md-3"></div>
			</div>
			
			
			
		</div>
		<div class="panel-body">
	   		<form role="form"  class="col-md-12" action="{{ $suspencion->action_form }}" method="post" accept-charset="UTF-8" enctype="multipart/form-data">
	   			{{ csrf_field() }}
	   			@if ($suspencion->id)
				{!!  method_field('PUT') !!}
	   			@endif
			  	<div class="form-group">
			    	<label for="fecha_inicio">Fecha hora inicio:</label>
			    	<div class="form-group">
			    		<input type="text" class="form-control datepicker date" id="fecha_inicio" name="fecha_inicio" value="{{ $suspencion->fecha_hora_inicio->format('Y-m-d') }}">
				    	@if($errors->has('fecha_inicio')!=null)
				    	<span class="help-inline">{{ $errors->first('fecha_inicio') }}</span>
				    	@endif
				    	
			    	</div>
			  	</div>

			  	<div class="form-group">
			    	<label for="fecha_termino">Fecha hora termino:</label>
			    	<div class="form-group">
			    		<input type="text" class="form-control datepicker date" id="fecha_termino" name="fecha_termino" value="{{ $suspencion->fecha_hora_termino->format('Y-m-d') }}">
				    	@if($errors->has('fecha_termino')!=null)
				    	<span class="help-inline">{{ $errors->first('fecha_termino') }}</span>
				    	@endif
				    	
			    	</div>
			  	</div>
			  	<div class="form-group">
			    	<label for="fecha_termino">Tiempo transcurrido:</label>
			    	<div class="form-group">
			    		<input type="text" id="tiempo_calculado" class="form-control" value="{{ $suspencion->tiempo()}}" readonly>
			    	</div>
			  	</div>
			  	<div class="form-group">
			    	<label for="descripcion">Descripción:</label>
			    	<textarea name="descripcion" id="descripcion" class="form-control" placeholder="ingrese la descripcion de la suspención" >{{ $suspencion->descripcion }}</textarea>
			    	@if($errors->has('descripcion')!=null)
			    	<span class="help-inline">{{ $errors->first('descripcion') }}</span>
			    	@endif
			  	</div>
			  	<button type="submit" class="btn btn-success pull-right">Guardar</button>
			</form>
		</div>
	</div>
</div>
<script>
	$('.datepicker').datetimepicker({
    	locale: 'es',
    	format: 'YYYY-MM-DD'
    }).on('dp.change', function(e) {
    	calcularDiferencia();
	});

    $('.timepicker').datetimepicker({
    	format: 'HH:mm:ss',
    }).on('dp.change', function(e) {
    	calcularDiferencia();
	});

    function calcularDiferencia(){
		data={fecha_inicio:$("#fecha_inicio").val(),
			// hora_inicio:$("#hora_inicio").val(),	
			fecha_termino:$("#fecha_termino").val(),
			// hora_termino:$("#hora_termino").val()
		};

		$.get("{{ route('util.calcular.tiempo--fecha') }}",data,function(resultado){
			$("#tiempo_calculado").val(resultado);
		})
	}
</script>
@endsection