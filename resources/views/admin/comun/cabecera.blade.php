<nav class="navbar navbar-default" role="navigation">
  <!-- El logotipo y el icono que despliega el menú se agrupan
       para mostrarlos mejor en los dispositivos móviles -->
  <div class="container">
    <div class="navbar-header">
    <button type="button" class="navbar-toggle" data-toggle="collapse"
            data-target=".navbar-ex1-collapse">
      <span class="sr-only">Desplegar navegación</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>
    <a class="navbar-brand" href="#">Sistema de asistencia</a>
  </div>
 
  <!-- Agrupar los enlaces de navegación, los formularios y cualquier
       otro elemento que se pueda ocultar al minimizar la barra -->
  @if(Auth::user()!=null)
  <div class="collapse navbar-collapse navbar-ex1-collapse">
    <ul class="nav navbar-nav">
      @can('trabajador.index',App\Admin\Asistencia\Trabajador::class)
      <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
          Personal <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
          <li><a href="{{ route('trabajador.listado') }}">Trabajadores</a></li>
          <li><a href="{{ route('cargo.listado') }}">Cargos</a></li>
          <li><a href="{{ route('sucursal.listado') }}">Sucursales</a></li>
          <li class="divider"></li>
          <li><a href="{{ route('usuario.listado') }}">Usuarios</a></li>
          
          {{-- <li class="divider"></li> --}}
        </ul>
      </li>
      @endcan
      <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
          Asistencia <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
          <li><a href="{{ route('reporte.trabajadores') }}">Reporte</a></li>
          <li class="divider"></li>
          <li><a href="{{ route('suspencion.listado') }}">Suspencion</a></li>
          {{-- <li><a href="#">Justificar marcacion</a></li> --}}
          {{-- <li><a href="#">Justificar permiso</a></li> --}}
          {{-- <li class="divider"></li> --}}
        </ul>
      </li>
      <?php /* ?>
      {{-- <li><a href="{{ route('admin.sucursales.index') }}">Sucursales</a></li> --}}
      {{-- <li><a href="{{ route('admin.proyectos.index') }}">Proyectos</a></li> --}}
      {{-- <li><a href="{{ route('admin.reportes.index') }}">Reportes</a></li> --}}
    
      <?php */ ?>
    </ul>
 
    <!-- <form class="navbar-form navbar-left" role="search">
      <div class="form-group">
        <input type="text" class="form-control" placeholder="Buscar">
      </div>
      <button type="submit" class="btn btn-default">Enviar</button>
    </form>
      -->
    <ul class="nav navbar-nav navbar-right">
     
      <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
          Hola, {{ Usuario::logueado() }} <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
          <li><a href="{{ route('usuario.editar-perfil') }}">Editar perfil</a></li>
          <li><a href="{{ route('usuario.cambiar-clave') }}">Cambiar clave</a></li>
          <li class="divider"></li>
          <li><a href="{{ route('logout') }}">Cerrar sesión</a></li>
        </ul>
      </li>
    </ul>
  </div>
  @else
  
  @endif
  </div>
</nav>
