<ul class="pagination">
  <li class="disabled">
    <a href="#">&laquo;</a>
  </li>
  <li class="active">
    <a href="#">1 <span class="sr-only">(página actual)</span></a>
  </li>
  ...
</ul>
<!-- 
<ul class="pager">
  <li><a href="#">Anterior</a></li>
  <li><a href="#">Siguiente</a></li>
</ul>

<ul class="pager">
  <li class="previous disabled"><a href="#">&larr; Anterior</a></li>
  <li class="next"><a href="#">Siguiente &rarr;</a></li>
</ul> -->