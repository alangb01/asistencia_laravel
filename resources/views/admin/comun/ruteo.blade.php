<?php if (isset($ruteo) && count($ruteo)>0): ?>
<ol class="breadcrumb">
	<?php 
	$posicion=0;
	foreach ($ruteo as $url => $ruta): $posicion++?>
	<li><a href="{{ $url }}">{{ $ruta }}</a></li>		
	<?php if ($posicion<count($ruteo)): ?>
	<li class="active">{{ $ruta }}</li>		
	<?php endif ?>
	<?php endforeach ?>
</ol>
<?php endif ?>