@if (session('notificacion'))
<div class="alert alert-{{ session('notificacion')['tipo'] }}">
  	@if(isset(session('notificacion')['url']))
  		<a href="{{ session('notificacion')['url'] }}" class="alert-link">{{ session('notificacion')['mensaje']}}</a>
  	@else
		<span class="">{{ session('notificacion')['mensaje']}}</span>
  	@endif
</div>
@endif
<!-- <div class="alert alert-success">
  <a href="#" class="alert-link">...</a>
</div>
 
<div class="alert alert-info">
  <a href="#" class="alert-link">...</a>
</div>
 
<div class="alert alert-warning">
  <a href="#" class="alert-link">...</a>
</div>
 
<div class="alert alert-danger">
  <a href="#" class="alert-link">...</a>
</div> -->

<script>
	$(".alert").fadeTo(500, 500).delay(5000).slideUp(500);
</script>