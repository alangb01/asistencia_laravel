@push('scripts')
	<script src="{{ asset('addon/jquery-1.12.4.min.js') }}"></script>
    <script src="{{ asset('addon/bootstrap-3.3.6-dist/js/bootstrap.js') }}"></script>
    <script src="{{ asset('addon/moment/moment.min.js') }}"></script>
    <script src="{{ asset('addon/moment/locales.min.js') }}"></script>
    <script src="{{ asset('addon/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js') }}"></script>
    <script src="{{ asset('js/funciones.js')}}"></script>
@endpush
@push('links')
     <link rel="stylesheet" href="{{ asset('addon/bootstrap-3.3.6-dist/css/bootstrap.css')}}">
     <link rel="stylesheet" href="{{ asset('addon/bootstrap-datetimepicker/css/bootstrap-datetimepicker.css')}}">

     <link rel="stylesheet" href="{{ asset('css/estilo.css')}}">
@endpush
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>@yield('titulo_pagina','sin titulo')</title>

	@stack('scripts')
	@stack('links')
</head>
<body>
	<header>
		@include('admin.comun.cabecera')
	</header>
	{{-- <div class="container"> --}}
		@include('admin.comun.ruteo')
		@yield('contenido_pagina')
	{{-- </div> --}}
	<footer>
		@include('admin.comun.pie')
	</footer>
</body>
</html>