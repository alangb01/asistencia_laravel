@extends ("admin.plantilla")
@section('titulo_pagina','Listado de usuarios')
@section('contenido_pagina')
@include('admin.comun.notificaciones')
<div class="container">
	<div class="panel panel-default">
	  <!-- Default panel contents -->
	  	<div class="panel-heading">
			<div class="row text-center">
				<div class="col-md-3"></div>
				<div class="col-md-6"><h4>@yield('titulo_pagina')</h4></div>
				<div class="col-md-3"><a href="{{ $url_nuevo }}" class="btn btn-success pull-right">Nuevo usuario</a></div>
			</div>
	  	
		
	  	</div>
	 <!--  <div class="panel-body">
	   <p>...</p>
	 </div>
	  -->
	  <!-- Table -->
	  <table class="table">
	   	<thead>
	   		<tr>
	   			<th>Id</th>
	   			<th>Trabajador</th>
	   			<th>Alias</th>
	   			<th>Correo</th>
	   			<th class="text-center">Opciones</th>
	   		</tr>
	   	</thead>
	   	<tbody>
	   		@if(isset($listado_usuarios) && count($listado_usuarios)>0)
			@foreach($listado_usuarios as $id=>$usuario)
				<tr>
					<td>{{ $usuario->id }}</td>
					<td>{{ $usuario->trabajador?$usuario->trabajador:"no asociado" }}</td>
					<td >{{ $usuario->name!=""?$usuario->name:'---' }}</td>
					<td >{{ $usuario->email }}</td>
					<td class="text-center">
						<div class="btn-group btn-group-sm">
							<a href="{{ $usuario->url_forzar_cambio_clave }}" class="btn btn-default">
								<span class="glyphicon glyphicon-refresh" aria-hidden="true" title="Forzar cambio clave">
							</a>

							<a href="{{ $usuario->url_resetear_clave }}" class="btn btn-default">
								<span class="glyphicon glyphicon-wrench" aria-hidden="true" title="Resetear clave">
							</a>
						</div>
						
						<form action="{{ $usuario->url_eliminar }}" method="post" class=" inline">
   							{{ csrf_field() }}
							{!! method_field('DELETE') !!}

							
							<div class="btn-group btn-group-sm">
								<a href="{{ $usuario->url_editar }}" class="btn btn-default btn-editar">
									<span class="glyphicon glyphicon-pencil" aria-hidden="true" title="Editar">
								</a>
								<button class="btn btn-default btn-eliminar" type="submit">
									<span class="glyphicon glyphicon-remove" aria-hidden="true" title="Eliminar">
								</button>
							</div>
						</form>	


					</td>
				</tr>
			@endforeach
			@else
				<tr>
					<td>
						No hay usuarioes registrados
					</td>
				</tr>
			@endif
	   	</tbody>
	   	<tfoot>
	   		<tr>
	   			<td colspan="10">
	   				{{ $listado_usuarios->links() }}
	   			</td>
	   		</tr>
	   	</tfoot>
	  </table>
	</div>
	
</div>
@endsection