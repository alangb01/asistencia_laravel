@extends ("admin.plantilla")
@section('titulo_pagina',$usuario->id>0?'Editar usuario':'Nuevo usuario')
@section('contenido_pagina')
@include('admin.comun.validacion')
<div class="container">
	<div class="panel panel-default">
		<!-- Default panel contents -->
		<div class="panel-heading">
			<div class="row text-center">
				<div class="col-md-3"><a href="{{ $url_volver}}" class="btn btn-primary pull-left">Volver</a></div>
				<div class="col-md-6">
					<h4>@yield('titulo_pagina')</h4>
				</div>
				<div class="col-md-3"></div>
			</div>
		</div>
		<div class="panel-body">
	   		<form role="form" class="col-md-12" action="{{ $usuario->action_form }}" method="post" accept-charset="UTF-8" enctype="multipart/form-data">
	   			{{ csrf_field() }}
	   			@if ($usuario->id)
				{!!  method_field('PUT') !!}
	   			@endif
			  	<div class="form-group">
			    	<label for="name">Alias:</label>
			    	<input type="text" class="form-control" id="name" name="name" value="{{ old('name',$usuario->name) }}">
			    	@if($errors->has('name')!=null)
			    	<span class="help-inline">{{ $errors->first('name') }}</span>
			    	@endif
			  	</div>
			  	<div class="form-group">
			    	<label for="apellido">Trabajador:</label>
			    	<select class="form-control" id="id_trabajador" name="id_trabajador">
			    		@if(isset($trabajadores_activos) && count($trabajadores_activos)>0)
						<option value="">Seleccione un trabajador</option>
			    		<?php foreach ($trabajadores_activos as $id => $trabajador): ?>
			    		<option value="{{ $trabajador->id }}" {{ $trabajador->id==old('id_trabajador',$usuario->trabajador!=null?$usuario->trabajador->id:0)?'selected':'' }}>{{ $trabajador }}</option>
			    		<?php endforeach ?>
			    		@else
			    		<option value="">No hay trabajadores disponibles</option>
						@endif
					</select>
					@if($errors->has('id_trabajador')!=null)
			    	<span class="help-inline">{{ $errors->first('id_trabajador') }}</span>
			    	@endif
			  	</div>
			  	<div class="form-group">
			    	<label for="email">Correo personal:</label>
			    	<input type="text" class="form-control" id="email" name="email"  value="{{ old('email',$usuario->email) }}" autocomplete="off">
			    	@if($errors->has('email')!=null)
			    	<span class="help-inline">{{ $errors->first('email') }}</span>
			    	@endif
			  	</div>

			  <!-- 	<div class="form-group">
			  			    	<label for="password">Clave:</label>
			  			    	<input type="password" class="form-control" id="password" name="password"  value="">
			  			    	@if($errors->has('password')!=null)
			  			    	<span class="help-inline">{{ $errors->first('password') }}</span>
			  			    	@endif
			  </div>
			  
			  <div class="form-group">
			  			    	<label for="password_confirmation">Confirmar clave:</label>
			  			    	<input type="password" class="form-control" id="password_confirmation" name="password_confirmation"  value="">
			  			    	@if($errors->has('password_confirmation')!=null)
			  			    	<span class="help-inline">{{ $errors->first('password_confirmation') }}</span>
			  			    	@endif
			  </div> -->
			  	
			  	<!-- <div class="checkbox">
			  				    	<label><input type="checkbox"> Remember me</label>
			  	</div> -->
			  	<div class="">
			  		<button type="submit" class="btn btn-success pull-right">Registrar</button>
			  	</div>
			</form>
		</div>
	</div>
</div>
<script type="text/javascript">
    function previsualizar(input_file_id,img_preview_id) {
        var oFReader = new FileReader();
        oFReader.readAsDataURL(document.getElementById(input_file_id).files[0]);

        oFReader.onload = function (oFREvent) {
        	if (!$("#"+img_preview_id).length) {
        		$("#"+input_file_id).parent().append($("<img id=\""+img_preview_id+"\" class=\"preview_foto\">"));
        	}

        	$("#"+img_preview_id).attr('src',oFREvent.target.result);	
        };
    };

    $(document).on('change','#ruta_foto',function(){
    	previsualizar("ruta_foto","preview_foto");
    })
</script>

<style>
	.preview_foto{
		margin:auto;
		width:100%;height:100%;
		max-height: 200px;
		max-width: 200px;
	}
</style>
@endsection