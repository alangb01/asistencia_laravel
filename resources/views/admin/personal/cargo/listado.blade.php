@extends ("admin.plantilla")
@section('titulo_pagina','Listado de cargos')
@section('contenido_pagina')
@include('admin.comun.notificaciones')
<div class="container">
	<div class="panel panel-default">
	  <!-- Default panel contents -->
	  <div class="panel-heading">
	  	<div class="row text-center">
	  		<div class="col-md-3"></div>
		  	<div class="col-md-6"><h4>@yield('titulo_pagina')</h4></div>
			<div class="col-md-3">
				<a href="{{ $url_nuevo }}" class="btn btn-success pull-right">Nuevo cargo</a>
			</div>
	  	</div>
	  </div>
	  <table class="table table-default table-condensed table-hover ">
	   	<thead>
	   		<tr>
	   			<th class="col-md-1">Id</th>
	   			<th>Cargo</th>
	   			<th class="col-md-2 text-center">Opciones</th>
	   		</tr>
	   	</thead>
	   	<tbody>
	   		@if(isset($listado_cargos) && count($listado_cargos)>0)
			@foreach($listado_cargos as $id=>$cargo)
				<tr class="">
					<td class="col-md-1">{{ $cargo->id }}</td>
					<td >{{ $cargo->nombre }}</td>
					<td class="col-md-2 text-center">
						<form action="{{ $cargo->url_eliminar }}" method="post" class=" inline">
   							{{ csrf_field() }}
							{!! method_field('DELETE') !!}
							<div class="btn-group btn-group-sm">
								<a href="{{ $cargo->url_editar }}" class="btn btn-default btn-editar">
									<span class="glyphicon glyphicon-pencil" aria-hidden="true" title="Editar">
								</a>
								<button class="btn btn-default btn-eliminar" type="submit">
									<span class="glyphicon glyphicon-remove" aria-hidden="true" title="Eliminar">
								</button>
							</div>
						</form>	


					</td>
				</tr>
			@endforeach
			@else
				<tr>
					<td colspan="3">
						No hay cargos registrados
					</td>
				</tr>
			@endif
	   	</tbody>
	   	<tfoot>
	   		<tr>
	   			<td colspan="10">
	   				{{ $listado_cargos->links() }}
	   			</td>
	   		</tr>
	   	</tfoot>
	  </table>
	</div>
	
</div>
@endsection