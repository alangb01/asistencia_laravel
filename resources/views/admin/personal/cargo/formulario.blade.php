@extends ("admin.plantilla")
@section('titulo_pagina',$cargo->id?'Editar cargo':'Nuevo cargo')
@section('contenido_pagina')
<div class="container">
	<div class="panel panel-default">
		<!-- Default panel contents -->
		<div class="panel-heading">
			<div class="row text-center">
				<div class="col-md-3"><a href="{{ $url_volver}}" class="btn btn-primary pull-left">Volver</a></div>
				<div class="col-md-6">
					<h4>@yield('titulo_pagina')</h4>
				</div>
				<div class="col-md-3"></div>
			</div>
			
			
			
		</div>
		<div class="panel-body">
	   		<form role="form" action="{{ $cargo->action_form }}" method="post" accept-charset="UTF-8" enctype="multipart/form-data">
	   			{{ csrf_field() }}
	   			@if ($cargo->id)
				{!!  method_field('PUT') !!}
	   			@endif
	   			
			  	<div class="form-group ">
			    	<label for="nombre">Nombre:</label>
			    	<input type="text" class="form-control" id="nombre" name="nombre" value="{{ old('nombre',$cargo->nombre) }}" placeholder="ingrese el nombre del cargo">
			    	
			    	@if($errors->has('nombre')!=null)
			    	<span class="help-inline">{{ $errors->first('nombre') }}</span>
			    	@endif
			  	</div>
			  	<div class="form-group">
				    	<label for="estado">Estado:</label>
				    	<select class="form-control" id="estado" name="estado">
				    		@if(isset($estados_disponibles) && count($estados_disponibles)>0)
							<option value="">Seleccione un estado</option>
				    		<?php foreach ($estados_disponibles as $id => $estado): ?>
				    		<option value="{{ $estado->id }}" {{ old('estado',$cargo->estado)==$estado->id?'selected':'' }}>{{ $estado->nombre }}</option>
				    		<?php endforeach ?>
				    		@else
				    		<option value="">No hay estados</option>
							@endif
						</select>
						@if($errors->has('estado')!=null)
				    	<span class="help-inline">{{ $errors->first('estado') }}</span>
				    	@endif
				  	</div>
			  	<button type="submit" class="btn btn-success pull-right">Guardar</button>
			</form>
		</div>
	</div>
</div>
@endsection