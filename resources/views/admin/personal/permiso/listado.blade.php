@extends ("admin.plantilla")

@section('contenido_pagina')
@include('admin.comun.notificaciones')
<div class="container-fluid">
	<div class="panel panel-default">
	  <!-- Default panel contents -->
	  <div class="panel-heading">
	  	<a href="{{ $url_volver }}" class="btn btn-default">Volver </a>
	  	Listado de permisos
		<a href="{{ $url_nuevo }}" class="btn btn-default">Nuevo permiso</a>
	  </div>
	  <div class="panel-body">
	  	<div>USUARIO: {{ $usuario->name }}</div>
	  	<div>Trabajador: {{ $usuario->trabajador?$usuario->trabajador:'no asignado' }}</div>
	  </div>
	  <table class="table">
	   	<thead>
	   		<tr>
	   			<th>Id</th>
	   			<th>permiso</th>
	   			<th>Opciones</th>
	   		</tr>
	   	</thead>
	   	<tbody>
	   		@if(isset($usuario->permisos) && count($usuario->permisos)>0)
			@foreach($usuario->permisos as $id=>$permiso)
				<tr>
					<td>{{ $permiso->id }}</td>
					<td >{{ $permiso->nombre }}</td>
					<td>
						<form action="{{ $permiso->url_eliminar }}" method="post" class=" inline">
   							{{ csrf_field() }}
							{!! method_field('DELETE') !!}
							<div class="btn-group btn-group-sm">
								{{-- <a href="{{ $permiso->url_editar }}" class="btn btn-default">Editar</a> --}}
								<button class="btn btn-default" type="submit">Eliminar</button>
							</div>
						</form>	


					</td>
				</tr>
			@endforeach
			@else
				<tr>
					<td>
						No hay trabajadores registrados
					</td>
				</tr>
			@endif
	   	</tbody>
	   	<tfoot>
	   		<tr>
	   			<td colspan="10">
	   			</td>
	   		</tr>
	   	</tfoot>
	  </table>
	</div>
	
</div>
@endsection