@extends ("admin.plantilla")
@section('titulo_pagina','Listado de sucursales')
@section('contenido_pagina')
@include('admin.comun.notificaciones')
<div class="container">
	<div class="panel panel-default">
	  <!-- Default panel contents -->
	  <div class="panel-heading">
	  	<div class="row text-center">
	  		<div class="col-md-3"></div>
		  	<div class="col-md-6"><h4>	Listado de sucursales</h4></div>
			<div class="col-md-3">
				<a href="{{ $url_nuevo }}" class="btn btn-success pull-right">Nueva sucursal</a>
			</div>
	  	</div>
	  </div>
	  <table class="table">
	   	<thead>
	   		<tr>
	   			<th class="col-md-1">Id</th>
	   			<th>Cargo</th>
	   			<th>Dirección</th>
	   			<th>Teléfono</th>
	   			<th class="col-md-2 text-center">Opciones</th>
	   		</tr>
	   	</thead>
	   	<tbody>
	   		@if(isset($listado_sucursales) && count($listado_sucursales)>0)
			@foreach($listado_sucursales as $id=>$sucursal)
				<tr>
					<td class="col-md-1">{{ $sucursal->id }}</td>
					<td>{{ $sucursal->nombre }}</td>
					<td>{{ $sucursal->direccion }}</td>
					<td>{{ $sucursal->telefono }}</td>
					<td class="col-md-2 text-center">
						<form action="{{ $sucursal->url_eliminar }}" method="post" class=" inline">
   							{{ csrf_field() }}
							{!! method_field('DELETE') !!}
							<div class="btn-group btn-group-sm">
								<a href="{{ $sucursal->url_editar }}" class="btn btn-default btn-editar">
									<span class="glyphicon glyphicon-pencil" aria-hidden="true" title="Editar">
								</a>
								<button class="btn btn-default btn-eliminar" type="submit">
									<span class="glyphicon glyphicon-remove " aria-hidden="true" title="Eliminar">
								</button>
							</div>
						</form>	


					</td>
				</tr>
			@endforeach
			@else
				<tr>
					<td colspan="5">
						No hay sucursal registradas
					</td>
				</tr>
			@endif
	   	</tbody>
	   	<tfoot>
	   		<tr>
	   			<td colspan="5">
	   				{{ $listado_sucursales->links() }}
	   			</td>
	   		</tr>
	   	</tfoot>
	  </table>
	</div>
	
</div>
@endsection