@extends ("admin.plantilla")
@section('titulo_pagina',$sucursal->id>0?'Editar sucursal':'Nuevo sucursal')
@section('contenido_pagina')
<div class="container">
	<div class="panel panel-default">
		<!-- Default panel contents -->
		<div class="panel-heading">
			<div class="row text-center">
				<div class="col-md-3"><a href="{{ $url_volver}}" class="btn btn-primary pull-left">Volver</a></div>
				<div class="col-md-6">
					<h4>@yield('titulo_pagina')</h4>
				</div>
				<div class="col-md-3"></div>
			</div>
			
			
			
		</div>
		<div class="panel-body">
	   		<form role="form" action="{{ $sucursal->action_form }}" method="post" accept-charset="UTF-8" enctype="multipart/form-data">
	   			{{ csrf_field() }}
	   			@if ($sucursal->id)
				{!!  method_field('PUT') !!}
	   			@endif
	   			
			  	<div class="form-group">
			    	<label for="nombre">Nombre:</label>
			    	<input type="text" class="form-control" id="nombre" name="nombre" value="{{ old('nombre',$sucursal->nombre) }}" placeholder="ingrese el nombre de la sucursal">
			    	@if($errors->has('nombre')!=null)
			    	<span class="help-inline">{{ $errors->first('nombre') }}</span>
			    	@endif
			  	</div>
			  	<div class="form-group">
			    	<label for="direccion">Dirección:</label>
			    	<input type="text" class="form-control" id="direccion" name="direccion" value="{{ old('direccion',$sucursal->direccion) }}" placeholder="ingrese la direccion de la sucursal">
			    	@if($errors->has('direccion')!=null)
			    	<span class="help-inline">{{ $errors->first('direccion') }}</span>
			    	@endif
			  	</div>
			  	<div class="form-group">
			    	<label for="telefono">Teléfono:</label>
			    	<input type="text" class="form-control" id="telefono" name="telefono" value="{{ old('telefono',$sucursal->telefono) }}" placeholder="ingrese el telefono de la sucursal">
			    	@if($errors->has('telefono')!=null)
			    	<span class="help-inline">{{ $errors->first('telefono') }}</span>
			    	@endif
			  	</div>
			  	<div class="form-group">
				    	<label for="estado">Estado:</label>
				    	<select class="form-control" id="estado" name="estado">
				    		@if(isset($estados_disponibles) && count($estados_disponibles)>0)
							<option value="">Seleccione un estado</option>
				    		<?php foreach ($estados_disponibles as $id => $estado): ?>
				    		<option value="{{ $estado->id }}" {{ old('estado',$sucursal->estado)==$estado->id?'selected':'' }}>{{ $estado->nombre }}</option>
				    		<?php endforeach ?>
				    		@else
				    		<option value="">No hay estados</option>
							@endif
						</select>
						@if($errors->has('estado')!=null)
				    	<span class="help-inline">{{ $errors->first('estado') }}</span>
				    	@endif
				  	</div>
			  	<button type="submit" class="btn btn-success pull-right">Guardar</button>
			</form>
		</div>
	</div>
</div>
@endsection