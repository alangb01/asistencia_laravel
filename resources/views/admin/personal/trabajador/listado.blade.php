@extends ("admin.plantilla")
@section('titulo_pagina','Listado de trabajadores')
@section('contenido_pagina')
@include('admin.comun.notificaciones')
<div class="container">
	<div class="panel panel-default">
	  <!-- Default panel contents -->
	  <div class="panel-heading">
		<div class="row text-center">
			<div class="col-md-3"></div>
			<div class="col-md-6">
				<h4>Listado de trabajadores</h4>
			</div>
			<div class="col-md-3">
				<a href="{{ $url_nuevo }}" class="btn btn-success pull-right">Nuevo trabajador</a>
			</div>
		</div>
	  	
		
	  </div>
	 <!--  <div class="panel-body">
	   <p>...</p>
	 </div>
	  -->
	  <!-- Table -->
	  <table class="table">
	   	<thead>
	   		<tr>
	   			<th>Id</th>
	   			<th>Cargo</th>
	   			<th>Apellidos</th>
	   			<th>Nombres</th>
	   			<th>Fecha nacimiento</th>
	   			<th>Opciones</th>
	   		</tr>
	   	</thead>
	   	<tbody>
	   		@if(isset($listado_trabajadores) && count($listado_trabajadores)>0)
			@foreach($listado_trabajadores as $id=>$trabajador)
				<tr class="{{ $trabajador->estado==$trabajador::ESTADO_ACTIVO?'':'inactivo' }}">
					<td>{{ $trabajador->id }}</td>
					<td>{{ $trabajador->cargo?$trabajador->cargo:"no especificado" }}</td>
					<td>{{ $trabajador->apellido }}</td>
					<td >{{ $trabajador->nombre }}</td>
					<td>
						@if($trabajador->fecha_nacimiento->timestamp>0)
						{{ $trabajador->fecha_nacimiento->format('d-M-Y') }} 
						({{ $trabajador->edad_actual." años" }})
						@else
							No especificado
						@endif
						</td>
					<td>
						<form action="{{ $trabajador->url_eliminar }}" method="post" class=" inline">
   							{{ csrf_field() }}
							{!! method_field('DELETE') !!}
							<div class="btn-group btn-group-sm">
								<a href="{{ $trabajador->url_editar }}" class="btn btn-default btn-editar">
									<span class="glyphicon glyphicon-pencil" aria-hidden="true" title="Editar">
								</a>
								<button class="btn btn-default btn-eliminar" type="submit">
									<span class="glyphicon glyphicon-remove" aria-hidden="true" title="Eliminar">
								</button>
							</div>
						</form>	


					</td>
				</tr>
			@endforeach
			@else
				<tr>
					<td>
						No hay trabajadores registrados
					</td>
				</tr>
			@endif
	   	</tbody>
	   	<tfoot>
	   		<tr>
	   			<td colspan="10">
	   				{{ $listado_trabajadores->links() }}
	   			</td>
	   		</tr>
	   	</tfoot>
	  </table>
	</div>
	
</div>
@endsection