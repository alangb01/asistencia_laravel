@extends ("admin.plantilla")
@section('titulo_pagina',$trabajador->id>0?'Editar trabajador':'Nuevo trabajador')
@section('contenido_pagina')
@include('admin.comun.validacion')
<div class="container">
	<div class="panel panel-default">
		<!-- Default panel contents -->
		<div class="panel-heading">
			<div class="row text-center">
				<div class="col-md-3"><a href="{{ $url_volver}}" class="btn btn-primary pull-left">Volver</a></div>
				<div class="col-md-6"><h4>@yield('titulo_pagina')</h4></div>
				<div class="col-md-3"></div>
			</div>
			
			
			
		</div>
		<div class="panel-body">
	   		<form role="form"  action="{{ $trabajador->action_form }}" method="post" accept-charset="UTF-8" enctype="multipart/form-data">
	   			{{ csrf_field() }}
	   			@if ($trabajador->id)
				{!!  method_field('PUT') !!}
	   			@endif
	   			<fieldset class="col-md-6">
	   				<legend>Datos personales</legend>
	   				<div class="form-group">
	   					{{-- glyphicon glyphicon-lock --}}

				    	<label for="dni">DNI *:</label>
				    	<input type="numeric" class="form-control" id="dni" name="dni" value="{{ old('dni', $trabajador->dni) }}" placeholder="ingrese el DNI">
				    	@if($errors->has('dni')!=null)
				    	<span class="help-inline">{{ $errors->first('dni') }}</span>
				    	@endif
				  	</div>
				  	<div class="form-group">
				    	<label for="nombre">Nombre *:</label>
				    	<input type="text" class="form-control" id="nombre" name="nombre" value="{{ old('nombre',$trabajador->nombre) }}" placeholder="ingrese el nombre">
				    	@if($errors->has('nombre')!=null)
				    	<span class="help-inline">{{ $errors->first('nombre') }}</span>
				    	@endif
				  	</div>
				  	<div class="form-group">
				    	<label for="apellido">Apellido *:</label>
				    	<input type="text" class="form-control" id="apellido" name="apellido" value="{{ old('apellido',$trabajador->apellido) }}" placeholder="ingrese el apellido">
				    	@if($errors->has('apellido')!=null)
				    	<span class="help-inline">{{ $errors->first('apellido') }}</span>
				    	@endif
				  	</div>
				  	<div class="form-group">
				    	<label for="sexo_femenino">Sexo:</label>
				    	<div class="form-control">
				    		<label for="sexo_femenino" class="radio-inline ">
					    		<input type="radio" class="" id="sexo_femenino" name="sexo" value="{{ $trabajador::SEXO_FEMENINO }}" {{ old('sexo',$trabajador->sexo)==$trabajador::SEXO_FEMENINO?'checked':'' }}> Femenino
					    	</label>
					    	<label for="sexo_masculino" class="radio-inline ">
					    		<input type="radio" class="" id="sexo_masculino" name="sexo" value="{{ $trabajador::SEXO_MASCULINO }}" {{ old('sexo',$trabajador->sexo)==$trabajador::SEXO_MASCULINO?'checked':'' }}> Masculino
					    	</label>
				    	</div>
				    	@if($errors->has('sexo')!=null)
				    	<span class="help-inline">{{ $errors->first('sexo') }}</span>
				    	@endif
				  	</div>

				  	
				  	<?php 
				  		if ($trabajador->fecha_nacimiento->timestamp>0) {
				  			$fecha_nacimiento=$trabajador->fecha_nacimiento->format('Y-m-d');
				  		}else{
				  			$fecha_nacimiento="";
				  		}
				  		
				  	 ?>
				  	<div class="form-group" style="position:relative">
				    	<label for="fecha_nacimiento">Fecha nacimiento:</label>
				    	<input type="text" class="form-control datepicker_fecha_nacimiento" id="fecha_nacimiento" name="fecha_nacimiento" value="{{ old('fecha_nacimiento', $fecha_nacimiento) }}" placeholder="ingrese la fecha de nacimiento">
				    	@if($errors->has('fecha_nacimiento')!=null)
				    	<span class="help-inline">{{ $errors->first('fecha_nacimiento') }}</span>
				    	@endif
				  	</div>
				  	<div class="form-group">
				    	<label for="ruta_foto">Subir foto:</label>
						<input type="file" class="form-control" id="ruta_foto" name="ruta_foto" >
						<?php if ($trabajador->ruta_foto!=""): ?>
						<input type="hidden" id="ruta_foto_actual" name="ruta_foto_actual" value="{{ old('ruta_foto_actual',$trabajador->ruta_foto) }}">
						<?php endif ?>
						@if($trabajador->url_foto)
						<img id="preview_foto" src="{{ $trabajador->url_foto }}" alt="" class="form-control preview_foto">
						@endif
				  	</div>
	   			</fieldset>
	   			<fieldset class="col-md-6">
	   				<legend>Informacion de contacto</legend>
	   				<div class="form-group">
				    	<label for="correo_personal">Correo personal:</label>
				    	
				    	<input type="text" class="form-control" id="correo_personal" name="correo_personal"  value="{{ old('correo_personal',$trabajador->correo_personal) }}" placeholder="ingrese el correo electrónico">
				    	@if($errors->has('correo_personal')!=null)
				    	<span class="help-inline">{{ $errors->first('correo_personal') }}</span>
				    	@endif
				  	</div>
				  	<div class="form-group">
				    	<label for="telefono_fijo">Telefono fijo:</label>
				    	<input type="text" class="form-control" id="telefono_fijo" name="telefono_fijo"  value="{{ old('telefono_fijo',$trabajador->telefono_fijo) }}" placeholder="ingrese el telefono fijo">
				    	@if($errors->has('telefono_fijo')!=null)
				    	<span class="help-inline">{{ $errors->first('telefono_fijo') }}</span>
				    	@endif
				  	</div>
				  	<div class="form-group">
				    	<label for="telefono_celular">Telefono celular:</label>
				    	<input type="text" class="form-control" id="telefono_celular" name="telefono_celular"  value="{{ old('telefono_celular',$trabajador->telefono_celular) }}" placeholder="ingrese el telefono celular">
				    	@if($errors->has('telefono_celular')!=null)
				    	<span class="help-inline">{{ $errors->first('telefono_celular') }}</span>
				    	@endif
				  	</div>
				  	<div class="form-group">
				    	<label for="direccion">Direccion:</label>
				    	<input type="text" class="form-control" id="direccion" name="direccion" value="{{ old('direccion',$trabajador->direccion)}}" placeholder="ingrese la direccion">
				    	@if($errors->has('direccion')!=null)
				    	<span class="help-inline">{{ $errors->first('direccion') }}</span>
				    	@endif
				  	</div>
	   			</fieldset>
	   			<fieldset class="col-md-6">
	   				<legend>Informacion laboral</legend>
	   				<div class="form-group">
				    	<label for="apellido">Cargo:</label>
				    	<select class="form-control" id="id_cargo" name="id_cargo">
				    		@if(isset($cargos_activos) && count($cargos_activos)>0)
							<option value="">Seleccione un cargo</option>
				    		<?php foreach ($cargos_activos as $id => $cargo): ?>
				    		<option value="{{ $cargo->id }}" {{ old('id_cargo',$trabajador->id_cargo)==$cargo->id?'selected':'' }}>{{ $cargo->nombre }}</option>
				    		<?php endforeach ?>
				    		@else
				    		<option value="">No hay cargos disponibles</option>
							@endif
						</select>
						@if($errors->has('id_cargo')!=null)
				    	<span class="help-inline">{{ $errors->first('id_cargo') }}</span>
				    	@endif
				  	</div>
				  	<div class="form-group">
				    	<label for="apellido">Sucursal:</label>
				    	<select class="form-control" id="id_sucursal" name="id_sucursal">
				    		@if(isset($sucursales_activas) && count($sucursales_activas)>0)
							<option value="">Seleccione una sucursal</option>
				    		<?php foreach ($sucursales_activas as $id => $sucursal): ?>
				    		<option value="{{ $sucursal->id }}" {{ old('id_sucursal',$trabajador->id_sucursal)==$sucursal->id?'selected':'' }}>{{ $sucursal->nombre }}</option>
				    		<?php endforeach ?>
				    		@else
				    		<option value="">No hay sucursales disponibles</option>
							@endif
						</select>
						@if($errors->has('id_sucursal')!=null)
				    	<span class="help-inline">{{ $errors->first('id_sucursal') }}</span>
				    	@endif
				  	</div>
				  	<div class="form-group">
				    	<label for="estado">Estado:</label>
				    	<select class="form-control" id="estado" name="estado">
				    		@if(isset($estados_disponibles) && count($estados_disponibles)>0)
							<option value="">Seleccione un estado</option>
				    		<?php foreach ($estados_disponibles as $id => $estado): ?>
				    		<option value="{{ $estado->id }}" {{ old('estado',$trabajador->estado)==$estado->id?'selected':'' }}>{{ $estado->nombre }}</option>
				    		<?php endforeach ?>
				    		@else
				    		<option value="">No hay estados</option>
							@endif
						</select>
						@if($errors->has('estado')!=null)
				    	<span class="help-inline">{{ $errors->first('estado') }}</span>
				    	@endif
				  	</div>
	   			</fieldset>
			  	<!-- <div class="checkbox">
			  				    	<label><input type="checkbox"> Remember me</label>
			  	</div> -->
			  	<div class="col-md-12">
			  		<button type="submit" class="btn btn-success pull-right">Registrar</button>
			  	</div>
			</form>
		</div>
	</div>
</div>
<script type="text/javascript">
    function previsualizar(input_file_id,img_preview_id) {
        var oFReader = new FileReader();
        oFReader.readAsDataURL(document.getElementById(input_file_id).files[0]);

        oFReader.onload = function (oFREvent) {
        	if (!$("#"+img_preview_id).length) {
        		$("#"+input_file_id).parent().append($("<img id=\""+img_preview_id+"\" class=\"preview_foto form-control\">"));
        	}

        	$("#"+img_preview_id).attr('src',oFREvent.target.result);	
        };
    };

    $(document).on('change','#ruta_foto',function(){
    	previsualizar("ruta_foto","preview_foto");
    })
</script>

<style>
	.preview_foto{
		margin:10px auto;
		width:100%;height:100%;
		max-height: 125px;
		max-width: 125px;
	}
</style>
@endsection